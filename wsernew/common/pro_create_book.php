<?php
	include("../includes/client_config.php");
	include("../database/connection.php");
	include("../includes/server_config.php");
	include("../includes/utils.php");
	$header = new ResponseHeaderRest;
	$util = new utils;
	$row= new DB;
	
	$valid = $header->authorizedAccess();
	if($valid){
	global $new_token;
	
		include("includes/BOOK_CREATE.php");
		$EDITDETAIL	= new BOOK_CREATE;
		
		$status = $EDITDETAIL->FUNCTION_BOOK_CREATE();
		
		header("Content-Type: application/json");
		header("token-id: ".$new_token);
		
		echo $status;
		$row->close();
	}
?>

