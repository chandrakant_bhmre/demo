<?php 
	//error_reporting(0);
	include("../includes/client_config.php");
	include("../database/connection.php");
	include("../includes/server_config.php");
	include("../includes/utils.php");
	$header = new ResponseHeaderRest;
	$util = new utils;
	$row= new DB;
	$valid = $header->authorizedAccess();
	$authorized = $util->validate_comm();
	
	if($valid && is_array($authorized)){
		global $new_token;
		include("includes/PRO_USER_OUT.php");
		
		//Object is created
		$logout = new PRO_USER_OUT;
		
		//Function is called
		$status = $logout->FUNCTION_PRO_USER_OUT();
		
		// json with token in response header
		header("Content-Type: application/json");
		echo $status; 	//echo json
		$row->close();	//connection close
	}
?>