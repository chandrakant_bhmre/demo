<?php
class PRO_CHANGE_PASSWORD extends utils {
	
	function FUNCTION_PRO_CHANGE_PASSWORD(){
		global $row, $header;
		$json_input = file_get_contents("php://input");
		//$json_input=$_REQUEST['LOGIN_AUTH'];
		$data = json_decode($json_input, true);
		$accept = 'application/json';
				
			if(isset($data['P_UID']) && isset($data['P_TOKEN']) && isset($data['P_OPASS']) && isset($data['P_NPASS']))
			{
			
			$P_UID = filter_var($data['P_UID'], FILTER_SANITIZE_STRING); 
			$P_TOKEN = filter_var($data['P_TOKEN'], FILTER_SANITIZE_STRING); 
			$P_OPASS = $this->no_injection(filter_var($data['P_OPASS'], FILTER_SANITIZE_STRING));
			$P_NPASS = $this->no_injection(filter_var($data['P_NPASS'], FILTER_SANITIZE_STRING)); 
							
			if(!empty($P_OPASS) && !empty($P_UID) && !empty($P_TOKEN) && !empty($P_NPASS))
			{
				$TOKEN_VALIDATE = $this->WS_GET_TOKEN_VALIDATE($P_TOKEN,$P_UID);
				
				$P_USERID=$TOKEN_VALIDATE[0]['u_id'];
				
				if(count($TOKEN_VALIDATE)==1)
				{
					$CHK_STATUS = $this->FUNCTION_GET_PASS_EXIST($P_OPASS,$P_USERID,$P_UID);
					
					if($CHK_STATUS != 0 )
					{
						
							$C_STATUS = $this->FUNCTION_PASSWORD_CHANGE($P_OPASS,$P_NPASS,$P_USERID,$P_UID);
								
							
								
							if($C_STATUS != 0){
								
								$json = "{";
								$json = $json . "\"X_STS\":\"1\",";
								$json = $json . "\"X_MSG\":\"Password change successfully.\"";
								$json = $json . "}";
								echo $json;
								
								$statusCode = 200;
								$header->setHttpHeaders($accept, $statusCode);
							
								}
							else{
								$json = "{";
								$json = $json . "\"X_STS\":\"0\",";
								$json = $json . "\"X_MSG\":\"error.\"";
								$json = $json . "}";
								echo $json;
							}
						
						
					}
					else
					{
							$json = "{";
							$json = $json . "\"X_STS\":\"0\",";
							$json = $json . "\"X_MSG\":\"Old password is incorrect.\"";
							$json = $json . "}";
							echo $json;
					}
				}
				else
				{
					$json = "{";
						$json = $json . "\"X_STS\":\"0\",";
						$json = $json . "\"X_MSG\":\"Invalid token.\"";
						$json = $json . "}";
						echo $json;
					$statusCode = 401;
					$header->setHttpHeaders($accept, $statusCode);
					
				}
			}
			else{
				$json = "{";
						$json = $json . "\"X_STS\":\"0\",";
						$json = $json . "\"X_MSG\":\"Empty not allowed.\"";
						$json = $json . "}";
						echo $json;
				$statusCode = 404;
				$header->setHttpHeaders($accept, $statusCode);
			}
		}
		else{
			$json = "{";
						$json = $json . "\"X_STS\":\"0\",";
						$json = $json . "\"X_MSG\":\"Parameter is not set.\"";
						$json = $json . "}";
						echo $json;
			$statusCode = 404;
			$header->setHttpHeaders($accept, $statusCode);
		}
	}
	
	function FUNCTION_PASSWORD_CHANGE($P_OPASS,$P_NPASS,$P_USERID,$P_UID)
	{
		global $row;
		$evt_name=array();
		
					$sql ="UPDATE `pro_mst_user` SET `password` = '".md5($P_NPASS)."'
					WHERE `u_id` = '".trim($P_USERID)."' 
					AND `password` = '".md5($P_OPASS)."'";
					//echo $sql;
					$db_query = $row->query($sql);
					$affected_row = $row->affected_rows();
					
					
				
		return $affected_row;	
	}
	
	function FUNCTION_GET_PASS_EXIST($P_PASS,$P_USERID,$P_UID)
	{
		global $row;
		$evt_name=array();
		
		
	 	$sql = 		"SELECT * FROM `pro_mst_user` 
					WHERE `u_id` = '".trim($P_USERID)."' 
					AND `password` = '".md5($P_PASS)."'";
		
		//echo $sql;
			
		$db_query = $row->query($sql);
		$affected_row = $row->affected_rows();
		return $affected_row;
	}
	
	
}
?>