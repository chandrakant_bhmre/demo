<?php
class PRO_GET_BOOK_LIST extends utils 
{
	function FUNCTION_PRO_GET_BOOK_LIST()
	{
		global $row, $header;
		$json_input = file_get_contents("php://input");
		//$json_input=$_REQUEST['LOGIN_AUTH'];
		$data = json_decode($json_input, true);
		$accept = 'application/json';
		
			if(isset($data['P_UID']) && isset($data['P_TOKEN']) )
			{
				
			$P_UID = filter_var($data['P_UID'], FILTER_SANITIZE_NUMBER_INT); 
			$P_TOKEN = filter_var($data['P_TOKEN'], FILTER_SANITIZE_STRING); 
			
			
			if(!empty($P_UID) && !empty($P_TOKEN))
			{
				
				$TOKEN_VALIDATE = $this->WS_GET_TOKEN_VALIDATE($P_TOKEN,$P_UID);
				
				if(count($TOKEN_VALIDATE)==1)
				{
					
					$BOOK_ARRAY = $this->FUNCTION_GET_BOOK_ARRAY();
						$json = "{";
							$json = $json . "\"XSTS\":\"1\",";
							$json = $json . "\"XMSG\":\"Book list found\",";
							$json = $json . "\"X_BOOK_LIST\":";
							$json = $json . "[";
							for($i=0;$i<count($BOOK_ARRAY);$i++)
							{
								$json = $json . "{";
								$json = $json . "\"X_BID\":\"".$BOOK_ARRAY[$i]['b_id']."\",";
								$json = $json . "\"X_BNM\":\"".$BOOK_ARRAY[$i]['book_name']."\",";
								$json = $json . "\"X_BAUTHOR\":\"".$BOOK_ARRAY[$i]['author']."\",";
								$json = $json . "\"X_PRICE\":\"".$BOOK_ARRAY[$i]['price']."\",";
								$json = $json . "\"X_ACT\":\"".$BOOK_ARRAY[$i]['active']."\"";
								
								$json = $json . "}";
								
								if(count($BOOK_ARRAY)==($i+1))
								{
									$json = $json ."";
								}
								else
								{
									$json = $json .",";
								}
							}
							
							$json = $json . "]";
							$json = $json."}";
						return $json;
				}
				else
				{
					$json = "{";
						$json = $json . "\"XSTS\":\"0\"";
						$json = $json . "}";
						echo $json;
					$statusCode = 401;
					$header->setHttpHeaders($accept, $statusCode);
					
				}
			}
			else
			{
				$json = "{";
						$json = $json . "\"XSTS\":\"2\",";
						$json = $json . "\"XSTS\":\"Empty not allowed\"";
						$json = $json . "}";
						echo $json;
				$statusCode = 404;
				$header->setHttpHeaders($accept, $statusCode);
			}
		}
		else{
			$json = "{";
						$json = $json . "\"XSTS\":\"0\"";
						$json = $json . "}";
						echo $json;
			$statusCode = 404;
			$header->setHttpHeaders($accept, $statusCode);
		}
	}
	
	function FUNCTION_GET_BOOK_ARRAY()
	{
		global $row;
		$evt_name=array();
		$sql="SELECT * FROM pro_mst_book";
		
		$db_query=$row->query($sql);
		while($client_result=$row->next_record())
		{
			$record = array();
			foreach(array_keys($client_result) as $key)
			{
			if(gettype($key)=="string")
				{
					$record[$key] = stripslashes(stripslashes($client_result[$key]));
				}
			}
			$evt_name[]=$record;
		}
		return $evt_name;	
	}
}
	
?>