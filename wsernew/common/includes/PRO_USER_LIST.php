<?php
class PRO_USER_LIST extends utils {
	
	function FUNCTION_PRO_USER_LIST(){
		global $row, $header;
		$json_input = file_get_contents("php://input");
		//$json_input=$_REQUEST['LOGIN_AUTH'];//To run from postman
		$data = json_decode($json_input, true);
		
		$accept = 'application/json';
			if(isset($data['P_UID']) && isset($data['P_TOKEN'])){
				
			$P_UID = filter_var($data['P_UID'], FILTER_SANITIZE_STRING); 
			$P_TOKEN = filter_var($data['P_TOKEN'], FILTER_SANITIZE_STRING); 
			
			if(!empty($P_UID) && !empty($P_TOKEN)){
				
				$TOKEN_VALIDATE = $this->WS_GET_TOKEN_VALIDATE($P_TOKEN,$P_UID);
				
				if(count($TOKEN_VALIDATE)==1)
				{
						
							$json = "{";
							$json = $json . "\"X_STS\":\"1\",";
							$json = $json . "\"X_MSG\":\"User list found\",";
							$json = $json . "\"X_USER_LIST\":";
							$json = $json . "[";
							$USER_ARRAY_LIST = $this->WS_GET_EMPLOYEE_DETAILS();
							
							for($i=0;$i<count($USER_ARRAY_LIST);$i++)
							{
														
								$json = $json . "{";
								$json = $json . "\"X_UID\":\"".$USER_ARRAY_LIST[$i]['u_id']."\",";
								$json = $json . "\"X_FNAME\":\"".$USER_ARRAY_LIST[$i]['first_name']."\",";
								$json = $json . "\"X_LNAME\":\"".$USER_ARRAY_LIST[$i]['last_name']."\",";
								$json = $json . "\"X_MNO\":\"".$USER_ARRAY_LIST[$i]['mobile_no']."\",";
								$json = $json . "\"X_EMAIL\":\"".$USER_ARRAY_LIST[$i]['email']."\",";
								$json = $json . "\"X_AGE\":\"".$USER_ARRAY_LIST[$i]['age']."\",";
								$json = $json . "\"X_GENDER\":\"".$USER_ARRAY_LIST[$i]['gender']."\",";
								$json = $json . "\"X_CITY\":\"".$USER_ARRAY_LIST[$i]['city']."\",";
								$json = $json . "\"X_USTAT\":\"Y\"";
								
								$json = $json . "}";
							if(count($USER_ARRAY_LIST)==($i+1))
								{
									$json = $json ."";
								}
								else
								{
									$json = $json .",";
								}
							}
							
							$json = $json . "]";
							$json = $json."}";
						return $json;
				}
				else
				{
					$json = "{";
						$json = $json . "\"XSTS\":\"0\"";
						$json = $json . "}";
						echo $json;
					$statusCode = 401;
					$header->setHttpHeaders($accept, $statusCode);
					
				}
			}
			else{
				$json = "{";
						$json = $json . "\"XSTS\":\"0\"";
						$json = $json . "}";
						echo $json;
				$statusCode = 404;
				$header->setHttpHeaders($accept, $statusCode);
			}
		}
		else{
			$json = "{";
						$json = $json . "\"XSTS\":\"0\"";
						$json = $json . "}";
						echo $json;
			$statusCode = 404;
			$header->setHttpHeaders($accept, $statusCode);
		}
	}
	
	function WS_GET_EMPLOYEE_DETAILS()
	{
		global $row;
		$evt_name=array();
		$sql="SELECT * FROM pro_mst_user";
		$db_query=$row->query($sql);
		while($client_result=$row->next_record())
		{
			$record = array();
			foreach(array_keys($client_result) as $key)
			{
			if(gettype($key)=="string")
				{
					$record[$key] = stripslashes(stripslashes($client_result[$key]));
				}
			}
			$evt_name[]=$record;
		}
		return $evt_name;	
	}

}
?>