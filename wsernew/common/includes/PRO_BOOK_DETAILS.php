<?php
class PRO_BOOK_DETAILS extends utils {
	
	function FUNCTION_PRO_BOOK_DETAILS(){
		global $row, $header;
		$json_input = file_get_contents("php://input");
		//$json_input=$_REQUEST['LOGIN_AUTH'];
		$data = json_decode($json_input, true);
		
		$accept = 'application/json';
			if(isset($data['P_UID']) && isset($data['P_TOKEN']) && isset($data['P_BOOKID'])){
				
			$P_UID = filter_var($data['P_UID'], FILTER_SANITIZE_NUMBER_INT); 
			$P_TOKEN = filter_var($data['P_TOKEN'], FILTER_SANITIZE_STRING); 
			$P_BOOKID = filter_var($data['P_BOOKID'], FILTER_SANITIZE_NUMBER_INT); 
				
			if(!empty($P_UID) && !empty($P_TOKEN) && !empty($P_BOOKID))
			{
				$TOKEN_VALIDATE = $this->WS_GET_TOKEN_VALIDATE($P_TOKEN,$P_UID);
					
				if(count($TOKEN_VALIDATE)==1)
				{
					$BOOK_ARRAY = $this->FUNCTION_GET_BOOK_ARRAY($P_BOOKID);
					
						$json = $json . "{";
						$json = $json . "\"X_BID\":\"".$BOOK_ARRAY[0]['b_id']."\",";
						$json = $json . "\"X_BNAME\":\"".$BOOK_ARRAY[0]['book_name']."\",";
						$json = $json . "\"X_BAUTHOR\":\"".$BOOK_ARRAY[0]['author']."\",";
						$json = $json . "\"X_BIMG\":\"".$BOOK_ARRAY[0]['cover_image_string']."\",";
						$json = $json . "\"X_BPRICE\":\"".$BOOK_ARRAY[0]['price']."\",";
						$json = $json . "\"X_STS\":\"1\"";
						$json = $json . "}";
					return $json;
						
				}
				else
				{
					$json = "{";
						$json = $json . "\"XSTS\":\"0\"";
						$json = $json . "}";
						echo $json;
					$statusCode = 401;
					$header->setHttpHeaders($accept, $statusCode);
					
				}
			}
			else{
				$json = "{";
						$json = $json . "\"XSTS\":\"0\"";
						$json = $json . "}";
						echo $json;
				$statusCode = 404;
				$header->setHttpHeaders($accept, $statusCode);
			}
		}
		else{
			$json = "{";
						$json = $json . "\"XSTS\":\"0\"";
						$json = $json . "}";
						echo $json;
			$statusCode = 404;
			$header->setHttpHeaders($accept, $statusCode);
		}
	}
	
	
	function FUNCTION_GET_BOOK_ARRAY($P_BOOKID)
	{
		global $row;
		$evt_name=array();
		
				$sql="SELECT * FROM pro_mst_book WHERE b_id='".$P_BOOKID."'";
				$db_query=$row->query($sql);
		while($client_result=$row->next_record())
		{
			$record = array();
			foreach(array_keys($client_result) as $key)
			{
			if(gettype($key)=="string")
				{
					$record[$key] = stripslashes(stripslashes($client_result[$key]));
				}
			}
			$evt_name[]=$record;
		}
		return $evt_name;	
	}
}
?>