<?php
class PRO_USER_CREATE extends utils{
	
	function FUNCTION_PRO_USER_CREATE(){
		
		global $row, $header, $user_data, $new_token;
		$json_input = file_get_contents("php://input");
		//$json_input = $_REQUEST['LOGIN_AUTH'];
	//print_r($json_input);exit;
		$data = json_decode($json_input, true);
		//print_r($data);exit;		
		$accept = 'application/json';
		$json = "";
		
		if(isset($data['first_name']) && isset($data['last_name']) && isset($data['mobile_no']) && isset($data['email']) && isset($data['age']) && isset($data['city']) && isset($data['gender']) && isset($data['u_password'])){
			$first_name = $this->no_injection(filter_var($data['first_name'], FILTER_SANITIZE_STRING));
			$last_name = $this->no_injection(filter_var($data['last_name'], FILTER_SANITIZE_STRING));
			$mobile_no = $this->no_injection(filter_var($data['mobile_no'], FILTER_SANITIZE_NUMBER_INT));
			$email = $this->no_injection(filter_var($data['email'], FILTER_SANITIZE_STRING));
			$age = $this->no_injection(filter_var($data['age'], FILTER_SANITIZE_NUMBER_INT));
			$city = $this->no_injection(filter_var($data['city'], FILTER_SANITIZE_STRING));
			$gender = $this->no_injection(filter_var($data['gender'], FILTER_SANITIZE_STRING));
			$u_password = $this->no_injection(filter_var($data['u_password'], FILTER_SANITIZE_STRING));
			
			
			if(!empty($first_name) && !empty($last_name) && !empty($mobile_no) && !empty($email) && !empty($age)  && !empty($city)  && !empty($gender) && !empty($u_password)){
				$PNTOK = $this->randomString(35);
				
				//$first_name,$last_name,$mobile_no,$email,$age,$city,$gender,$u_password
					
					$CHECK_DUP = $this->CHK_DUPLICATE_FUNCTION($mobile_no);
					if(count($CHECK_DUP)==0)
					{

					$INSERT_RESULT = $this->INSERT_BOOK_DATA($first_name,$last_name,$mobile_no,$email,$age,$city,$gender,$u_password);
					
														
					if(count($INSERT_RESULT) != 0)
					{
				
						
				$GLOBALS['new_token'] = $PNTOK;
				$RET_CNT=$this->Find_USR_DATA_NEW($mobile_no,$u_password);
				
				
			//	print_r($RET_CNT);exit;
				if(count($RET_CNT)==1){
					$sql_up="update pro_mst_user set session_id='".$PNTOK."' where u_id='".$RET_CNT[0]['u_id']."'";
					$db_query = $row->query($sql_up);
					$affected_row = $row->affected_rows();	
					
						$json = "{";
						$json = $json . "\"XUID\":\"".$RET_CNT[0]['u_id']."\",";
						$json = $json . "\"XFNM\":\"".$RET_CNT[0]['first_name']."\",";
						$json = $json . "\"XLNM\":\"".$RET_CNT[0]['last_name']."\",";
						$json = $json . "\"XMOBILE\":\"".$RET_CNT[0]['mobile_no']."\",";
						$json = $json . "\"XEMAIL\":\"".$RET_CNT[0]['email']."\",";
						$json = $json . "\"XAGE\":\"".$RET_CNT[0]['age']."\",";
						$json = $json . "\"XGENDER\":\"".$RET_CNT[0]['gender']."\",";
						$json = $json . "\"XTOKEN\":\"".$RET_CNT[0]['session_id']."\",";
						$json = $json . "\"XCITY\":\"".$RET_CNT[0]['city']."\",";
						$json = $json . "\"XSTS\":\"1\"";
						$json = $json . "}";
						
						return  $json;
						exit;
						
				}
			else{
				$statusCode = 401;
				$header->setHttpHeaders($accept, $statusCode);
			}
			
			}
					else
					{
						$json = "{";
						$json = $json . "\"XSTS\":\"0\",";
						$json = $json . "\"XMSG\":\"User Insetion error.\"";
						$json = $json . "}";
						echo $json;
						
						$statusCode = 401;
					$header->setHttpHeaders($accept, $statusCode);
					}
				}
				else
				{
					$json = "{";
						$json = $json . "\"XSTS\":\"0\",";
						$json = $json . "\"XMSG\":\"Book duplicate Found!\"";
						$json = $json . "}";
						echo $json;
					$statusCode = 401;
					$header->setHttpHeaders($accept, $statusCode);
				}
			
			}
			else{
				$statusCode = 404;
				$header->setHttpHeaders($accept, $statusCode);
			}
		}
		else{
			$statusCode = 404;
			$header->setHttpHeaders($accept, $statusCode);
		}
	}
	
	
	function CHK_DUPLICATE_FUNCTION($mobile_no)
	{
		global $row;
		$evt_name=array();
		$sql="SELECT u_id FROM pro_mst_user WHERE mobile_no='".$mobile_no."'";
		
		$db_query=$row->query($sql);
		while($client_result=$row->next_record())
		{
			$record = array();
			foreach(array_keys($client_result) as $key)
			{
			if(gettype($key)=="string")
				{
					$record[$key] = stripslashes(stripslashes($client_result[$key]));
				}
			}
			$evt_name[]=$record;
		}
		return $evt_name;	
	}
		
		
	function INSERT_BOOK_DATA($first_name,$last_name,$mobile_no,$email,$age,$city,$gender,$u_password)
	{
		global $row;
		$evt_name=array();
		$sql = "INSERT INTO `pro_mst_user`(
							`first_name`,
							`last_name`,
							`mobile_no`,
							`email`,
							`age`,
							`gender`,
							`city`,
							`password`,
							`created_by`,
							`created_on`
							) VALUES (
							'".$first_name."',
							'".$last_name."',
							'".$mobile_no."',
							'".$email."',
							'".$age."',
							'".$gender."',
							'".$city."',
							'".md5($u_password)."',
							'1',
							'".date('Y-m-d H:i:s')."'
							)";
				//	echo $sql;exit;
		
		$db_query = $row->query($sql);
		$affected_row = $row->affected_rows();
		return $affected_row;
	}		
		
	function Find_USR_DATA_NEW($PUNM,$PCPW)
	{
		$sql="select * from pro_mst_user where mobile_no='".$PUNM."' and password='".md5($PCPW)."'";
		
		global $row;
		$evt_name=array();
			$db_query=$row->query($sql);
			while($client_result=$row->next_record())
			{
				$record = array();
				foreach(array_keys($client_result) as $key)
				{
				if(gettype($key)=="string")
					{
						$record[$key] = stripslashes(stripslashes($client_result[$key]));
					}
				}
				$evt_name[]=$record;
			}
			return $evt_name;	
	}

}
?>