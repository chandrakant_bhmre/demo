<?php
class PROFILE_UPDATE extends utils 
{
	
	function FUNCTION_PROFILE_UPDATE()
	{
		global $row, $header;
		$json_input = file_get_contents("php://input");
		//$json_input=$_REQUEST['LOGIN_AUTH'];
		$data = json_decode($json_input, true);
		
		//echo '<pre>';print_r($data);echo '</pre>';
		//exit;
		$accept = 'application/json';
		
			if(isset($data['P_UID']) && isset($data['P_TOKEN']) && isset($data['P_FNM']) && isset($data['P_LNM']) && isset($data['P_EMIAL']) && isset($data['P_MID']) && isset($data['P_CITY']) && isset($data['P_AGE']) && isset($data['P_GENDER']))
			{
			
			$P_UID = filter_var($data['P_UID'], FILTER_SANITIZE_NUMBER_INT);
			$P_TOKEN = filter_var($data['P_TOKEN'], FILTER_SANITIZE_STRING); 
			$P_FNM = filter_var($data['P_FNM'], FILTER_SANITIZE_STRING); 
			$P_LNM = filter_var($data['P_LNM'], FILTER_SANITIZE_STRING);
			$P_EMIAL = $data['P_EMIAL'];
			$P_CITY = filter_var($data['P_CITY'], FILTER_SANITIZE_STRING);
			$P_AGE = filter_var($data['P_AGE'], FILTER_SANITIZE_NUMBER_INT);
			$P_GENDER = filter_var($data['P_GENDER'], FILTER_SANITIZE_STRING);
			$P_MID = filter_var($data['P_MID'], FILTER_SANITIZE_NUMBER_INT);
					
			
			if(!empty($P_UID) && !empty($P_TOKEN) && !empty($P_FNM) && !empty($P_LNM) && !empty($P_EMIAL) && !empty($P_MID))
			{
				
				$TOKEN_VALIDATE = $this->WS_GET_TOKEN_VALIDATE($P_TOKEN,$P_UID);
				
				if(count($TOKEN_VALIDATE)==1)
				{
					/*$CHK_DUP = $this->FUNCTION_DUPLICATE_BOOK($P_FNM,$P_MID);
					if(count($CHK_DUP)==0)
					{*/
					$RESULT = $this->FUNCTION_UPDATE_BOOK_DATA($P_UID,$P_FNM,$P_LNM,$P_EMIAL,$P_CITY,$P_AGE,$P_GENDER,$P_MID);
					
														
					if(count($RESULT) != 0){
						
						$json = "{";
						$json = $json . "\"X_STS\":\"1\",";
						$json = $json . "\"X_MSG\":\"Profile updated successfully.\"";
						$json = $json . "}";
						echo $json;
						
						$statusCode = 200;
						$header->setHttpHeaders($accept, $statusCode);
					
						}
					else{
						$json = "{";
						$json = $json . "\"X_STS\":\"0\",";
						$json = $json . "\"X_MSG\":\"Profile updatation error.\"";
						$json = $json . "}";
						echo $json;
					}
				/*}
				else
				{
					$json = "{";
						$json = $json . "\"X_STS\":\"0\",";
						$json = $json . "\"X_MSG\":\"Book duplicate Found!\"";
						$json = $json . "}";
						echo $json;
					$statusCode = 401;
					$header->setHttpHeaders($accept, $statusCode);
					
				}	*/
				
				}
				else
				{
					$json = "{";
						$json = $json . "\"X_STS\":\"0\",";
						$json = $json . "\"X_MSG\":\"Invalid token.\"";
						$json = $json . "}";
						echo $json;
					$statusCode = 401;
					$header->setHttpHeaders($accept, $statusCode);
					
				}
			}
			else{
				$json = "{";
						$json = $json . "\"X_STS\":\"0\",";
						$json = $json . "\"X_MSG\":\"Empty not allowed.\"";
						$json = $json . "}";
						echo $json;
				$statusCode = 404;
				$header->setHttpHeaders($accept, $statusCode);
			}
		}
		else{
			$json = "{";
						$json = $json . "\"X_STS\":\"0\",";
						$json = $json . "\"X_MSG\":\"Parameter is not set.\"";
						$json = $json . "}";
						echo $json;
			$statusCode = 404;
			$header->setHttpHeaders($accept, $statusCode);
		}
	}
	
	function FUNCTION_UPDATE_BOOK_DATA($P_UID,$P_FNM,$P_LNM,$P_EMIAL,$P_CITY,$P_AGE,$P_GENDER,$P_MID)
	{
		global $row;
		$evt_name=array();
				
		$sql = "UPDATE `pro_mst_user` set
							`first_name`='".$P_FNM."',
							`last_name`='".$P_LNM."',
							`email`='".$P_EMIAL."',
							`age`='".$P_AGE."',
							`gender`='".$P_GENDER."',
							`city`='".$P_CITY."',
							`modified_by`='".$P_UID."',
							`modified_on`='".date('Y-m-d H:i:s')."'";
		$sql.=" WHERE u_id='".$P_MID."'";
	 	
		$db_query = $row->query($sql);
		$affected_row = $row->affected_rows();

		return $affected_row;
	}
	
	function FUNCTION_DUPLICATE_BOOK($P_FNM,$P_MID)
	{
		global $row;
		$evt_name=array();
		$sql="SELECT b_id FROM pro_mst_book where book_name='".$P_FNM."' AND b_id!=".$P_MID."";
		$db_query=$row->query($sql);
		while($client_result=$row->next_record())
		{
			$record = array();
			foreach(array_keys($client_result) as $key)
			{
			if(gettype($key)=="string")
				{
					$record[$key] = stripslashes(stripslashes($client_result[$key]));
				}
			}
			$evt_name[]=$record;
		}
		return $evt_name;	
	}
}
?>