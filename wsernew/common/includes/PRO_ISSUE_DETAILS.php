<?php
class PRO_ISSUE_DETAILS extends utils {
	
	function FUNCTION_PRO_ISSUE_DETAILS(){
		global $row, $header;
		$json_input = file_get_contents("php://input");
		//$json_input=$_REQUEST['LOGIN_AUTH'];
		$data = json_decode($json_input, true);
		
		$accept = 'application/json';
			if(isset($data['P_UID']) && isset($data['P_TOKEN']) && isset($data['P_ISSUEID'])){
				
			$P_UID = filter_var($data['P_UID'], FILTER_SANITIZE_NUMBER_INT); 
			$P_TOKEN = filter_var($data['P_TOKEN'], FILTER_SANITIZE_STRING); 
			$P_ISSUEID = filter_var($data['P_ISSUEID'], FILTER_SANITIZE_NUMBER_INT); 
				
			if(!empty($P_UID) && !empty($P_TOKEN) && !empty($P_ISSUEID))
			{
				$TOKEN_VALIDATE = $this->WS_GET_TOKEN_VALIDATE($P_TOKEN,$P_UID);
					
				if(count($TOKEN_VALIDATE)==1)
				{
					$BOOK_ARRAY = $this->FUNCTION_GET_BOOK_ARRAY($P_ISSUEID);
					
					$single_user=$this->GET_SINGLE_USER($BOOK_ARRAY[0]['u_id']);
								
								
								$usernm=$single_user[0]['first_name']." ".$single_user[0]['last_name'];
								
								$single_book=$this->GET_SINGLE_BOOKNM($BOOK_ARRAY[0]['book_id']);
								
								$booknm=$single_book[0]['book_name']." (".$single_book[0]['author'].")";
						$json = $json . "{";
						$json = $json . "\"X_RID\":\"".$BOOK_ARRAY[0]['rent_id']."\",";
						$json = $json . "\"X_BNAME\":\"".$booknm."\",";
						$json = $json . "\"X_UNAME\":\"".$usernm."\",";
						$json = $json . "\"X_RENT\":\"".$BOOK_ARRAY[0]['book_rent']."\",";
						$json = $json . "\"X_ISSUEDT\":\"".$BOOK_ARRAY[0]['issue_date']."\",";
						$json = $json . "\"X_STS\":\"1\"";
						$json = $json . "}";
					return $json;
						
				}
				else
				{
					$json = "{";
						$json = $json . "\"XSTS\":\"0\"";
						$json = $json . "}";
						echo $json;
					$statusCode = 401;
					$header->setHttpHeaders($accept, $statusCode);
					
				}
			}
			else{
				$json = "{";
						$json = $json . "\"XSTS\":\"0\"";
						$json = $json . "}";
						echo $json;
				$statusCode = 404;
				$header->setHttpHeaders($accept, $statusCode);
			}
		}
		else{
			$json = "{";
						$json = $json . "\"XSTS\":\"0\"";
						$json = $json . "}";
						echo $json;
			$statusCode = 404;
			$header->setHttpHeaders($accept, $statusCode);
		}
	}
	
	
	function FUNCTION_GET_BOOK_ARRAY($P_ISSUEID)
	{
		global $row;
		$evt_name=array();
		
				$sql="SELECT * FROM pro_trn_rent WHERE rent_id='".$P_ISSUEID."'";
				$db_query=$row->query($sql);
		while($client_result=$row->next_record())
		{
			$record = array();
			foreach(array_keys($client_result) as $key)
			{
			if(gettype($key)=="string")
				{
					$record[$key] = stripslashes(stripslashes($client_result[$key]));
				}
			}
			$evt_name[]=$record;
		}
		return $evt_name;	
	}
}
?>