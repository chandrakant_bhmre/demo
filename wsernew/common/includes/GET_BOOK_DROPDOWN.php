<?php
class GET_BOOK_DROPDOWN extends utils {
	function FUNCTION_GET_BOOK_DROPDOWN(){
		global $row, $header;
		$json_input = file_get_contents("php://input");
		//$json_input=$_REQUEST['LOGIN_AUTH'];
		$data = json_decode($json_input, true);
		$accept = 'application/json';
		
			if(isset($data['P_UID']) && isset($data['P_TOKEN'])){
				
			$P_UID = filter_var($data['P_UID'], FILTER_SANITIZE_STRING); 
			$P_TOKEN = filter_var($data['P_TOKEN'], FILTER_SANITIZE_STRING); 
			if(!empty($P_UID) && !empty($P_TOKEN)){
				
				$TOKEN_VALIDATE = $this->WS_GET_TOKEN_VALIDATE($P_TOKEN,$P_UID);
				
				if(count($TOKEN_VALIDATE)==1)
				{
					$BOOK_ARRAY = $this->FUNCTION_GET_BOOK_ARRAY();
						$json = "{";
							$json = $json . "\"X_STS\":\"1\",";
							$json = $json . "\"X_MSG\":\"Book list found\",";
							$json = $json . "\"X_BOOK_LIST\":";
							$json = $json . "[";
							for($i=0;$i<count($BOOK_ARRAY);$i++)
							{
								$fullname=$BOOK_ARRAY[$i]['book_name']." (".$BOOK_ARRAY[$i]['author'].")";
								$json = $json . "{";
								$json = $json . "\"X_BID\":\"".$BOOK_ARRAY[$i]['b_id']."\",";
								$json = $json . "\"X_BNAME\":\"".$fullname."\",";
								$json = $json . "\"X_PSTAT\":\"y\"";
								
								$json = $json . "}";
								
								if(count($BOOK_ARRAY)==($i+1))
								{
									$json = $json ."";
								}
								else
								{
									$json = $json .",";
								}
							}
							
							$json = $json . "]";
							$json = $json."}";
						return $json;
				}
				else
				{
					$json = "{";
						$json = $json . "\"XSTS\":\"0\"";
						$json = $json . "}";
						echo $json;
					$statusCode = 401;
					$header->setHttpHeaders($accept, $statusCode);
					
				}
			}
			else{
				$json = "{";
						$json = $json . "\"XSTS\":\"2\"";
						$json = $json . "}";
						echo $json;
				$statusCode = 404;
				$header->setHttpHeaders($accept, $statusCode);
			}
		}
		else{
			$json = "{";
						$json = $json . "\"XSTS\":\"0\"";
						$json = $json . "}";
						echo $json;
			$statusCode = 404;
			$header->setHttpHeaders($accept, $statusCode);
		}
	}
	
	function FUNCTION_GET_BOOK_ARRAY()
	{
		global $row;
		$evt_name=array();
		
		$sql="SELECT b_id,book_name,author FROM pro_mst_book where b_id not in (select book_id from pro_trn_rent where return_date IS NULL)";
				
				
		$db_query=$row->query($sql);
		while($client_result=$row->next_record())
		{
			$record = array();
			foreach(array_keys($client_result) as $key)
			{
			if(gettype($key)=="string")
				{
					$record[$key] = stripslashes(stripslashes($client_result[$key]));
				}
			}
			$evt_name[]=$record;
		}
		return $evt_name;	
	}
	
}
?>