<?php
class PRO_USER_PROFILE extends utils {
	
	function FUNCTION_PRO_USER_PROFILE(){
		global $row, $header;
		$json_input = file_get_contents("php://input");
		//$json_input=$_REQUEST['LOGIN_AUTH'];
		$data = json_decode($json_input, true);
		
		$accept = 'application/json';
			if(isset($data['P_UID']) && isset($data['P_TOKEN']) && isset($data['P_MID'])){
				
			$P_UID = filter_var($data['P_UID'], FILTER_SANITIZE_NUMBER_INT); 
			$P_TOKEN = filter_var($data['P_TOKEN'], FILTER_SANITIZE_STRING); 
			$P_MID = filter_var($data['P_MID'], FILTER_SANITIZE_NUMBER_INT); 
				
			if(!empty($P_UID) && !empty($P_TOKEN) && !empty($P_MID))
			{
				$TOKEN_VALIDATE = $this->WS_GET_TOKEN_VALIDATE($P_TOKEN,$P_UID);
					
				if(count($TOKEN_VALIDATE)==1)
				{
					$PROFILE_ARRAY = $this->FUNCTION_GET_PROFILE_ARRAY($P_MID);
					
						$json = $json . "{";
						$json = $json . "\"X_UID\":\"".$PROFILE_ARRAY[0]['u_id']."\",";
						$json = $json . "\"X_FNAME\":\"".$PROFILE_ARRAY[0]['first_name']."\",";
						$json = $json . "\"X_LNAME\":\"".$PROFILE_ARRAY[0]['last_name']."\",";
						$json = $json . "\"X_MONO\":\"".$PROFILE_ARRAY[0]['mobile_no']."\",";
						$json = $json . "\"X_EMAIL\":\"".$PROFILE_ARRAY[0]['email']."\",";
						$json = $json . "\"X_AGE\":\"".$PROFILE_ARRAY[0]['age']."\",";
						$json = $json . "\"X_GENDER\":\"".$PROFILE_ARRAY[0]['gender']."\",";
						$json = $json . "\"X_CITY\":\"".$PROFILE_ARRAY[0]['city']."\",";
						$json = $json . "\"X_STS\":\"1\"";
						$json = $json . "}";
					return $json;
						
				}
				else
				{
					$json = "{";
						$json = $json . "\"XSTS\":\"0\"";
						$json = $json . "}";
						echo $json;
					$statusCode = 401;
					$header->setHttpHeaders($accept, $statusCode);
					
				}
			}
			else{
				$json = "{";
						$json = $json . "\"XSTS\":\"0\"";
						$json = $json . "}";
						echo $json;
				$statusCode = 404;
				$header->setHttpHeaders($accept, $statusCode);
			}
		}
		else{
			$json = "{";
						$json = $json . "\"XSTS\":\"0\"";
						$json = $json . "}";
						echo $json;
			$statusCode = 404;
			$header->setHttpHeaders($accept, $statusCode);
		}
	}
	
	
	function FUNCTION_GET_PROFILE_ARRAY($P_MID)
	{
		global $row;
		$evt_name=array();
		
				$sql="SELECT * FROM pro_mst_user WHERE u_id='".$P_MID."'";
				$db_query=$row->query($sql);
		while($client_result=$row->next_record())
		{
			$record = array();
			foreach(array_keys($client_result) as $key)
			{
			if(gettype($key)=="string")
				{
					$record[$key] = stripslashes(stripslashes($client_result[$key]));
				}
			}
			$evt_name[]=$record;
		}
		return $evt_name;	
	}
}
?>