<?php
class USER_LOGIN extends utils{
	
	function WS_USER_LOGIN(){
		
		global $row, $header, $user_data, $new_token;
		$json_input = file_get_contents("php://input");
		//$json_input = $_REQUEST['LOGIN_AUTH'];
	//print_r($json_input);exit;
		$data = json_decode($json_input, true);
				
		$accept = 'application/json';
		$json = "";
		
		if(isset($data['PUNM']) && isset($data['PCPW'])){
			$PUNM = $this->no_injection(filter_var($data['PUNM'], FILTER_SANITIZE_STRING));
			$PCPW = $this->no_injection(filter_var($data['PCPW'], FILTER_SANITIZE_STRING));
			
			if(!empty($PUNM) && !empty($PCPW)){
				$PNTOK = $this->randomString(35);
				$PNSID = $this->randomString(70);
							
				
				$GLOBALS['new_token'] = $PNTOK;
				$RET_CNT=$this->Find_USR_DATA_NEW($PUNM,$PCPW);
				print_r($RET_CNT);exit;
				if(count($RET_CNT)){
					
					$sql_up="update skd_mst_user set ws_token='".$PNTOK."',session_id='".$PNSID."' where user_name='".$PUNM."' and password='".md5($PCPW)."'";
					$db_query = $row->query($sql_up);
					$affected_row = $row->affected_rows();	
					
					
				$success=$this->GET_DATA_FROM_LOGIN($PUNM,$PCPW);
				
				if(isset($success))
				{
					
					if(count($success)== 1)
					{
						
						
						$USER_DATA = $this->GET_EMPLOYEE_DATA($success[0]['employee_id']);
						
						//$name=$code=$desi=$dob=$email=$dept='';
						for($k=0;$k<count($USER_DATA);$k++)
						{
							
							if(trim($name)=='')
							{$name = $USER_DATA[$k]['name'];}
							else{$name .= ','.$USER_DATA[$k]['name'];}
							if(trim($dept)=='')
							{	$dept = $USER_DATA[$k]['dept'];}
							else
							{$dept .= ','.$USER_DATA[$k]['dept'];		}
							
							if(trim($desi) == '')
							{$desi = $USER_DATA[$k]['desi'];}
							else
							{$desi .= ','.$USER_DATA[$k]['desi'];}
							
							if(trim($dob) == '')
							{$dob =$USER_DATA[$k]['dob'];}
							else
							{$dob .= ','.$USER_DATA[$k]['dob'];		}
							
							if(trim($email) == '')
							{$email = $USER_DATA[$k]['email'];}
							else
							{$email .= ','.$USER_DATA[$k]['email'];}
							
							if(trim($code) == '')
							{	$code = $USER_DATA[$k]['code'];}
							else
							{ $code .=','.$USER_DATA[$k]['code'];}
							
							if(trim($company_id) == '')
							{	$company_id = $USER_DATA[$k]['company_id'];}
							else
							{ $company_id .=','.$USER_DATA[$k]['company_id'];}
							
						
						}
						
						
												
						$json = "{";
						$json = $json . "\"XUID\":\"".$success[0]['user_id']."\",";
						$json = $json . "\"XEMPID\":\"".$success[0]['employee_id']."\",";
						$json = $json . "\"XEMPCODE\":\"".$code."\",";
						$json = $json . "\"XDEPTID\":\"".$dept."\",";
						$json = $json . "\"XEMPDESIG\":\"".$desi."\",";
						$json = $json . "\"XFNM\":\"".$name."\",";
						$json = $json . "\"XLNM\":\"".$name."\",";
						$json = $json . "\"XEMPBIRTH\":\"".$dob."\",";
						$json = $json . "\"XUNME\":\"".$success[0]['user_name']."\",";
						$json = $json . "\"XEMAIL\":\"".$email."\",";
						$json = $json . "\"XCOMPID\":\"".$company_id."\",";
						$json = $json . "\"XROLE\":\"".$success[0]['role_id']."\",";
						$json = $json . "\"XROLECODE\":\"".$success[0]['role_code']."\",";
						$json = $json . "\"XTOKEN\":\"".$success[0]['ws_token']."\",";
						$json = $json . "\"XSESID\":\"".$success[0]['session_id']."\",";
						$json = $json . "\"XROLENM\":\"".$success[0]['role_name']."\",";
						$json = $json . "\"XSTS\":\"1\"";
						$json = $json . "}";
						
						return $json;
						
						
					}
					else{
						$json = "{";
						$json = $json . "\"XSTS\":\"0\"";
						$json = $json . "}";
						return $json;
					}
					return $json;
				}
				else{
					$json = "{";
					$json = $json . "\"XSTS\":\"0\"";
					$json = $json . "}";
					return $json;
				}
				
				}
			else{
				$statusCode = 401;
				$header->setHttpHeaders($accept, $statusCode);
			}
			}
			else{
				$statusCode = 404;
				$header->setHttpHeaders($accept, $statusCode);
			}
		}
		else{
			$statusCode = 404;
			$header->setHttpHeaders($accept, $statusCode);
		}
	}
	
	
	
	function GET_EMPLOYEE_DATA($EMPID)
	{
		$EMP = explode(',',$EMPID);
		$EMPDATA = array();
		
		for($i=0;$i<count($EMP);$i++)
		{
			$ID = $EMP[$i];
			$DATA = $this->GET_EMPLOYEE_SINGLE($ID);
			
			$EMPDATA[$i]['name'] = $DATA[0]['emp_name'];
			$EMPDATA[$i]['dept'] = $DATA[0]['dept_id'];
			$EMPDATA[$i]['code'] = $DATA[0]['employee_code'];
			$EMPDATA[$i]['desi'] = $DATA[0]['designation'];
			$EMPDATA[$i]['email'] = $DATA[0]['email'];
			$EMPDATA[$i]['dob'] = $DATA[0]['birth_date'];
			$EMPDATA[$i]['company_id'] = $DATA[0]['company_id'];
			
			
			
		}
		
		
		return $EMPDATA;
		
	}
	
	
	function GET_DATA_FROM_LOGIN($PUNM,$PCPW)
	{
		$sql="select pro_mst_user.employee_id,pro_mst_user.session_id,pro_mst_user.user_id,pro_mst_user.ws_token,pro_mst_user.user_name,pro_mst_user.role_id,pro_mst_user_role.role_code,pro_mst_user_role.role_name from pro_mst_user INNER JOIN pro_mst_user_role ON pro_mst_user.role_id=pro_mst_user_role.role_id where pro_mst_user.user_name='".$PUNM."' and pro_mst_user.password='".md5($PCPW)."'";
		
		
		//echo $sql;
		
		global $row;
		$evt_name=array();
			$db_query=$row->query($sql);
			while($client_result=$row->next_record())
			{
				$record = array();
				foreach(array_keys($client_result) as $key)
				{
				if(gettype($key)=="string")
					{
						$record[$key] = stripslashes(stripslashes($client_result[$key]));
					}
				}
				$evt_name[]=$record;
			}
			return $evt_name;	
	}
	
	
	function Find_USR_DATA_NEW($PUNM,$PCPW)
	{
		$sql="select * from pro_mst_user where user_name='".$PUNM."' and password='".md5($PCPW)."'";
		
		global $row;
		$evt_name=array();
			$db_query=$row->query($sql);
			while($client_result=$row->next_record())
			{
				$record = array();
				foreach(array_keys($client_result) as $key)
				{
				if(gettype($key)=="string")
					{
						$record[$key] = stripslashes(stripslashes($client_result[$key]));
					}
				}
				$evt_name[]=$record;
			}
			return $evt_name;	
	}

}
?>