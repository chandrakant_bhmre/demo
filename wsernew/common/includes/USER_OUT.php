<?php
class USER_OUT extends utils{
	
	function WS_logout(){
		global $row, $header, $user_data, $data;
		
		$accept = 'application/json';
		$json = "";
		
		if(isset($data['PUID']) && isset($data['PUTY']) && isset($data['PUTOKEN'])){
			
			$PUID = $this->no_injection(filter_var($data['PUID'], FILTER_SANITIZE_NUMBER_INT));
			$PUTY = $this->no_injection(filter_var($data['PUTY'], FILTER_SANITIZE_STRING));
			$PUTOKEN = $this->no_injection(filter_var($data['PUTOKEN'], FILTER_SANITIZE_STRING));
			
			if(!empty($PUID) && !empty($PUTY) && !empty($PUTOKEN)){
				
				$success=$this->PROCEDURE_CALLING_FUNCTION_NEW($PUID,$PUTOKEN);
				
				if(isset($success)){
					
					if(count($success)== 1){
						$sql_up="update pro_mst_user set session_id=NULL,ws_token=NULL where user_id='".$PUID."' and ws_token='".$PUTOKEN."'";
					$db_query = $row->query($sql_up);
					$affected_row = $row->affected_rows();	
						
						
						$json = "{";
						$json = $json . "\"XSTS\":\"1\"";
						$json = $json . "}";
						return $json;
					}
					else
					{
						$json = "{";
						$json = $json . "\"XSTS\":\"0\"";
						$json = $json . "}";
						return $json;
					}
					return $json;
				}
				else
				{
					$json = "{";
					$json = $json . "\"XSTS\":\"0\"";
					$json = $json . "}";
					return $json;
				}
			}
			else
			{
				$statusCode = 404;
				$header->setHttpHeaders($accept, $statusCode);
			}
		}
		else{
			$statusCode = 404;
			$header->setHttpHeaders($accept, $statusCode);
		}
	}
	
	function PROCEDURE_CALLING_FUNCTION_NEW($PUID,$PUTOKEN)
	{
		$sql="select count(*) from pro_mst_user where user_id='".$PUID."' and ws_token='".$PUTOKEN."'";
		
		global $row;
		$evt_name=array();
			$db_query=$row->query($sql);
			while($client_result=$row->next_record())
			{
				$record = array();
				foreach(array_keys($client_result) as $key)
				{
				if(gettype($key)=="string")
					{
						$record[$key] = stripslashes(stripslashes($client_result[$key]));
					}
				}
				$evt_name[]=$record;
			}
			return $evt_name;	
	}
}
?>