<?php
class GET_BOOK_COUNT extends utils 
{
	function FUNCTION_GET_BOOK_COUNT()
	{
		global $row, $header;
		$json_input = file_get_contents("php://input");
		//$json_input=$_REQUEST['LOGIN_AUTH'];//To run from postman
		$data = json_decode($json_input, true);
		
			if(isset($data['P_UID']) && isset($data['P_TOKEN'])){
				
			$P_UID = filter_var($data['P_UID'], FILTER_SANITIZE_NUMBER_INT); 
			$P_TOKEN = filter_var($data['P_TOKEN'], FILTER_SANITIZE_STRING);
						
			if(!empty($P_UID) && !empty($P_TOKEN))
			{
				$TOKEN_VALIDATE = $this->WS_GET_TOKEN_VALIDATE($P_TOKEN,$P_UID);
				
				if(count($TOKEN_VALIDATE)==1)
				{
					$MARKETING_ARRAY = $this->WS_GET_MARKETING_LIST();
					
					
						$json = "{";
						$json = $json . "\"X_STS\":\"1\",";
						$json = $json . "\"X_MSG\":\"Book list found\",";
						$json = $json . "\"X_BOOK_LIST\":";
						$json = $json . "[";
						$json = $json . "{";
							$json = $json . "\"X_BOOKCNT\":\"".count($MARKETING_ARRAY)."\"";
														
							$json = $json . "}";
						$json = $json . "]";
					$json = $json . "}";
						return $json;
				}
				else
				{
					$json = "{";
						$json = $json . "\"X_STS\":\"0\"";
						$json = $json . "}";
						echo $json;
					$statusCode = 401;
					$header->setHttpHeaders($accept, $statusCode);
					
				}
			}
			else
			{
				$json = "{";
						$json = $json . "\"X_STS\":\"0\",";
						$json = $json . "}";
						echo $json;
				$statusCode = 404;
				$header->setHttpHeaders($accept, $statusCode);
			}
		}
		else{
			$json = "{";
						$json = $json . "\"X_STS\":\"0\"";
						$json = $json . "}";
						echo $json;
			$statusCode = 404;
			$header->setHttpHeaders($accept, $statusCode);
		}
	}
	
	function WS_GET_MARKETING_LIST()
	{
		global $row;
		$evt_name=array();
			$sql="SELECT b_id from pro_mst_book WHERE active='y' ";

				
		$db_query=$row->query($sql);
		while($client_result=$row->next_record())
		{
			$record = array();
			foreach(array_keys($client_result) as $key)
			{
			if(gettype($key)=="string")
				{
					$record[$key] = stripslashes(stripslashes($client_result[$key]));
				}
			}
			$evt_name[]=$record;
		}
		return $evt_name;	
	}
	
	
	
}
?>