<?php
class BOOK_RETURN_UPDATE extends utils 
{
	
	function FUNCTION_BOOK_RETURN_UPDATE()
	{
		global $row, $header;
		$json_input = file_get_contents("php://input");
		//$json_input=$_REQUEST['LOGIN_AUTH'];
		$data = json_decode($json_input, true);
		
		//echo '<pre>';print_r($data);echo '</pre>';
		//exit;
		$accept = 'application/json';
		
			if(isset($data['P_UID']) && isset($data['P_TOKEN']) && isset($data['P_RENTID']) && isset($data['P_RETURNDT']))
			{
			
			$P_UID = filter_var($data['P_UID'], FILTER_SANITIZE_NUMBER_INT);
			$P_TOKEN = filter_var($data['P_TOKEN'], FILTER_SANITIZE_STRING); 
			$P_RENTID = filter_var($data['P_RENTID'], FILTER_SANITIZE_NUMBER_INT); 
			$P_RETURNDT = filter_var($data['P_RETURNDT'], FILTER_SANITIZE_STRING);
							
			
			if(!empty($P_UID) && !empty($P_TOKEN) && !empty($P_RENTID) && !empty($P_RETURNDT))
			{
				
				$TOKEN_VALIDATE = $this->WS_GET_TOKEN_VALIDATE($P_TOKEN,$P_UID);
				
				if(count($TOKEN_VALIDATE)==1)
				{
					/*$CHK_DUP = $this->FUNCTION_DUPLICATE_BOOK($P_BOOKNM,$P_BOOKID);
					if(count($CHK_DUP)==0)
					{*/
					$RESULT = $this->FUNCTION_UPDATE_BOOK_DATA($P_UID,$P_RENTID,$P_RETURNDT);
					
														
					if(count($RESULT) != 0){
						
						$json = "{";
						$json = $json . "\"X_STS\":\"1\",";
						$json = $json . "\"X_MSG\":\"Book return successfully.\"";
						$json = $json . "}";
						echo $json;
						
						$statusCode = 200;
						$header->setHttpHeaders($accept, $statusCode);
					
						}
					else{
						$json = "{";
						$json = $json . "\"X_STS\":\"0\",";
						$json = $json . "\"X_MSG\":\"Book return updatation error.\"";
						$json = $json . "}";
						echo $json;
					}
				/*}
				else
				{
					$json = "{";
						$json = $json . "\"X_STS\":\"0\",";
						$json = $json . "\"X_MSG\":\"Book duplicate Found!\"";
						$json = $json . "}";
						echo $json;
					$statusCode = 401;
					$header->setHttpHeaders($accept, $statusCode);
					
				}*/	
				
				}
				else
				{
					$json = "{";
						$json = $json . "\"X_STS\":\"0\",";
						$json = $json . "\"X_MSG\":\"Invalid token.\"";
						$json = $json . "}";
						echo $json;
					$statusCode = 401;
					$header->setHttpHeaders($accept, $statusCode);
					
				}
			}
			else{
				$json = "{";
						$json = $json . "\"X_STS\":\"0\",";
						$json = $json . "\"X_MSG\":\"Empty not allowed.\"";
						$json = $json . "}";
						echo $json;
				$statusCode = 404;
				$header->setHttpHeaders($accept, $statusCode);
			}
		}
		else{
			$json = "{";
						$json = $json . "\"X_STS\":\"0\",";
						$json = $json . "\"X_MSG\":\"Parameter is not set.\"";
						$json = $json . "}";
						echo $json;
			$statusCode = 404;
			$header->setHttpHeaders($accept, $statusCode);
		}
	}
	
	function FUNCTION_UPDATE_BOOK_DATA($P_UID,$P_RENTID,$P_RETURNDT)
	{
		global $row;
		$evt_name=array();
				
		$sql = "UPDATE `pro_trn_rent` set
							`return_date`='".date("Y-m-d", strtotime($P_RETURNDT))."',
							`modified_by`='".$P_UID."',
							`modified_on`='".date('Y-m-d H:i:s')."'";
		$sql.=" WHERE rent_id='".$P_RENTID."'";
	 	//echo $sql;exit;
		$db_query = $row->query($sql);
		$affected_row = $row->affected_rows();

		return $affected_row;
	}
	
	/*function FUNCTION_DUPLICATE_BOOK($P_BOOKNM,$P_BOOKID)
	{
		global $row;
		$evt_name=array();
		$sql="SELECT b_id FROM pro_mst_book where book_name='".$P_BOOKNM."' AND b_id!=".$P_BOOKID."";
		$db_query=$row->query($sql);
		while($client_result=$row->next_record())
		{
			$record = array();
			foreach(array_keys($client_result) as $key)
			{
			if(gettype($key)=="string")
				{
					$record[$key] = stripslashes(stripslashes($client_result[$key]));
				}
			}
			$evt_name[]=$record;
		}
		return $evt_name;	
	}*/
}
?>