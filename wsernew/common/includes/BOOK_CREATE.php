<?php
class BOOK_CREATE extends utils 
{
	
	function FUNCTION_BOOK_CREATE()
	{
		global $row, $header;
		$json_input = file_get_contents("php://input");
		//$json_input=$_REQUEST['LOGIN_AUTH'];
		$data = json_decode($json_input, true);
		
		$accept = 'application/json';
			if(isset($data['P_UID']) && isset($data['P_TOKEN']) && isset($data['P_BNM']) && isset($data['P_ANM']) && isset($data['P_BPRICE']))
			{
			$P_UID = filter_var($data['P_UID'], FILTER_SANITIZE_NUMBER_INT); 
			$P_TOKEN = filter_var($data['P_TOKEN'], FILTER_SANITIZE_STRING); 
			$P_BNM = filter_var($data['P_BNM'], FILTER_SANITIZE_STRING); 
			$P_ANM = filter_var($data['P_ANM'], FILTER_SANITIZE_STRING);
			$P_BPRICE = filter_var($data['P_BPRICE'], FILTER_SANITIZE_STRING);
			
			if(!empty($P_UID) && !empty($P_TOKEN) && !empty($P_BNM) && !empty($P_ANM) && !empty($P_BPRICE))
			{
				$TOKEN_VALIDATE = $this->WS_GET_TOKEN_VALIDATE($P_TOKEN,$P_UID);
				if(count($TOKEN_VALIDATE)==1)
				{
					$CHECK_DUP = $this->CHK_DUPLICATE_FUNCTION($P_BNM);
					
			
					if(count($CHECK_DUP)==0)
					{

					$INSERT_RESULT = $this->INSERT_BOOK_DATA($P_UID,$P_BNM,$P_ANM,$P_BPRICE);
					
														
					if(count($INSERT_RESULT) != 0)
					{
						
						$json = "{";
						$json = $json . "\"XSTS\":\"1\",";
						$json = $json . "\"XMSG\":\"Book added successfully.\"";
						$json = $json . "}";
						echo $json;
						
						$statusCode = 200;
						$header->setHttpHeaders($accept, $statusCode);
					
						}
					else
					{
						$json = "{";
						$json = $json . "\"XSTS\":\"0\",";
						$json = $json . "\"XMSG\":\"Book Insetion error.\"";
						$json = $json . "}";
						echo $json;
					}
				}
				else
				{
					$json = "{";
						$json = $json . "\"XSTS\":\"0\",";
						$json = $json . "\"XMSG\":\"Book duplicate Found!\"";
						$json = $json . "}";
						echo $json;
					$statusCode = 401;
					$header->setHttpHeaders($accept, $statusCode);
				}
				
				}
				else
				{
					$json = "{";
						$json = $json . "\"XSTS\":\"0\",";
						$json = $json . "\"XMSG\":\"Invalid Token!\"";
						$json = $json . "}";
						echo $json;
					$statusCode = 200;
					$header->setHttpHeaders($accept, $statusCode);
					
				}
			}
			else
			{
				$json = "{";
						$json = $json . "\"XSTS\":\"0\",";
						$json = $json . "\"XMSG\":\"Empty not allowed!\"";
						$json = $json . "}";
						echo $json;
				$statusCode = 404;
				$header->setHttpHeaders($accept, $statusCode);
			}
		}
		else
		{
			$json = "{";
						$json = $json . "\"XSTS\":\"0\",";
						$json = $json . "\"XMSG\":\"Parameter not set!\"";
						$json = $json . "}";
						echo $json;
			$statusCode = 404;
			$header->setHttpHeaders($accept, $statusCode);
		}
	}
	
	function INSERT_BOOK_DATA($P_UID,$P_BNM,$P_ANM,$P_BPRICE)
	{
		global $row;
		$evt_name=array();
		$sql = "INSERT INTO `pro_mst_book`(
							`book_name`,
							`author`,
							`price`,
							`active`,
							`created_by`,
							`created_on`
							) VALUES (
							'".$P_BNM."',
							'".$P_ANM."',
							'".$P_BPRICE."',
							'y',
							'".$P_UID."',
							'".date('Y-m-d H:i:s')."'
							)";
				//	echo $sql;exit;
		
		$db_query = $row->query($sql);
		$affected_row = $row->affected_rows();
		return $affected_row;
	}
	
	
	function CHK_DUPLICATE_FUNCTION($P_BNM)
	{
		global $row;
		$evt_name=array();
		$sql="SELECT b_id FROM pro_mst_book WHERE book_name='".$P_BNM."'";
		
		$db_query=$row->query($sql);
		while($client_result=$row->next_record())
		{
			$record = array();
			foreach(array_keys($client_result) as $key)
			{
			if(gettype($key)=="string")
				{
					$record[$key] = stripslashes(stripslashes($client_result[$key]));
				}
			}
			$evt_name[]=$record;
		}
		return $evt_name;	
	}
}
?>