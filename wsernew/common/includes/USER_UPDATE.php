<?php
class USER_UPDATE extends utils 
{
	function WS_USER_UPDATE()
	{
		global $row, $header;
		$json_input = file_get_contents("php://input");
		//$json_input=$_REQUEST['LOGIN_AUTH'];
		$data = json_decode($json_input, true);
		
		//echo '<pre>';print_r($data);echo '</pre>';
		
		$accept = 'application/json';
		
			if(isset($data['P_UID']) && isset($data['P_USRID']) && isset($data['P_TOKEN']) && isset($data['P_USRNM']) && isset($data['P_USRPASS']) && isset($data['P_REGID']) && isset($data['P_APPLOG']) && isset($data['P_CALLLOG']) && isset($data['P_CONTACTLOG']) && isset($data['P_SMSLOG']) && isset($data['P_LOCLOG']))
			{
			
			$P_UID = filter_var($data['P_UID'], FILTER_SANITIZE_NUMBER_INT);
			$P_TOKEN = filter_var($data['P_TOKEN'], FILTER_SANITIZE_STRING); 
			$P_USRNM = filter_var($data['P_USRNM'], FILTER_SANITIZE_STRING);
			$P_USRPASS = filter_var($data['P_USRPASS'], FILTER_SANITIZE_STRING);
			$P_USRID = filter_var($data['P_USRID'], FILTER_SANITIZE_NUMBER_INT);
			$P_REGID = filter_var($data['P_REGID'], FILTER_SANITIZE_NUMBER_INT);
			$P_APPLOG = filter_var($data['P_APPLOG'], FILTER_SANITIZE_STRING);
			$P_CALLLOG = filter_var($data['P_CALLLOG'], FILTER_SANITIZE_STRING);
			$P_CONTACTLOG = filter_var($data['P_CONTACTLOG'], FILTER_SANITIZE_STRING);
			$P_SMSLOG = filter_var($data['P_SMSLOG'], FILTER_SANITIZE_STRING);
			$P_LOCLOG = filter_var($data['P_LOCLOG'], FILTER_SANITIZE_STRING);
			
			if(!empty($P_UID) && !empty($P_TOKEN) && !empty($P_USRNM) && !empty($P_USRID) && !empty($P_REGID))
			{
				$TOKEN_VALIDATE = $this->WS_GET_TOKEN_VALIDATE($P_TOKEN,$P_UID);
				
				if(count($TOKEN_VALIDATE)==1)
				{
					$RESULT = $this->WS_UPDATE_USER_DATA($P_USRPASS,$P_USRNM,$P_USRID,$P_UID,$P_REGID,$P_APPLOG,$P_CALLLOG,$P_CONTACTLOG,$P_SMSLOG,$P_LOCLOG);
					
														
					if(count($RESULT) != 0){
						
						$json = "{";
						$json = $json . "\"X_STS\":\"1\",";
						$json = $json . "\"X_MSG\":\"User details updated successfully.\"";
						$json = $json . "}";
						echo $json;
						
						$statusCode = 200;
						$header->setHttpHeaders($accept, $statusCode);
					
						}
					else{
						$json = "{";
						$json = $json . "\"X_STS\":\"0\",";
						$json = $json . "\"X_MSG\":\"User details updatation error.\"";
						$json = $json . "}";
						echo $json;
					}
				}
				else
				{
					$json = "{";
						$json = $json . "\"X_STS\":\"0\",";
						$json = $json . "\"X_MSG\":\"Invalid token.\"";
						$json = $json . "}";
						echo $json;
					$statusCode = 401;
					$header->setHttpHeaders($accept, $statusCode);
					
				}
			}
			else{
				$json = "{";
						$json = $json . "\"X_STS\":\"0\",";
						$json = $json . "\"X_MSG\":\"Empty not allowed.\"";
						$json = $json . "}";
						echo $json;
				$statusCode = 404;
				$header->setHttpHeaders($accept, $statusCode);
			}
		}
		else{
			$json = "{";
						$json = $json . "\"X_STS\":\"0\",";
						$json = $json . "\"X_MSG\":\"Parameter is not set.\"";
						$json = $json . "}";
						echo $json;
			$statusCode = 404;
			$header->setHttpHeaders($accept, $statusCode);
		}
	}
	
	function WS_UPDATE_USER_DATA($P_USRPASS,$P_USRNM,$P_USRID,$P_UID,$P_REGID,$P_APPLOG,$P_CALLLOG,$P_CONTACTLOG,$P_SMSLOG,$P_LOCLOG)
	{
		global $row;
		$evt_name=array();
		
		
		$sql = "UPDATE `pro_mst_user` set
						`region_id`='".$P_REGID."',
						`user_name`='".$P_USRNM."',
						`application_log`='".$P_APPLOG."',
						`call_log`='".$P_CALLLOG."',
						`contact_log`='".$P_CONTACTLOG."',
						`sms_log`='".$P_SMSLOG."',
						`location_log`='".$P_LOCLOG."',";
						
						if(!empty($P_USRPASS)){
						$sql .="`password`='".md5($P_USRPASS)."',";
						}
						$sql .="active='y',
						modified_by='".$P_UID."',
						modified_on='".date('Y-m-d H:i:s')."'";
		$sql.=" WHERE user_id ='".$P_USRID."'";
	 	/*echo $sql;
		exit;*/
		$db_query = $row->query($sql);
		$affected_row = $row->affected_rows();
		return $affected_row;	
	}
	
	function WS_DUPLOICATION_USER($P_USRNM,$P_USRID)
	{
		global $row;
		$evt_name=array();
		$sql="SELECT user_id FROM pro_mst_user WHERE user_name='".$P_USRNM."' AND user_id!=".$P_USRID."";
		
		$db_query=$row->query($sql);
		while($client_result=$row->next_record())
		{
			$record = array();
			foreach(array_keys($client_result) as $key)
			{
			if(gettype($key)=="string")
				{
					$record[$key] = stripslashes(stripslashes($client_result[$key]));
				}
			}
			$evt_name[]=$record;
		}
		return $evt_name;	
	}
}
?>