<?php 
	//error_reporting(0);
	include("../includes/client_config.php");
	include("../database/connection.php");
	include("../includes/server_config.php");

	include("../includes/utils.php");
		
	$header = new ResponseHeaderRest;
	$row= new DB;
	
	$valid = $header->authorizedAccess();
	if($valid){
		
		global $new_token;
		include("includes/PRO_USER_LOGIN.php");
		
		//Object is created
		$login = new PRO_USER_LOGIN;
	
		//Function is called
		$status = $login->FUNCTION_USER_LOGIN();
		
		header("Content-Type: application/json");
		header("token-id: ".$new_token);
		echo $status; 	 //echo json
		$row->close(); //connection close
	}
?>