<?php
class utils {
	
	function WS_GET_TOKEN_VALIDATE($P_TOKEN,$P_UID)
	{
		global $row;
		$evt_name=array();
		
				$sql="SELECT * FROM pro_mst_user WHERE session_id='".$P_TOKEN."' AND u_id='".$P_UID."'";
			
				$db_query=$row->query($sql);
		while($client_result=$row->next_record())
		{
			$record = array();
			foreach(array_keys($client_result) as $key)
			{
			if(gettype($key)=="string")
				{
					$record[$key] = stripslashes(stripslashes($client_result[$key]));
				}
			}
			$evt_name[]=$record;
		}
		return $evt_name;	
	}
	
	
	function GET_SINGLE_USER($P_USRID)
	{
		global $row;
		$evt_name=array();
		
		$sql="SELECT first_name,last_name FROM pro_mst_user where u_id=".$P_USRID."";
		
		$db_query=$row->query($sql);
		while($client_result=$row->next_record())
		{
			$record = array();
			foreach(array_keys($client_result) as $key)
			{
			if(gettype($key)=="string")
				{
					$record[$key] = stripslashes(stripslashes($client_result[$key]));
				}
			}
			$evt_name[]=$record;
		}
		return $evt_name;	
	}
	
	//Book Name Find
	function GET_SINGLE_BOOKNM($P_BID)
	{
		global $row;
		$evt_name=array();
		
		$sql="SELECT book_name,author FROM pro_mst_book where b_id=".$P_BID."";
		
		$db_query=$row->query($sql);
		while($client_result=$row->next_record())
		{
			$record = array();
			foreach(array_keys($client_result) as $key)
			{
			if(gettype($key)=="string")
				{
					$record[$key] = stripslashes(stripslashes($client_result[$key]));
				}
			}
			$evt_name[]=$record;
		}
		return $evt_name;	
	}
	
		
	public function randomString($length = 15) {
		$str = "";
		$characters = array_merge(range('A','Z'), range('a','z'), range('0','9'));
		$max = count($characters) - 1;
		for ($i = 0; $i < $length; $i++) {
			$rand = mt_rand(0, $max);
			$str .= $characters[$rand];
		}
		return $str;
	}
	
	public function randomStringPassword($length = 8) {
		$str = "";
		$characters = array_merge(range('A','Z'), range('0','9'));
		$max = count($characters) - 1;
		for ($i = 0; $i < $length; $i++) {
			$rand = mt_rand(0, $max);
			$str .= $characters[$rand];
		}
		return $str;
	}
	
		
	######### Check Injection ################
	function no_injection($string){
		$string = trim($string);				// remove spaces from start and end 
		//$string = mysqli_real_escape_string ($string);	// Escape a string for query
		return $string;
	}
	
	function sql_injection($string){
		$string = trim($string);
		$string = addslashes($string);
		return $string;
	}
	######### End of Check Injection ################
}
?>