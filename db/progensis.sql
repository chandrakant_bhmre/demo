-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 02, 2022 at 03:37 AM
-- Server version: 5.7.14
-- PHP Version: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `progensis`
--

-- --------------------------------------------------------

--
-- Table structure for table `pro_mst_book`
--

CREATE TABLE `pro_mst_book` (
  `b_id` int(11) NOT NULL,
  `book_name` varchar(255) DEFAULT NULL,
  `author` varchar(255) NOT NULL,
  `cover_image_string` varchar(255) DEFAULT NULL,
  `price` float(10,2) DEFAULT NULL,
  `active` char(1) NOT NULL DEFAULT 'y',
  `created_by` int(11) NOT NULL,
  `created_on` datetime NOT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `tabletimestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='book table';

--
-- Dumping data for table `pro_mst_book`
--

INSERT INTO `pro_mst_book` (`b_id`, `book_name`, `author`, `cover_image_string`, `price`, `active`, `created_by`, `created_on`, `modified_by`, `modified_on`, `tabletimestamp`) VALUES
(1, 'mrutyunjay', 'Shivaji Sawant', '', 450.00, 'y', 1, '2022-03-01 00:00:00', 1, '2022-03-02 07:08:09', '2022-03-01 15:19:24'),
(2, 'Yugandhara', 'Shivaji Sawant', NULL, 230.00, 'y', 1, '2022-03-01 22:01:24', 1, '2022-03-01 22:01:45', '2022-03-01 16:31:24');

-- --------------------------------------------------------

--
-- Table structure for table `pro_mst_user`
--

CREATE TABLE `pro_mst_user` (
  `u_id` int(11) NOT NULL,
  `first_name` varchar(50) DEFAULT NULL COMMENT 'task provide 255',
  `last_name` varchar(50) DEFAULT NULL COMMENT 'task provide 255',
  `mobile_no` varchar(10) DEFAULT NULL COMMENT 'int error show out of boud message',
  `email` varchar(255) DEFAULT NULL,
  `age` tinyint(3) DEFAULT NULL,
  `gender` enum('m','f','o') NOT NULL,
  `city` varchar(50) NOT NULL COMMENT 'task provide 255',
  `password` varchar(50) NOT NULL COMMENT 'task provide 255',
  `session_id` varchar(100) DEFAULT NULL,
  `active` char(1) NOT NULL DEFAULT 'y',
  `created_by` int(11) DEFAULT NULL,
  `created_on` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `table_time_stamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pro_mst_user`
--

INSERT INTO `pro_mst_user` (`u_id`, `first_name`, `last_name`, `mobile_no`, `email`, `age`, `gender`, `city`, `password`, `session_id`, `active`, `created_by`, `created_on`, `modified_by`, `modified_on`, `table_time_stamp`) VALUES
(1, 'Chandrakant R', 'Bhamare', '7755993416', 'chandrakant.bhmre@gmail.com', 35, 'm', 'Nashik', '5f4dcc3b5aa765d61d8327deb882cf99', '6WRvK3qjBUovr1Js8Kon9VVhxTScrtdJML3', 'y', 1, '2022-03-01 00:00:00', 1, '2022-03-02 08:13:39', '2022-03-01 08:11:49'),
(4, 'Ravindra', 'Bhamare', '9921423089', 'ds@gmail.com', 59, 'm', 'NAshik', 'fe86bd2ba496a597c65ac0f24142697a', 'F1QNLNKrA5xO3RUaKVjO75tGh3TBvvdZdOB', 'y', 1, '2022-03-02 09:00:20', 4, '2022-03-02 09:00:43', '2022-03-02 03:30:20');

-- --------------------------------------------------------

--
-- Table structure for table `pro_trn_rent`
--

CREATE TABLE `pro_trn_rent` (
  `rent_id` int(11) NOT NULL,
  `u_id` int(11) NOT NULL,
  `book_id` int(11) NOT NULL,
  `issue_date` date DEFAULT NULL,
  `return_date` date DEFAULT NULL,
  `book_rent` float(10,2) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `created_on` datetime NOT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `tabletimestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pro_trn_rent`
--

INSERT INTO `pro_trn_rent` (`rent_id`, `u_id`, `book_id`, `issue_date`, `return_date`, `book_rent`, `created_by`, `created_on`, `modified_by`, `modified_on`, `tabletimestamp`) VALUES
(1, 1, 1, '2022-03-01', '2022-03-02', 50.00, 1, '2022-03-01 00:00:00', 1, '2022-03-02 06:39:46', '2022-03-01 17:01:21'),
(3, 1, 2, '2022-03-01', '2022-03-02', 20.00, 1, '2022-03-02 06:06:35', NULL, NULL, '2022-03-02 00:36:35'),
(4, 1, 1, '2022-03-02', NULL, 20.00, 1, '2022-03-02 06:52:27', NULL, NULL, '2022-03-02 01:22:27');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `pro_mst_book`
--
ALTER TABLE `pro_mst_book`
  ADD PRIMARY KEY (`b_id`);

--
-- Indexes for table `pro_mst_user`
--
ALTER TABLE `pro_mst_user`
  ADD PRIMARY KEY (`u_id`),
  ADD UNIQUE KEY `mobile_no` (`mobile_no`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `pro_trn_rent`
--
ALTER TABLE `pro_trn_rent`
  ADD PRIMARY KEY (`rent_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `pro_mst_book`
--
ALTER TABLE `pro_mst_book`
  MODIFY `b_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `pro_mst_user`
--
ALTER TABLE `pro_mst_user`
  MODIFY `u_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `pro_trn_rent`
--
ALTER TABLE `pro_trn_rent`
  MODIFY `rent_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
