//product js
		$(document).ready(function() {
			pageSetUp();
			
			$.validator.addMethod("alphanumeric", function(value, element) { //alert(value);
    return this.optional(element) || /^[a-zA-Z0-9 ]+$/i.test(value); 
	/////^\w+$/		
}, "Special characters not allowed");
			
			
			//******************Profile ADD VALIDATION ********************/
			var $ProfileEditForm = $('#profile-edit-form').validate({
				ignore: ":hidden",   
			// Rules for form validation
				rules : {
					first_name : {
						required : true,
						accept: "[a-zA-Z -/.]"
					},
					last_name : {
						required : true,
						accept: "[a-zA-Z -/.]"
					},
					mobile_no : {
						required : true,
						accept: "[0-9]"
					},
					email : {
						required : true,
						accept: "[a-zA-Z0-9 -/.]"
					},
					city : {
						required : true
					},
					age : {
						required : true,
						accept: "[0-9]"
					},
					gender : {
						required : true
					}
					
					},
		
				// Messages for form validation
				messages : {
					first_name :{
						required : 'Please enter first name.',
						accept:"Accecpt only a to z-/. special character."
					},
					last_name : {
						required : 'Please enter last name.',
						accept:"Accecpt only a to z-/. special character."
					},
					mobile_no : {
						required : 'Please enter mobile.',
						accept:"Accecpt only numbers and (.) special character."
					},
					email : {
						required : 'Please enter email.',
						accept:"Accecpt a-zA-Z0-9 -/."
					},
					city : {
						required : 'Please enter city.',
						accept:"Accecpt a-zA-Z -/."
					},
					age : {
						required : 'Please enter city.',
						accept:"Accecpt 0-9."
					},
					gender : {
						required : 'Please select gender.',
						accept:"Accecpt 0-9."
					}
				},
				 submitHandler: function(form) {
					checkProfileEdit();
					return false;
				 },
		
				// Do not change code below
				errorPlacement : function(error, element) {
					error.insertAfter(element.parent());
				}
			});
			//*******************Profile edit VALIDATION END ***************/
			
			/************CHECK Profile edit VALIDATION********************/
			function checkProfileEdit()
			{
				$('#btnbookedit').prop('disabled', true);
				$("#btnbookedit").removeClass('btn-primary');
				$("#btnbookedit").html('Submitting..');
				$("#btnbookedit").addClass('btn-success disabled');
				
				var data = $('#profile-edit-form').serialize();
				//alert(data);
				
				$.ajax({
						 type: "POST",
						 url: site_path + "admin/user/userprofileedit_save", 
						 type : 'POST',
						 data : data,
							 success: 
							  function(resp){ //alert(resp);
								d=JSON.parse(resp);//alert(d.error);
								//alert(d.file_name);
								if(d.X_STS!=1)//not valid
								{
										 $.SmartMessageBox({
											title : "Alert!",
											content : d.X_MSG,
											buttons : '[Ok]'
										}, function(ButtonPressed) {
											if (ButtonPressed === "Ok") 
											{
												$("#btnbookedit").addClass('btn-primary');
												$("#btnbookedit").html('Update');
												$("#btnbookedit").removeClass('btn-success disabled');
												$('#btnbookedit').prop('disabled', false);
											}
											
										});
									
								}//if !=1
								else
								{ 
								showMessages('success',d.X_MSG,'btnbookedit','Update book','admin/home');
									//window.location.href=site_path + "admin/project";
									
								}//else
								
								
						  }
						});	
				return false;
			}

			
			//TABLES
				var responsiveHelper_dt_employee = undefined;
												
				var breakpointDefinition = {
					tablet : 1024,
					phone : 480
				};
	
				// User master  list view function start 
			$('#dt_employee').dataTable({
					"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>"+
						"t"+
						"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
					"autoWidth" : true,
			        "oLanguage": {
					    "sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
					},
   				    "ajax": site_path + 'admin/user/getdata',
					"preDrawCallback" : function() {
						// Initialize the responsive datatables helper once.
						if (!responsiveHelper_dt_employee) {
							responsiveHelper_dt_employee = new ResponsiveDatatablesHelper($('#dt_employee'), breakpointDefinition);
						}
					},
					"rowCallback" : function(nRow) {
						responsiveHelper_dt_employee.createExpandIcon(nRow);
					},
					"drawCallback" : function(oSettings) {
						responsiveHelper_dt_employee.respond();
					}
				});
			// User master  list view function Closed 
		});
