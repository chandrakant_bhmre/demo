		$(document).ready(function() {
			
			
			
			pageSetUp();
			
			$.validator.addMethod("alphanumeric", function(value, element) { //alert(value);
    return this.optional(element) || /^[a-zA-Z0-9 ]+$/i.test(value); 
	/////^\w+$/		
}, "Special characters not allowed");
			
			//******************Book ADD VALIDATION ********************/
			var $projectaddForm = $('#book-add-form').validate({
				ignore: [],    
			// Rules for form validation
				rules : {
					book_name : {
						required : true,
						accept: "[a-zA-Z0-9 -/.]"
					},
					author_name : {
						required : true,
						accept: "[a-zA-Z0-9 -/.]"
					},
					book_amount : {
						required : true,
						accept: "[0-9 .]"
					}
					
					},
		
				// Messages for form validation
				messages : {
					book_name :{
						required : 'Please enter book name.',
						accept:"Accecpt only -/. special character."
					},
					author_name : {
						required : 'Please enter auther name.',
						accept:"Accecpt only -/. special character."
					},
					book_amount : {
						required : 'Please enter price.',
						accept:"Accecpt only numbers and (.) special character."
					}
					
				},
				 submitHandler: function(form) {
					checkBookadd();
					return false;
				 },
		
				// Do not change code below
				errorPlacement : function(error, element) {
					error.insertAfter(element.parent());
				}
			});
			//*******************Book VALIDATION END ***************/
			
			/************CHECK Book VALIDATION********************/
			function checkBookadd()
			{
				$('#btnbookadd').prop('disabled', true);
				$("#btnbookadd").removeClass('btn-primary');
				$("#btnbookadd").html('Submitting..');
				$("#btnbookadd").addClass('btn-success disabled');
				
				var data = $('#book-add-form').serialize();
				
				
				$.ajax({
						 type: "POST",
						 url: site_path + "admin/book/bookadd_save", 
						 type : 'POST',
						 data : data,
							 success: 
							  function(resp){ //alert(resp);
								d=JSON.parse(resp);//alert(d.error);
								//alert(d.file_name);
								if(d.XSTS!=1)//not valid
								{
										 $.SmartMessageBox({
											title : "Alert!",
											content : d.XMSG,
											buttons : '[Ok]'
										}, function(ButtonPressed) {
											if (ButtonPressed === "Ok") 
											{
												$("#btnbookadd").addClass('btn-primary');
												$("#btnbookadd").html('Add');
												$("#btnbookadd").removeClass('btn-success disabled');
												$('#btnbookadd').prop('disabled', false);
											}
											
										});
									
								}//if !=1
								else
								{ 
									window.location.href=site_path + "admin/book";
									
								}//else
								
								
						  }
						});	
				return false;
			}
			
			
			//******************Book ADD VALIDATION ********************/
			var $BookEditForm = $('#book-edit-form').validate({
				ignore: ":hidden",   
			// Rules for form validation
				rules : {
					book_name : {
						required : true,
						accept: "[a-zA-Z0-9 -/.]"
					},
					author_name : {
						required : true,
						accept: "[a-zA-Z0-9 -/.]"
					},
					book_amount : {
						required : true,
						accept: "[0-9 .]"
					}
					
					},
		
				// Messages for form validation
				messages : {
					book_name :{
						required : 'Please enter book name.',
						accept:"Accecpt only -/. special character."
					},
					author_name : {
						required : 'Please enter auther name.',
						accept:"Accecpt only -/. special character."
					},
					book_amount : {
						required : 'Please enter price.',
						accept:"Accecpt only numbers and (.) special character."
					}
				},
				 submitHandler: function(form) {
					checkBookEdit();
					return false;
				 },
		
				// Do not change code below
				errorPlacement : function(error, element) {
					error.insertAfter(element.parent());
				}
			});
			//*******************Book edit VALIDATION END ***************/
			
			/************CHECK Book edit VALIDATION********************/
			function checkBookEdit()
			{
				$('#btnbookedit').prop('disabled', true);
				$("#btnbookedit").removeClass('btn-primary');
				$("#btnbookedit").html('Submitting..');
				$("#btnbookedit").addClass('btn-success disabled');
				
				var data = $('#book-edit-form').serialize();
				//alert(data);
				
				$.ajax({
						 type: "POST",
						 url: site_path + "admin/book/bookedit_save", 
						 type : 'POST',
						 data : data,
							 success: 
							  function(resp){ //alert(resp);
								d=JSON.parse(resp);//alert(d.error);
								//alert(d.file_name);
								if(d.X_STS!=1)//not valid
								{
										 $.SmartMessageBox({
											title : "Alert!",
											content : d.X_MSG,
											buttons : '[Ok]'
										}, function(ButtonPressed) {
											if (ButtonPressed === "Ok") 
											{
												$("#btnbookedit").addClass('btn-primary');
												$("#btnbookedit").html('Update');
												$("#btnbookedit").removeClass('btn-success disabled');
												$('#btnbookedit').prop('disabled', false);
											}
											
										});
									
								}//if !=1
								else
								{ 
								showMessages('success',d.X_MSG,'btnbookedit','Update book','admin/book');
									//window.location.href=site_path + "admin/project";
									
								}//else
								
								
						  }
						});	
				return false;
			}

		
		
			

			/* BASIC ;*/
				var responsiveHelper_dt_book_list = undefined;
				
												
				var breakpointDefinition = {
					tablet : 1024,
					phone : 480
				};
	
				// Book list
				$('#dt_book_list').dataTable({
					"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>"+
						"t"+
						"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
					"autoWidth" : true,
			        "oLanguage": {
					    "sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
					},
   				    "ajax": site_path + 'admin/book/getdata',
					"preDrawCallback" : function() {
						// Initialize the responsive datatables helper once.
						if (!responsiveHelper_dt_book_list) {
							responsiveHelper_dt_book_list = new ResponsiveDatatablesHelper($('#dt_book_list'), breakpointDefinition);
						}
					},
					"rowCallback" : function(nRow) {
						responsiveHelper_dt_book_list.createExpandIcon(nRow);
					},
					"drawCallback" : function(oSettings) {
						responsiveHelper_dt_book_list.respond();
					}
				});
				
				
				
		});
		
	

		/**********DELETE book type******************/
			function project_delete(opid)
			{ 
			//alert(desigid);
				$.SmartMessageBox({
					title : "Alert!",
					content : 'Are you sure to delete this record?',
					buttons : '[Ok][Cancel]'
				}, function(ButtonPressed) {
					if (ButtonPressed === "Ok") 
					{
						$.ajax({
						 type: "POST",
						 url: site_path + "commonfunctionController/project_delete", 
						  data: {opid : opid},
						 cache:false,
						 success: 
							  function(data){ //alert(data);
								
								if(data==1)
								{	window.location.reload();
								}
								
						  }
						});	
					}
					else 
					{
						return false;
					}
					
		
				});
				return false;
			}
			/************DELETE Book type END *********/