		
		// DO NOT REMOVE : GLOBAL FUNCTIONS!
		// site path declear by chandrakant bhamare
		//localhost
		 var site_path ="http://localhost/progensis/";
		
				
		$(document).ready(function() {
			pageSetUp();
			// dashboard tabs fregments
			$('#tabs').tabs();
		
			$(".alert-danger").fadeTo(2000, 1800).slideUp(1800, function(){
				$(".alert-danger").slideUp(1800);
			});
			
			$(".alert-success").fadeTo(2000, 1800).slideUp(1800, function(){
				$(".alert-success").slideUp(1800);
			});
			
			$('#myModal').on('shown.bs.modal', function () {
    $('#enter_otp').focus();
}) ;

$("#checkallPayout").click(function () {
        $('#dt_cli_list tbody tr:visible input[type="checkbox"]').prop('checked', this.checked);
    });

		
$.validator.addMethod("alphabet", function(value, element) { //alert(value);
    return this.optional(element) || /^[a-zA-Z ]+$/i.test(value); 
	/////^\w+$/		
}, "Special characters & number not allowed");

$.validator.addMethod("alphanumeric", function(value, element) { //alert(value);
    return this.optional(element) || /^[a-zA-Z0-9- ]+$/i.test(value); 
	/////^\w+$/		
}, "Special characters & spaces not allowed");

jQuery.validator.addMethod("noSpace", function(value, element) { 
  return value.indexOf(" ") < 0 && value != ""; 
}, "No space please and don't leave it empty");

jQuery.validator.addMethod("validate_email", function(value, element) {
    if (/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(value)) { 
        return true;
    } else {
        return false;
    }
}, "Please enter a valid Email.");

jQuery.validator.addMethod("accept", function(value, element, param) { 
  return value.match(new RegExp("." + param + "$"));
});
			
			$(".js-status-update a").click(function() {
					var selText = $(this).text();
					var $this = $(this);
					$this.parents('.btn-group').find('.dropdown-toggle').html(selText + ' <span class="caret"></span>');
					$this.parents('.dropdown-menu').find('li').removeClass('active');
					$this.parent().addClass('active');
				});

				/*
				* TODO: add a way to add more todo's to list
				*/

				// initialize sortable
				$(function() {
					$("#sortable1, #sortable2").sortable({
						handle : '.handle',
						connectWith : ".todo",
						update : countTasks
					}).disableSelection();
				});

				// check and uncheck
				$('.todo .checkbox > input[type="checkbox"]').click(function() {
					var $this = $(this).parent().parent().parent();

					if ($(this).prop('checked')) {
						$this.addClass("complete");

						// remove this if you want to undo a check list once checked
						//$(this).attr("disabled", true);
						$(this).parent().hide();

						// once clicked - add class, copy to memory then remove and add to sortable3
						$this.slideUp(500, function() {
							$this.clone().prependTo("#sortable3").effect("highlight", {}, 800);
							$this.remove();
							countTasks();
						});
					} else {
						// insert undo code here...
					}

				})
				// count tasks
				function countTasks() {

					$('.todo-group-title').each(function() {
						var $this = $(this);
						$this.find(".num-of-tasks").text($this.next().find("li").size());
					});

				}


				/* hide default buttons */
				$('.fc-toolbar .fc-right, .fc-toolbar .fc-center').hide();

				/*
				 * CHAT
				 */

				$.filter_input = $('#filter-chat-list');
				$.chat_users_container = $('#chat-container > .chat-list-body')
				$.chat_users = $('#chat-users')
				$.chat_list_btn = $('#chat-container > .chat-list-open-close');
				$.chat_body = $('#chat-body');

				/*
				* LIST FILTER (CHAT)
				*/

				// custom css expression for a case-insensitive contains()
				jQuery.expr[':'].Contains = function(a, i, m) {
					return (a.textContent || a.innerText || "").toUpperCase().indexOf(m[3].toUpperCase()) >= 0;
				};

				function listFilter(list) {// header is any element, list is an unordered list
					// create and add the filter form to the header

					$.filter_input.change(function() {
						var filter = $(this).val();
						if (filter) {
							// this finds all links in a list that contain the input,
							// and hide the ones not containing the input while showing the ones that do
							$.chat_users.find("a:not(:Contains(" + filter + "))").parent().slideUp();
							$.chat_users.find("a:Contains(" + filter + ")").parent().slideDown();
						} else {
							$.chat_users.find("li").slideDown();
						}
						return false;
					}).keyup(function() {
						// fire the above change event after every letter
						$(this).change();

					});

				}
				
			/******************CUSTOMER DOC ADD VALIDATION ******************/
			var $customerdocaddForm = $('#customerdoc-add-form').validate({
				ignore: ":hidden",
			// Rules for form validation
				rules : {
					client_id : {
						required : true
					},
					doc_file_name : {
						required : true
					},
					doc_name : {
						required : true,
						alphanumeric: true
					}
					
				},
		
				// Messages for form validation
				messages : {
					
					client_id : {
						required : 'Please select client'
					},
					doc_file_name : {
						required : 'Please select document'
					},
					doc_name : {
						required : 'Please enter document name',
						alphanumeric: 'Special characters not allowed'
					}					
				},
				 submitHandler: function(form) {
					checkCustomerDocument();
					return false;
				 },
		
				// Do not change code below
				errorPlacement : function(error, element) {
					error.insertAfter(element.parent());
				}
			});
			//*******************CUSTOMER VALIDATION END ***************/
			
			/************CHECK vendor PIN VALIDATION********************/
			function checkCustomerDocument()
			{
				$('#btnclidocadd').prop('disabled', true);
				$("#btnclidocadd").removeClass('btn-primary');
				$("#btnclidocadd").html('Submitting..');
				$("#btnclidocadd").addClass('btn-success disabled');
				
				var data = $('#customerdoc-add-form').serialize();
				//alert(new FormData($('#state-add-form')[0]));
				//alert(data);
				
				/**************IMAGE UPLOAD *********/
				var formData = new FormData( $("#customerdoc-add-form")[0] );
				
				$.ajax({
						 type: "POST",
						 url: site_path + "admin/customer/customerdoc_save", 
						 type : 'POST',
						data : formData,
						async : false,
						cache : false,
						contentType : false,
						processData : false,
						 success: 
							  function(resp){ //alert(resp);
								d=JSON.parse(resp);//alert(d.XSTS);
								//alert(d.file_name);
								if(d.XSTS!=1)//not valid
								{
										 $.SmartMessageBox({
											title : "Alert!",
											content : d.XMSG,
											buttons : '[Ok]'
										}, function(ButtonPressed) {
											if (ButtonPressed === "Ok") 
											{
												$("#btnclidocadd").addClass('btn-primary');
												$("#btnclidocadd").html('Add');
												$("#btnclidocadd").removeClass('btn-success disabled');
												$('#btnclidocadd').prop('disabled', false);
											}
											
										});
									
								}//if !=1
								else
								{ 
									showMessages('success',d.XMSG,'btnclidocadd','Add Document','admin/customer/customerdoc');
									
								}//else
								
								
						  }
						});	
				/****************IMG UPLOAD END********************/
				return false;
			}
			
			var $customerdoceditForm = $('#customerdoc-edit-form').validate({
				ignore: ":hidden",
			// Rules for form validation
				rules : {
					client_id : {
						required : true
					},
					doc_file_name : {
						required : true
					},
					doc_name : {
						required : true,
						alphanumeric: true
					}
					
				},
		
				// Messages for form validation
				messages : {
					
					client_id : {
						required : 'Please select client'
					},
					doc_file_name : {
						required : 'Please select document'
					},
					doc_name : {
						required : 'Please enter document name',
						alphanumeric: 'Special characters not allowed'
					}					
				},
				 submitHandler: function(form) {
					checkCustomerDocumentEdit();
					return false;
				 },
		
				// Do not change code below
				errorPlacement : function(error, element) {
					error.insertAfter(element.parent());
				}
			});
			
			function checkCustomerDocumentEdit()
			{
				$('#btnclidocedit').prop('disabled', true);
				$("#btnclidocedit").removeClass('btn-primary');
				$("#btnclidocedit").html('Submitting..');
				$("#btnclidocedit").addClass('btn-success disabled');
				
				var data = $('#customerdoc-edit-form').serialize();
				//alert(new FormData($('#state-add-form')[0]));
				//alert(data);
				
				/**************IMAGE UPLOAD *********/
				var formData = new FormData( $("#customerdoc-edit-form")[0] );
				
				$.ajax({
						 type: "POST",
						 url: site_path + "admin/customer/customerdocedit_save", 
						 type : 'POST',
						data : formData,
						async : false,
						cache : false,
						contentType : false,
						processData : false,
						 success: 
							  function(resp){ //alert(resp);
								d=JSON.parse(resp);//alert(d.XSTS);
								//alert(d.file_name);
								if(d.XSTS!=1)//not valid
								{
										 $.SmartMessageBox({
											title : "Alert!",
											content : d.XMSG,
											buttons : '[Ok]'
										}, function(ButtonPressed) {
											if (ButtonPressed === "Ok") 
											{
												$("#btnclidocedit").addClass('btn-primary');
												$("#btnclidocedit").html('Update Document');
												$("#btnclidocedit").removeClass('btn-success disabled');
												$('#btnclidocedit').prop('disabled', false);
											}
											
										});
									
								}//if !=1
								else
								{ 
									showMessages('success',d.XMSG,'btnclidocedit','Update Document','admin/customer/customerdoc');
									
								}//else
								
								
						  }
						});	

				return false;
			}
			//**************************CUSTOMER DOC VALIDATION END ***********************/
				
			
			//******************CUSTOMER ADD VALIDATION ********************/
			var $customeraddForm = $('#customer-add-form').validate({
				ignore: ":hidden",
			// Rules for form validation
				rules : {
					company_id : {
						required : true
					},cust_name : {
						required : true,
						alphabet: true
					},
					cust_code : {
						required : true,
						//alphanumeric: true
						accept: "[a-zA-Z0-9 ,-/]"
					},
					cust_type : {
						required : true
					},
					cust_address : {
						required : true,
						//alphanumeric: true
						accept: "[a-zA-Z0-9 ,-/.]"
					},
					state_id : {
						required : true
					},
					dist_id : {
						required : true
					},
					tl_id : {
						required : true
					},
					city_name : {
						required : true,
						alphabet: true
					},
					pin_code : {
						digits : true
					},
					desig_id : {
						required : true
					},
					c_per_name : {
						required : true,
						alphabet: true
					},
					c_per_mobile : {
						required : true,
						number : true,
						maxlength : 10,
						minlength : 10
					},
					spe_disc : {
						required : true,
						number : true,
						maxlength : 2,
						minlength : 2
					},
					prime_loc : {
						required : true
					},
					area_id : {
						required : true
					},
					mobile_no : {
						required : true,
						number : true,
						maxlength : 10,
						minlength : 10
					},bank_city : {
						required : true,
						alphabet: true
					}
					
				},
		
				// Messages for form validation
				messages : {
					
					company_id : {
						required : 'Please select company'
					},cust_name : {
						required : 'Please enter client name.',
						alphabet: 'Special characters and numbars not allowed.'
					},
					cust_code : {
						required : 'Please enter client code.',
						//alphanumeric: 'Special characters not allowed'
						accept:"Accecpt only -,/. special character."
					},
					cust_type : {
						required : 'Please select client type.'
					},
					cust_address : {
						required : 'Please enter client address.',
						//alphanumeric: 'Special characters not allowed'
						accept:"Accecpt only -,/. special character."
					},
					state_id : {
						required : 'Please select state.'
					},
					dist_id : {
						required : 'Please select district.'
					},
					tl_id : {
						required : 'Please select Taluka.'
					},
					city_name : {
						required : 'Please enter city name.',
						alphabet: 'Special characters and numbers not allowed.'
					},
					pin_code : {
						digits : 'Digits only please.'
					},
					desig_id : {
						required : 'Please select designation.'
					},
					c_per_name : {
						required : 'Please enter contact person name.',
						alphabet: 'Special characters and numbers not allowed.'
					},
					c_per_mobile : {
						required : 'Please enter mobile no.',
						number : 'Please enter numbers only.',
						maxlength : 'Please enter 10 digit number.',
						minlength : 'Please enter 10 digit number.'
					},
					spe_disc : {
						required : 'Please enter spacial discount.',
						number : 'Please enter numbers only.',
						maxlength : 'Please enter 2 digit number.',
						minlength : 'Please enter 2 digit number.'
					},
					prime_loc : {
						required : 'Please select prime location Yes/No.'
					},
					area_id : {
						required : 'Please select area.'
					},mobile_no : {
						required : 'Please enter whatsapp no.',
						number : 'Please enter numbers only.',
						maxlength : 'Please enter 10 digit number.',
						minlength : 'Please enter 10 digit number.'
					},bank_city : {
						required : 'Please enter designation.',
						alphabet: 'Special characters and numbars not allowed.'
					}
					
				},
				 submitHandler: function(form) {
					checkCustomer();
					return false;
				 },
		
				// Do not change code below
				errorPlacement : function(error, element) {
					error.insertAfter(element.parent());
				}
			});
			//*******************CUSTOMER VALIDATION END ***************/
			
			/************CHECK vendor PIN VALIDATION********************/
			function checkCustomer()
			{
				$('#btncustomeradd').prop('disabled', true);
				$("#btncustomeradd").removeClass('btn-primary');
				$("#btncustomeradd").html('Submitting..');
				$("#btncustomeradd").addClass('btn-success disabled');
				
				var data = $('#customer-add-form').serialize();
				//alert(new FormData($('#state-add-form')[0]));
				//alert(data);
				
				/**************IMAGE UPLOAD *********/
				var formData = new FormData( $("#customer-add-form")[0] );
				
				$.ajax({
						 type: "POST",
						 url: site_path + "commonfunctionController/do_upload", 
						 type : 'POST',
						data : formData,
						async : false,
						cache : false,
						contentType : false,
						processData : false,
						 success: 
							  function(resp){ //alert(resp);
								d=JSON.parse(resp);//alert(d.error);
								//alert(d.file_name);
								if(d.error!=undefined)//not valid
								{
										 $.SmartMessageBox({
											title : "Alert!",
											content : d.error,
											buttons : '[Ok]'
										}, function(ButtonPressed) {
											if (ButtonPressed === "Ok") 
											{
												$("#btncustomeradd").addClass('btn-primary');
												$("#btncustomeradd").html('Add');
												$("#btncustomeradd").removeClass('btn-success disabled');
												$('#btncustomeradd').prop('disabled', false);
											}
											
										});
									
								}//if !=1
								else
								{ 
									//save package
									saveCustomerData(d.upload_data['file_name']);
									
								}//else
								
								
						  }
						});	
				/****************IMG UPLOAD END********************/
				return false;
			}
			
			/*****customer DATAVALIDATE ********/
			function saveCustomerData()
			{
				var data = $('#customer-add-form').serialize();
				//alert('in save'+data);
				$.ajax({
						 type: "POST",
						 url: site_path + "admin/customer/customeradd_save", 
						 data: data,
						 cache:false,
						 success: 
							  function(resp){ //alert(resp);
								d=JSON.parse(resp);//alert(d.error);
								//alert(d.file_name);
								if(d.XSTS!=1)//not valid
								{
										 $.SmartMessageBox({
											title : "Alert!",
											content : d.XMSG,
											buttons : '[Ok]'
										}, function(ButtonPressed) {
											if (ButtonPressed === "Ok") 
											{
												$("#btncustomeradd").addClass('btn-primary');
												$("#btncustomeradd").html('Add');
												$("#btncustomeradd").removeClass('btn-success disabled');
												$('#btncustomeradd').prop('disabled', false);
											}
											
										});
									
								}//if !=1
								else
								{ 
								showMessages('success',d.XMSG,'btncustomeradd','New Client','admin/client');
									//window.location.href=site_path + "admin/client";
								}//else
							}
						});	
			}
			// save Customer data
			
			
			
			
			
			//******************LEAD ADD VALIDATION ********************/
			var $customeraddForm = $('#lead-add-form').validate({
				ignore: ":hidden",
			// Rules for form validation
				rules : {
					leadbyid : {
						required : true
					},
					cust_type : {
						required : true
					},
					spe_disc : {
						required : true
					},
					prime_loc : {
						required : true
					},
					area_id : {
						required : true
					},
					cust_name : {
						required : true,
						alphabet: true
					},
					leadby_name : {
						required : true,
						alphabet: true
					},
					cust_code : {
						required : true,
						//alphanumeric: true
						accept: "[a-zA-Z0-9 ,-/.]"
					},
					project_type_id : {
						required : true
					},
					project_category_id : {
						required : true
					},
					cust_address : {
						required : true,
						//alphanumeric: true
						accept: "[a-zA-Z0-9 ,-/.]"
					},
					state_id : {
						required : true
					},
					dist_id : {
						required : true
					},
					tl_id : {
						required : true
					},
					city_name : {
						required : true,
						alphanumeric: true
					},
					pin_code : {
						digits : true
					},
					desig_id : {
						required : true
					},
					c_per_name : {
						required : true,
						alphabet: true
					},
					c_per_mobile : {
						required : true,
						number : true,
						maxlength : 10,
						minlength : 10
					},
					mobile_no : {
						required : true,
						number : true,
						maxlength : 10,
						minlength : 10
					}
					
				},
		//
				// Messages for form validation
				messages : {
					leadbyid : {
						required : 'Please enter leadby.'
					},
					cust_type : {
						required : 'Please select type.'
					},
					spe_disc : {
						required : 'Please select discount Yes/No.'
					},
					prime_loc : {
						required : 'Please select prime location Yes/No.'
					},
					area_id : {
						required : 'Please select area.'
					},
					cust_name : {
						required : 'Please enter unit name.',
						alphabet: 'Special characters and numbers not allowed.'
					},
					leadby_name : {
						required : 'Please enter lead by name.',
						alphabet: 'Special characters and numbers not allowed.'
					},
					cust_code : {
						required : 'Please enter unit code.',
						//alphanumeric: 'Special characters not allowed.'
						accept : "Accecpt only -,/. special character"
					},
					project_type_id : {
						required : 'Please select Exp / New.'
					},
					project_category_id : {
						required : 'Please select industrial scale.'
					},
					cust_address : {
						required : 'Please enter customer address.',
						//alphanumeric: 'Special characters not allowed.'
						accept : "Accecpt only -,/. special character"
					},
					state_id : {
						required : 'Please select state.'
					},
					dist_id : {
						required : 'Please select district.'
					},
					tl_id : {
						required : 'Please select taluka.'
					},
					city_name : {
						required : 'Please enter city name.',
						alphanumeric: 'Special characters not allowed.'
					},
					pin_code : {
						digits : 'Digits only please.'
					},
					desig_id : {
						required : 'Please select designation.'
					},
					c_per_name : {
						required : 'Please enter contact person name.',
						alphabet: 'Special characters and numbers not allowed.'
					},
					c_per_mobile : {
						required : 'Please enter mobile no.',
						number : 'Please enter numbers only.',
						maxlength : 'Please enter 10 digit number.',
						minlength : 'Please enter 10 digit number.'
					},mobile_no : {
						required : 'Please enter whatsapp no.',
						number : 'Please enter numbers only.',
						maxlength : 'Please enter 10 digit number.',
						minlength : 'Please enter 10 digit number.'
					}
					
				},
				 submitHandler: function(form) {
					checkLead();
					return false;
				 },
		
				// Do not change code below
				errorPlacement : function(error, element) {
					error.insertAfter(element.parent());
				}
			});
			//*******************CUSTOMER VALIDATION END ***************/
			
			/************CHECK vendor PIN VALIDATION********************/
			function checkLead()
			{
				$('#btnleadadd').prop('disabled', true);
				$("#btnleadadd").removeClass('btn-primary');
				$("#btnleadadd").html('Submitting..');
				$("#btnleadadd").addClass('btn-success disabled');
				
				var data = $('#lead-add-form').serialize();
				//alert(new FormData($('#state-add-form')[0]));
				//alert(data);
				
				/**************IMAGE UPLOAD *********/
				var formData = new FormData( $("#lead-add-form")[0] );
				
				$.ajax({
						 type: "POST",
						 url: site_path + "commonfunctionController/do_upload", 
						 type : 'POST',
						data : formData,
						async : false,
						cache : false,
						contentType : false,
						processData : false,
						 success: 
							  function(resp){ //alert(resp);
								d=JSON.parse(resp);//alert(d.error);
								//alert(d.file_name);
								if(d.error!=undefined)//not valid
								{
										 $.SmartMessageBox({
											title : "Alert!",
											content : d.error,
											buttons : '[Ok]'
										}, function(ButtonPressed) {
											if (ButtonPressed === "Ok") 
											{
												$("#btnleadadd").addClass('btn-primary');
												$("#btnleadadd").html('Add');

												$("#btnleadadd").removeClass('btn-success disabled');
												$('#btnleadadd').prop('disabled', false);
											}
											
										});
									
								}//if !=1
								else
								{ 
									//save package
									saveLeadData(d.upload_data['file_name']);
									
								}//else
								
								
						  }
						});	
				/****************IMG UPLOAD END********************/
				return false;
			}
			
			/*****customer DATAVALIDATE ********/
			function saveLeadData()
			{
				var data = $('#lead-add-form').serialize();
				//alert('in save'+data);
				$.ajax({
						 type: "POST",
						 url: site_path + "admin/lead/leadadd_save", 
						 data: data,
						 cache:false,
						 success: 
							  function(resp){ //alert(resp);
								d=JSON.parse(resp);//alert(d.error);
								//alert(d.file_name);
								if(d.XSTS!=1)//not valid
								{
										 $.SmartMessageBox({
											title : "Alert!",
											content : d.XMSG,
											buttons : '[Ok]'
										}, function(ButtonPressed) {
											if (ButtonPressed === "Ok") 
											{
												$("#btnleadadd").addClass('btn-primary');
												$("#btnleadadd").html('Add');
												$("#btnleadadd").removeClass('btn-success disabled');
												$('#btnleadadd').prop('disabled', false);
											}
											
										});
									
								}//if !=1
								else
								{ 
								showMessages('success',d.XMSG,'btncustomeradd','New Lead','admin/lead');
									//window.location.href=site_path + "admin/client";
								}//else
							}
						});	
			}
			// save Customer data
			
						
			
			
			
			//******************Company EDIT VALIDATION ********************/
			var $companyeditForm = $('#company-update-form').validate({
			// Rules for form validation
				rules : {
					company_name : {
						required : true,
						alphanumeric: true
					},
					company_code : {
						required : true,
						alphanumeric: true
					},
					company_type : {
						required : true
					},
					comapny_address : {
						required : true,
						alphanumeric: true
					},
					state_id : {
						required : true
					},
					dist_id : {
						required : true
					},
					city_name : {
						required : true,
						alphanumeric: true
					},
					pin_code : {
						required : true,
						digits : true
					},
					mobile_no : {
						required : true,
						number : true,
						maxlength : 10,
						minlength : 10
					},
					email : {
						email : true
					},
					phone_no : {
						number : true,
						maxlength : 12,
						minlength : 10
					},
					contact_person_name : {
						required : true,
						alphanumeric: true
					},
					contact_person_mobile_no : {
						required : true,
						number : true,
						maxlength : 10,
						minlength : 10
					}			
				},
		
				// Messages for form validation
				messages : {
					
					company_name : {
						required : 'Please enter company name',
						alphanumeric: 'Special characters not allowed'
					},
					company_code : {
						required : 'Please enter company code',
						alphanumeric: 'Special characters not allowed'
					},
					company_type : {
						required : 'Please select company type'
					},
					company_address : {
						required : 'Please enter company address',
						alphanumeric: 'Special characters not allowed'
					},
					state_id : {
						required : 'Please select state'
					},
					dist_id : {
						required : 'Please select district'
					},
					city_name : {
						required : 'Please enter city name',
						alphanumeric: 'Special characters not allowed'
					},
					pin_code : {
						required : 'Please enter pin code',
						digits : 'Digits only please'
					},
					mobile_no : {
						required : 'Please enter mobile no',
						number : 'Please enter numbers only',
						maxlength : 'Please enter 10 digit number',
						minlength : 'Please enter 10 digit number'
					},
					email : {
						required : 'Please enter email address',
						email : 'Please enter valid email'
					},
					phone_no : {
						number : 'Please enter numbers only',
						maxlength : 'Please enter valid contact number',
						minlength : 'Please enter valid contact number'
					},
					contact_person_name : {
						required : 'Please enter contact Person name',
						alphanumeric: 'Special characters not allowed'
					},
					contact_person_mobile_no : {
						required : 'Please enter mobile no',
						number : 'Please enter numbers only',
						maxlength : 'Please enter 10 digit number',
						minlength : 'Please enter 10 digit number'
					}
				},
				 submitHandler: function(form) {
					return checkcompany_edit();
					//return false;
				 },
		
				// Do not change code below
				errorPlacement : function(error, element) {
					error.insertAfter(element.parent());
				}
			});
			
			function checkcompany_edit()
			{
				$('#btncomapnyupdate').prop('disabled', true);
				$("#btncomapnyupdate").removeClass('btn-primary');
				$("#btncomapnyupdate").html('Submitting..');
				$("#btncomapnyupdate").addClass('btn-success disabled');
				
				return true;
			}
			//*******************Company Update VALIDATION END ***************/
			
			
			
			//******************CUSTOMER EDIT VALIDATION ********************/
			var $customereditForm = $('#customer-update-form').validate({
			// Rules for form validation
				rules : {
					cust_name : {
						required : true,
						alphabet: true
					},
					cust_code : {
						required : true,
						//alphanumeric: true
						accept: "[a-zA-Z0-9 ,-/]"
					},
					cust_type : {
						required : true
					},
					cust_address : {
						required : true,
						//alphanumeric: true
						accept: "[a-zA-Z0-9 ,-/.]"
					},
					state_id : {
						required : true
					},
					dist_id : {
						required : true
					},
					tl_id : {
						required : true
					},
					city_name : {
						required : true,
						alphabet: true
					},
					pin_code : {
						digits : true
					},
					desig_id : {
						required : true
					},
					c_per_name : {
						required : true,
						alphabet: true
					},
					c_per_mobile : {
						required : true,
						number : true,
						maxlength : 10,
						minlength : 10
					},
					spe_disc : {
						required : true,
						number : true,
						maxlength : 2,
						minlength : 2
					},
					prime_loc : {
						required : true
					},
					area_id : {
						required : true
					},
					mobile_no : {
						required : true,
						number : true,
						maxlength : 10,
						minlength : 10
					},bank_city : {
						required : true,
						alphabet: true
					}	
					
				},
		
				// Messages for form validation
				messages : {
					
					cust_name : {
						required : 'Please enter client name.',
						alphabet: 'Special characters and numbars not allowed.'
					},
					cust_code : {
						required : 'Please enter client code.',
						//alphanumeric: 'Special characters not allowed'
						accept:"Accecpt only -,/. special character."
					},
					cust_type : {
						required : 'Please select client type.'
					},
					cust_address : {
						required : 'Please enter client address.',
						//alphanumeric: 'Special characters not allowed'
						accept:"Accecpt only -,/. special character."
					},
					state_id : {
						required : 'Please select state.'
					},
					dist_id : {
						required : 'Please select district.'
					},
					tl_id : {
						required : 'Please select Taluka.'
					},
					city_name : {
						required : 'Please enter city name.',
						alphabet: 'Special characters and numbers not allowed.'
					},
					pin_code : {
						digits : 'Digits only please.'
					},
					desig_id : {
						required : 'Please select designation.'
					},
					c_per_name : {
						required : 'Please enter contact person name.',
						alphabet: 'Special characters and numbers not allowed.'
					},
					c_per_mobile : {
						required : 'Please enter mobile no.',
						number : 'Please enter numbers only.',
						maxlength : 'Please enter 10 digit number.',
						minlength : 'Please enter 10 digit number.'
					},
					spe_disc : {
						required : 'Please enter special descount.',
						number : 'Please enter numbers only.',
						maxlength : 'Please enter 2 digit number.',
						minlength : 'Please enter 2 digit number.'
					},
					prime_loc : {
						required : 'Please select prime location Yes/No.'
					},
					area_id : {
						required : 'Please select area.'
					},mobile_no : {
						required : 'Please enter whatsapp no.',
						number : 'Please enter numbers only.',
						maxlength : 'Please enter 10 digit number.',
						minlength : 'Please enter 10 digit number.'
					},bank_city : {
						required : 'Please enter desgnation.',
						alphabet: 'Special characters and numbars not allowed.'
					}
					
				},
				 submitHandler: function(form) {
					return checkcustomer_edit();
					//return false;
				 },
		
				// Do not change code below
				errorPlacement : function(error, element) {
					error.insertAfter(element.parent());
				}
			});
			
			function checkcustomer_edit()
			{
				$('#btncustomerupdate').prop('disabled', true);
				$("#btncustomerupdate").removeClass('btn-primary');
				$("#btncustomerupdate").html('Submitting..');
				$("#btncustomerupdate").addClass('btn-success disabled');
				
				var data = $('#customer-update-form').serialize();
				
				$.ajax({
						 type: "POST",
						 url: site_path + "admin/customer/customeredit_save", 
						 type : 'POST',
						 data : data,
						 success: 
							  function(resp){ //alert(resp);
								d=JSON.parse(resp);
								
								if(d.X_STS!=1)//not valid
								{
										 $.SmartMessageBox({
											title : "Alert!",
											content : d.X_MSG,
											buttons : '[Ok]'
										}, function(ButtonPressed) {
											if (ButtonPressed === "Ok") 
											{
												$("#btncustomerupdate").addClass('btn-primary');
												$("#btncustomerupdate").html('Update');
												$("#btncustomerupdate").removeClass('btn-success disabled');
												$('#btncustomerupdate').prop('disabled', false);
											}
											
										});
									
								}//if !=1
								else
								{ 
									showMessages('success',d.X_MSG,'btncustomerupdate','Update','admin/client');
								}//else
								
								
						  }
						});
				
				return false;
			}
			//*******************CUSTOMER Update VALIDATION END ***************/
			
			
			//******************Lead EDIT VALIDATION ********************/
			var $leadeditForm = $('#lead-update-form').validate({
			// Rules for form validation
				rules : {
					leadbyid : {
						required : true
					},
					cust_type : {
						required : true
					},
					spe_disc : {
						required : true
					},
					prime_loc : {
						required : true
					},
					area_id : {
						required : true
					},
					cust_name : {
						required : true,
						alphabet: true
					},
					leadby_name : {
						required : true,
						alphabet: true
					},
					cust_code : {
						required : true,
						//alphanumeric: true
						accept: "[a-zA-Z0-9 ,-/.]"
					},
					project_type_id : {
						required : true
					},
					project_category_id : {
						required : true
					},
					cust_address : {
						required : true,
						//alphanumeric: true
						accept: "[a-zA-Z0-9 ,-/.]"
					},
					state_id : {
						required : true
					},
					dist_id : {
						required : true
					},
					tl_id : {
						required : true
					},
					city_name : {
						required : true,
						alphanumeric: true
					},
					pin_code : {
						digits : true
					},
					desig_id : {
						required : true
					},
					c_per_name : {
						required : true,
						alphabet: true
					},
					c_per_mobile : {
						required : true,
						number : true,
						maxlength : 10,
						minlength : 10
					},
					mobile_no : {
						required : true,
						number : true,
						maxlength : 10,
						minlength : 10
					}
					
				},
		//
				// Messages for form validation
				messages : {
					leadbyid : {
						required : 'Please enter leadby.'
					},
					cust_type : {
						required : 'Please select type.'
					},
					spe_disc : {
						required : 'Please select discount Yes/No.'
					},
					prime_loc : {
						required : 'Please select prime location Yes/No.'
					},
					area_id : {
						required : 'Please select area.'
					},
					cust_name : {
						required : 'Please enter unit name.',
						alphabet: 'Special characters and numbers not allowed.'
					},
					leadby_name : {
						required : 'Please enter lead by name.',
						alphabet: 'Special characters and numbers not allowed.'
					},
					cust_code : {
						required : 'Please enter unit code.',
						//alphanumeric: 'Special characters not allowed.'
						accept : "Accecpt only -,/. special character"
					},
					project_type_id : {
						required : 'Please select Exp / New.'
					},
					project_category_id : {
						required : 'Please select industrial scale.'
					},
					cust_address : {
						required : 'Please enter customer address.',
						//alphanumeric: 'Special characters not allowed.'
						accept : "Accecpt only -,/. special character"
					},
					state_id : {
						required : 'Please select state.'
					},
					dist_id : {
						required : 'Please select district.'
					},
					tl_id : {
						required : 'Please select taluka.'
					},
					city_name : {
						required : 'Please enter city name.',
						alphanumeric: 'Special characters not allowed.'
					},
					pin_code : {
						digits : 'Digits only please.'
					},
					desig_id : {
						required : 'Please select designation.'
					},
					c_per_name : {
						required : 'Please enter contact person name.',
						alphabet: 'Special characters and numbers not allowed.'
					},
					c_per_mobile : {
						required : 'Please enter mobile no.',
						number : 'Please enter numbers only.',
						maxlength : 'Please enter 10 digit number.',
						minlength : 'Please enter 10 digit number.'
					},mobile_no : {
						required : 'Please enter whatsapp no.',
						number : 'Please enter numbers only.',
						maxlength : 'Please enter 10 digit number.',
						minlength : 'Please enter 10 digit number.'
					}
				},
				 submitHandler: function(form) {
					return checklead_edit();
					//return false;
				 },
		
				// Do not change code below
				errorPlacement : function(error, element) {
					error.insertAfter(element.parent());
				}
			});
			
			function checklead_edit()
			{
				$('#btnleadupdate').prop('disabled', true);
				$("#btnleadupdate").removeClass('btn-primary');
				$("#btnleadupdate").html('Submitting..');
				$("#btnleadupdate").addClass('btn-success disabled');
				
				var data = $('#lead-update-form').serialize();
				
				$.ajax({
						 type: "POST",
						 url: site_path + "admin/lead/leadedit_save", 
						 type : 'POST',
						 data : data,
						 success: 
							  function(resp){ //alert(resp);
								d=JSON.parse(resp);
								
								if(d.X_STS!=1)//not valid
								{
										 $.SmartMessageBox({
											title : "Alert!",
											content : d.X_MSG,
											buttons : '[Ok]'
										}, function(ButtonPressed) {
											if (ButtonPressed === "Ok") 
											{
												$("#btnleadupdate").addClass('btn-primary');
												$("#btnleadupdate").html('Update');
												$("#btnleadupdate").removeClass('btn-success disabled');
												$('#btnleadupdate').prop('disabled', false);
											}
											
										});
									
								}//if !=1
								else
								{ 
									showMessages('success',d.X_MSG,'btnleadupdate','Update','admin/lead');
								}//else
								
								
						  }
						});
				
				return false;
			}
			//*******************Lead Update VALIDATION END ***************/
			
			
			var $changepassForm = $("#admin-changepass").validate({
				// Rules for form validation
				rules : {
					old_password : {
						required : true
					},
					new_password : {
						required : true,
						minlength : 6,
						maxlength : 20
					},
					confirm_password : {
						required : true,
						minlength : 6,
						maxlength : 20,
						equalTo : '#new_password'
					}
				},
	
				// Messages for form validation
				messages : {
					old_password : {
						required : 'Please enter your password'
					},
					new_password : {
						required : 'Please enter your new password'
					},
					confirm_password : {
						required : 'Please enter your password one more time',
						equalTo : 'Please enter the same password as above'
					}
				},
				 submitHandler: function(form) {
					checkoldpass();
					return false;
				 },
	
				// Do not change code below
				errorPlacement : function(error, element) {
					error.insertAfter(element.parent());
				}
			});
			
			var $loginForm = $("#login-form").validate({
				// Rules for form validation
				rules : {
					email : {
						required : true,
						email : true
					},
					password : {
						required : true,
						minlength : 3,
						maxlength : 20
					}
				},
	
				// Messages for form validation
				messages : {
					email : {
						required : 'Please enter your email address',
						email : 'Please enter a VALID email address'
					},
					password : {
						required : 'Please enter your password'
					}
				},
	
				// Do not change code below
				errorPlacement : function(error, element) {
					error.insertAfter(element.parent());
				}
			});
	
				
			// START AND FINISH DATE
			$('#startdate').datepicker({
				dateFormat : 'dd.mm.yy',
				autoSize: true,
				changeMonth: true,
				changeYear: true,
				prevText : '<i class="fa fa-chevron-left"></i>',
				nextText : '<i class="fa fa-chevron-right"></i>',
				onSelect : function(selectedDate) {
					$('#finishdate').datepicker('option', 'minDate', selectedDate);
				}
			});
			
			$('#finishdate').datepicker({
				dateFormat : 'dd.mm.yy',
				prevText : '<i class="fa fa-chevron-left"></i>',
				nextText : '<i class="fa fa-chevron-right"></i>',
				onSelect : function(selectedDate) {
					$('#startdate').datepicker('option', 'maxDate', selectedDate);
				}
			});
			
			//Profile page datepickers
			
			$('#birth_date').datepicker({
				dateFormat : 'dd.mm.yy',
				autoSize: true,
				changeMonth: true,
				changeYear: true,
				yearRange: '1950:2050',
				prevText : '<i class="fa fa-chevron-left"></i>',
				nextText : '<i class="fa fa-chevron-right"></i>'
			});
			
			$('#date_of_app').datepicker({
				dateFormat : 'dd.mm.yy',
				autoSize: true,
				changeMonth: true,
				changeYear: true,
				yearRange: '1950:2050',
				prevText : '<i class="fa fa-chevron-left"></i>',
				nextText : '<i class="fa fa-chevron-right"></i>'
			});
			
			$('#upto_date').datepicker({
				dateFormat : 'dd.mm.yy',
				autoSize: true,
				changeMonth: true,
				changeYear: true,
				yearRange: '1950:2050',
				prevText : '<i class="fa fa-chevron-left"></i>',
				nextText : '<i class="fa fa-chevron-right"></i>'
			});
			
			$('#from_date').datepicker({
				dateFormat : 'dd.mm.yy',
				autoSize: true,
				changeMonth: true,
				changeYear: true,
				yearRange: '1950:2050',
				prevText : '<i class="fa fa-chevron-left"></i>',
				nextText : '<i class="fa fa-chevron-right"></i>'
			});
			
			$('#to_date').datepicker({
				dateFormat : 'dd.mm.yy',
				autoSize: true,
				changeMonth: true,
				changeYear: true,
				yearRange: '1950:2050',
				prevText : '<i class="fa fa-chevron-left"></i>',
				nextText : '<i class="fa fa-chevron-right"></i>'
			});
			
			$('#anniversary_date').datepicker({
				dateFormat : 'dd.mm.yy',
				autoSize: true,
				changeMonth: true,
				changeYear: true,
				yearRange: '1950:2050',
				prevText : '<i class="fa fa-chevron-left"></i>',
				nextText : '<i class="fa fa-chevron-right"></i>'
			});
			
			$('#marriage_date').datepicker({
				dateFormat : 'dd.mm.yy',
				autoSize: true,
				changeMonth: true,
				changeYear: true,
				yearRange: '1950:2050',
				prevText : '<i class="fa fa-chevron-left"></i>',
				nextText : '<i class="fa fa-chevron-right"></i>'
			});
			
			$('#join_date').datepicker({
				dateFormat : 'dd.mm.yy',
				autoSize: true,
				changeMonth: true,
				changeYear: true,
				yearRange: '1950:2050',
				prevText : '<i class="fa fa-chevron-left"></i>',
				nextText : '<i class="fa fa-chevron-right"></i>'
			});
			
			
			
			
			$('#close_date').datepicker({
				dateFormat : 'dd.mm.yy',
				autoSize: true,
				changeMonth: true,
				changeYear: true,
				yearRange: '1950:2050',
				prevText : '<i class="fa fa-chevron-left"></i>',
				nextText : '<i class="fa fa-chevron-right"></i>'
			});
			
						
			$('#ec_date_update').datepicker({
				dateFormat : 'dd.mm.yy',
				autoSize: true,
				changeMonth: true,
				changeYear: true,
				yearRange: '2010:2050',
				prevText : '<i class="fa fa-chevron-left"></i>',
				nextText : '<i class="fa fa-chevron-right"></i>'
			});

			
			/* // DOM Position key index //
		
			l - Length changing (dropdown)
			f - Filtering input (search)
			t - The Table! (datatable)
			i - Information (records)
			p - Pagination (paging)
			r - pRocessing 
			< and > - div elements
			<"#id" and > - div with an id
			<"class" and > - div with a class
			<"#id.class" and > - div with an id and class
			
			Also see: http://legacy.datatables.net/usage/features
			*/	
	
			/* BASIC ;*/
				var responsiveHelper_dt_ajax = undefined;
				var responsiveHelper_dt_basic = undefined;
				var responsiveHelper_dt_city_list = undefined;
				var responsiveHelper_dt_customer_list = undefined;
				var responsiveHelper_dt_region_list = undefined;
				var responsiveHelper_dt_regiondist_list = undefined;
				var responsiveHelper_dt_lead_list = undefined;
				var responsiveHelper_dt_checklist_list = undefined;
				var responsiveHelper_dt_service_list = undefined;	
				var responsiveHelper_dt_clidoc_list = undefined;
								
				var breakpointDefinition = {
					tablet : 1024,
					phone : 480
				};
				
				
				$('#dt_basic_ajax').dataTable({
					"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>"+
						"t"+
						"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
					"autoWidth" : true,
			        "oLanguage": {
					    "sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
					},
					"preDrawCallback" : function() {
						// Initialize the responsive datatables helper once.
						if (!responsiveHelper_dt_ajax) {
							responsiveHelper_dt_ajax = new ResponsiveDatatablesHelper($('#dt_basic_ajax'), breakpointDefinition);
						}
					},
					"rowCallback" : function(nRow) {
						responsiveHelper_dt_ajax.createExpandIcon(nRow);
					},
					"drawCallback" : function(oSettings) {
						responsiveHelper_dt_ajax.respond();
					}
				});
				
				
				$('#dt_regiondist_list').dataTable({
					"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>"+
						"t"+
						"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
					"autoWidth" : true,
			        "oLanguage": {
					    "sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
					},
					"preDrawCallback" : function() {
						// Initialize the responsive datatables helper once.
						if (!responsiveHelper_dt_regiondist_list) {
							responsiveHelper_dt_regiondist_list = new ResponsiveDatatablesHelper($('#dt_regiondist_list'), breakpointDefinition);
						}
					},
					"rowCallback" : function(nRow) {
						responsiveHelper_dt_regiondist_list.createExpandIcon(nRow);
					},
					"drawCallback" : function(oSettings) {
						responsiveHelper_dt_regiondist_list.respond();
					}
				});
				
	
				$('#dt_basic').dataTable({
					"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>"+
						"t"+
						"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
					"autoWidth" : true,
			        "oLanguage": {
					    "sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
					},
   				    "ajax": site_path + 'admin/vendor/getdata',
					"preDrawCallback" : function() {
						// Initialize the responsive datatables helper once.
						if (!responsiveHelper_dt_basic) {
							responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
						}
					},
					"rowCallback" : function(nRow) {
						responsiveHelper_dt_basic.createExpandIcon(nRow);
					},
					"drawCallback" : function(oSettings) {
						responsiveHelper_dt_basic.respond();
					}
				});
				
				$('#dt_clidoc_list').dataTable({
					"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>"+
						"t"+
						"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
					"autoWidth" : true,
			        "oLanguage": {
					    "sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
					},
   				    "ajax": site_path + 'admin/Customer/getdatadoc',
					"preDrawCallback" : function() {
						// Initialize the responsive datatables helper once.
						if (!responsiveHelper_dt_clidoc_list) {
							responsiveHelper_dt_clidoc_list = new ResponsiveDatatablesHelper($('#dt_clidoc_list'), breakpointDefinition);
						}
					},
					"rowCallback" : function(nRow) {
						responsiveHelper_dt_clidoc_list.createExpandIcon(nRow);
					},
					"drawCallback" : function(oSettings) {
						responsiveHelper_dt_clidoc_list.respond();
					}
				});
				
						
	/* END BASIC */
			// Lead master  list view function start 
			$('#dt_lead_list').dataTable({
					"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>"+
						"t"+
						"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
					"autoWidth" : true,
			        "oLanguage": {
					    "sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
					},
   				    "ajax": site_path + 'admin/lead/getdata',
					"preDrawCallback" : function() {
						// Initialize the responsive datatables helper once.
						if (!responsiveHelper_dt_lead_list) {
							responsiveHelper_dt_lead_list = new ResponsiveDatatablesHelper($('#dt_lead_list'), breakpointDefinition);
						}
					},
					"rowCallback" : function(nRow) {
						responsiveHelper_dt_lead_list.createExpandIcon(nRow);
					},
					"drawCallback" : function(oSettings) {
						responsiveHelper_dt_lead_list.respond();
					}
				});
			// Lead master  list view function Closed 
	
	
			// Customer master  list view function start 
			$('#dt_customer_list').dataTable({
					"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>"+
						"t"+
						"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
					"autoWidth" : true,
			        "oLanguage": {
					    "sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
					},
   				    "ajax": site_path + 'admin/customer/getdata',
					"preDrawCallback" : function() {
						// Initialize the responsive datatables helper once.
						if (!responsiveHelper_dt_customer_list) {
							responsiveHelper_dt_customer_list = new ResponsiveDatatablesHelper($('#dt_customer_list'), breakpointDefinition);
						}
					},
					"rowCallback" : function(nRow) {
						responsiveHelper_dt_customer_list.createExpandIcon(nRow);
					},
					"drawCallback" : function(oSettings) {
						responsiveHelper_dt_customer_list.respond();
					}
				});
			// Customer master  list view function Closed 
			
			$('#datatable_tabletools').dataTable({ 
					"searching" : false,
					"lengthChange": false,
					"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>"+
						"t"+
						"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
					"autoWidth" : true,
			        "oLanguage": {
					    "sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
					},
					
					"aoColumns": [{"bVisible": false,"sWidth":"5%"},{"sWidth": "10%"},{"sWidth": "5%"},{"sWidth": "10%"},{"sWidth": "10%"},{"sWidth": "10%"},{"sWidth": "10%"},{"sWidth": "10%"},{"sWidth": "10%"},{"sWidth": "15%"},{"sWidth": "5%"}]	,
   				  
					"preDrawCallback" : function() { 
						// Initialize the responsive datatables helper once.
						if (!responsiveHelper_datatable_tabletools) {
							responsiveHelper_datatable_tabletools = new ResponsiveDatatablesHelper($('#datatable_tabletools'), breakpointDefinition);
						}
					},
					"rowCallback" : function(nRow) {
						responsiveHelper_datatable_tabletools.createExpandIcon(nRow);
					},
					"drawCallback" : function(oSettings) {
						responsiveHelper_datatable_tabletools.respond();
					}
				});
			
			
				$('#datatable_tabletools tbody').on( 'click', 'tr', function () {
					
					$(this).toggleClass('selected');
					
				} );
				

			/***********REGION LIST *******************************/
			$('#dt_region_list').dataTable({
					"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>"+
						"t"+
						"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
					"autoWidth" : true,
			        "oLanguage": {
					    "sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
					},
   				   
				   "ajax": site_path + 'admin/Region/getdata_region',
					"preDrawCallback" : function() {
						// Initialize the responsive datatables helper once.
						if (!responsiveHelper_dt_region_list) {
							responsiveHelper_dt_region_list = new ResponsiveDatatablesHelper($('#dt_region_list'), breakpointDefinition);
						}
					},
					"rowCallback" : function(nRow) {
						responsiveHelper_dt_region_list.createExpandIcon(nRow);
					},
					"drawCallback" : function(oSettings) {
						responsiveHelper_dt_region_list.respond();
					}
				});
			/*************REGION LIST END *************************/
			
						
						
			// city list
			$('#dt_city_list').dataTable({
					"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>"+
						"t"+
						"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
					"autoWidth" : true,
			        "oLanguage": {
					    "sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
					},
   				   
				   "ajax": site_path + 'admin/city/getdata',
					"preDrawCallback" : function() {
						// Initialize the responsive datatables helper once.
						if (!responsiveHelper_dt_city_list) {
							responsiveHelper_dt_city_list = new ResponsiveDatatablesHelper($('#dt_city_list'), breakpointDefinition);
						}
					},
					"rowCallback" : function(nRow) {
						responsiveHelper_dt_city_list.createExpandIcon(nRow);
					},
					"drawCallback" : function(oSettings) {
						responsiveHelper_dt_city_list.respond();
					}
				});
			/*************CITY LIST END *************************/
			
			$('#dt_checklist_list').dataTable({
					"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>"+
						"t"+
						"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
					"autoWidth" : true,
			        "oLanguage": {
					    "sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
					},
   				   // "ajax": site_path + 'admin/Checklist/getdata',
					"ajax": {
						
						'type': 'POST',
						'url': site_path + 'admin/checklist/getdata',
						'data': {id:$('#checklistService-form').serialize()}
        			},
					
					"preDrawCallback" : function() {
						// Initialize the responsive datatables helper once.
						if (!responsiveHelper_dt_checklist_list) {
							responsiveHelper_dt_checklist_list = new ResponsiveDatatablesHelper($('#dt_checklist_list'), breakpointDefinition);
						}
					},
					"rowCallback" : function(nRow) {
						responsiveHelper_dt_checklist_list.createExpandIcon(nRow);
					},
					"drawCallback" : function(oSettings) {
						responsiveHelper_dt_checklist_list.respond();
					}
				});
				
				$('#dt_service_list').dataTable({
					"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>"+
						"t"+
						"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
					"autoWidth" : true,
			        "oLanguage": {
					    "sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
					},
   				    "ajax": site_path + 'admin/Checklist/getdata_services',
					"preDrawCallback" : function() {
						// Initialize the responsive datatables helper once.
						if (!responsiveHelper_dt_service_list) {
							responsiveHelper_dt_service_list = new ResponsiveDatatablesHelper($('#dt_service_list'), breakpointDefinition);
						}
					},
					"rowCallback" : function(nRow) {
						responsiveHelper_dt_service_list.createExpandIcon(nRow);
					},
					"drawCallback" : function(oSettings) {
						responsiveHelper_dt_service_list.respond();
					}
				});
				
		});
		
		// PAGE RELATED SCRIPTS

			/* chart colors default */
			var $chrt_border_color = "#efefef";
			var $chrt_grid_color = "#DDD"
			var $chrt_main = "#E24913";
			/* red       */
			var $chrt_second = "#6595b4";
			/* blue      */
			var $chrt_third = "#FF9F01";
			/* orange    */
			var $chrt_fourth = "#7e9d3a";
			/* green     */
			var $chrt_fifth = "#BD362F";
			/* dark red  */
			var $chrt_mono = "#000";

			$(document).ready(function() {
				
				//No of leads report
			// DO NOT REMOVE : GLOBAL FUNCTIONS!
				pageSetUp();
				// Bar charet for marketing
				
				
				/* bar chart for applcation processing as project*/
				
				if ($("#bar-chart").length) {
					/*var data1 = [];
					for (var i = 0; i <= 12; i += 1)
						data1.push([i, 20]);

					var data2 = [];
					for (var i = 0; i <= 12; i += 1)
						data2.push([i, 10]);

					var data3 = [];
					for (var i = 0; i <= 12; i += 1)
						data3.push([i, 30]);*/
						/*data1=data1_1;
						data2=data2_1;
						data3=data3_1;*/
					
					var ds = new Array();

					ds.push({
						data : data1,
						bars : {
							show : true,
							barWidth : 0.2,
							order : 1,
						}
					});
					ds.push({
						data : data2,
						bars : {
							show : true,
							barWidth : 0.2,
							order : 2
						}
					});
					ds.push({
						data : data3,
						bars : {
							show : true,
							barWidth : 0.2,
							order : 3
						}
					});

					//Display graph
					$.plot($("#bar-chart"), ds, {
						colors : [$chrt_second, $chrt_fourth, "#666", "#BBB"],
						grid : {
							show : true,
							hoverable : true,
							clickable : true,
							tickColor : $chrt_border_color,
							borderWidth : 0,
							borderColor : $chrt_border_color,
						},
						legend : true,
						tooltip : true,
						tooltipOpts : {
							content : "<b>%x</b> = <span>%y</span>",
							defaultTheme : false
						}

					});

				}

				/* end bar chart */
			});

			/* end flot charts */

		
		
		///////////////////////////////////////////////////////////////
		
		//*************GET DISTRICT ON CHANGE OF STATE******************/
		function getdistrict(state_id,element)
		{ 
		//alert(state_id);
			$.ajax({
			 type: "POST",
			 url: site_path + "commonfunctionController/getdistrict", 
			 data: {state : state_id},
			 dataType: "JSON",  
			 cache:false,
			 success: 
				  function(data){ //alert(data.length);

					$('#'+element).html('<option value="">District</option>');
					var toAppend = '';
				   $.each(data,function(i,o){
				   toAppend += '<option value="'+o.ID+'">'+o.DNM+'</option>';
				  });
		
				 	$('#'+element).append(toAppend);
					}
			   
			});
			
		}
		
		$('#state_id').change(function(event) {
        $('#tl_id').html('<option value="">Taluka</option>');
		 $('#area_id').html('<option value="">Area</option>');
    }); 
		
		
		function gettalukapincode(tl_id)
		{ 
		//alert(tl_id);
			$.ajax({
			 type: "POST",
			 url: site_path + "commonfunctionController/getTalukaPincode", 
			 data: {tal : tl_id},
			 dataType: "JSON",  
			 cache:false,
			 success: 
				  function(data){ 
				  if(data==0){
				  	$('#pin_code1').html('<input type="text" name="pin_code" id="pin_code" maxlength="6" onkeypress="return isNumberKey(event)" value="" placeholder="Pin code">');
				  }else
				  {
					  $('#pin_code1').html('<input type="text" name="pin_code" id="pin_code" maxlength="6" onkeypress="return isNumberKey(event)" value="'+data+'" placeholder="Pin code">');
					  
				  }

					
					/*$('#'+element).html('<option value="">Taluka</option>');
					var toAppend = '';
				   $.each(data,function(i,o){
				   toAppend += '<option value="'+o.ID+'">'+o.TNM+'</option>';
				  });
		
				 	$('#'+element).append(toAppend);*/

					
			  }
			});
		}
		
		function gettaluka(dist_id,element)
		{ 
		//alert(state_id);
			$.ajax({
			 type: "POST",
			 url: site_path + "commonfunctionController/getTaluka", 
			 data: {dist : dist_id},
			 dataType: "JSON",  
			 cache:false,
			 success: 
				  function(data){ //alert(data.length);

					$('#'+element).html('<option value="">Taluka</option>');
					var toAppend = '';
				   $.each(data,function(i,o){
				   toAppend += '<option value="'+o.ID+'">'+o.TNM+'</option>';
				  });
		
				 	$('#'+element).append(toAppend);

					
			  }
			});
		}
		
		$('#dist_id').change(function(event) {
         $('#area_id').html('<option value="">Area</option>');
    }); 
		
		
		function getarea(area_id,element)
		{ 
		//alert(state_id);
			$.ajax({
			 type: "POST",
			 url: site_path + "commonfunctionController/getArea", 
			 data: {area : area_id},
			 dataType: "JSON",  
			 cache:false,
			 success: 
				  function(data){ //alert(data.length);

					$('#'+element).html('<option value="">Area</option>');
					var toAppend = '';
				   $.each(data,function(i,o){
				   toAppend += '<option value="'+o.ID+'">'+o.ANM+'</option>';
				  });
		
				 	$('#'+element).append(toAppend);

					
			  }
			});
		}
		
		//*************GET PRoduct category ON CHANGE OF STATE******************/
		function getcategorytype(product_type_id,element)
		{ 
		//alert(state_id);
			$.ajax({
			 type: "POST",
			 url: site_path + "commonfunctionController/getcategory", 
			 data: {typeid : product_type_id},
			 dataType: "JSON",  
			 cache:false,
			 success: 
				  function(data){ //alert(data.length);

					$('#'+element).html('<option value="">Category</option>');
					var toAppend = '';
				   $.each(data,function(i,o){
				   toAppend += '<option value="'+o.ID+'">'+o.CNM+'</option>';
				  });
		
				 	$('#'+element).append(toAppend);

					
			  }
			});
		}
		
		/*************Change all data list change status ******/
		function change_all_status(status,element,rcid,section)
		{ 
			var add_class;
			var remove_class;
			var showmsg;
			
			if(status=='y')
			{add_class='btn-warning';remove_class='btn-success';showmsg='Inactive';}
			else if(status=='n')
			{add_class='btn-success';remove_class='btn-warning';showmsg='Active';}
			
			$.SmartMessageBox({
					title : "Alert!",
					content : 'Are you sure to '+showmsg+' this record?',
					buttons : '[Ok][Cancel]'
				}, function(ButtonPressed) {
					if (ButtonPressed === "Ok") 
					{
						$.ajax({
						 type: "POST",
						 url: site_path + "commonfunctionController/change_status_comman", 
						 data: {stat : status,rcid : rcid,section : section},
						 cache:false,
						 success: 
							  function(data){ //alert(data);
								
								if(data==1)
								{
									window.location.reload();
								}
								
						  }
						});
						
					}
					else 
					{
						return false;
					}
					
		
				});
				return false;
		}
		/********* Change all data list change status end**********/
		
		
			
			/***********REGION VALIDATION *****/
		var $regionaddForm = $("#region-add-form").validate({
				// Rules for form validation
				rules : {
					region_code : {
						required : true,
						maxlength: 2,
						minlength:2
					},
					region_name : {
						required : true,
						alphanumeric: true
					}
				},
	
				// Messages for form validation
				messages : {
					region_code : {
						required : 'Please enter region code'
					},
					region_name : {
						required : 'Please enter region name'
					}
				},
				 submitHandler: function(form) {
					checkRegion();
					return false;
				 },
	
				// Do not change code below
				errorPlacement : function(error, element) {
					error.insertAfter(element.parent());
				}
			});
		/***********Region Add  VALIDATION END *****/
		
		/************CHECK REgion Code duplicate********************/
			function checkRegion()
			{
				$('#btnregionadd').prop('disabled', true);
				$("#btnregionadd").removeClass('btn-primary');
				$("#btnregionadd").html('Submitting..');
				$("#btnregionadd").addClass('btn-success disabled');
				
				var data = $('#region-add-form').serialize();
				$.ajax({
						 type: "POST",
						 url: site_path + "admin/Region/regionadd_save", 
						 type : 'POST',
						 data : data,
						 async : false,
						 cache : false,
						 success: 
							  function(resp){ //alert(resp);
								d=JSON.parse(resp);//alert(d.error);
								//alert(d.file_name);
								if(d.XSTS!=1)//not valid
								{
										 $.SmartMessageBox({
											title : "Alert!",
											content : d.XMSG,
											buttons : '[Ok]'
										}, function(ButtonPressed) {
											if (ButtonPressed === "Ok") 
											{
												$("#btnregionadd").addClass('btn-primary');
												$("#btnregionadd").html('Add Region');
												$("#btnregionadd").removeClass('btn-success disabled');
												$('#btnregionadd').prop('disabled', false);
											}
											
										});
									
								}//if !=1
								else
								{ 
									window.location.href=site_path + "admin/Region";
								}//else
								
								
						  }
						});	
				return false;
			}//region add End
			
			//***********************REGION VALIDATION END***************************/
			
			/***********CHECKLIST VALIDATION *****/
		var $checklistaddForm = $("#checklist-add-form").validate({
				// Rules for form validation
				rules : {
					order_no : {
						required : true
					},
					checklist_name : {
						required : true,
						alphanumeric: true
					},
					checklist_fees : {
						required : true,
						digits: true
					},
					time_interval : {
						required : true,
						digits: true
					}
				},
	
				// Messages for form validation
				messages : {
					order_no : {
						required : 'Please select order no'
					},
					checklist_name : {
						required : 'Please enter checklist description'
					},
					checklist_fees : {
						required : 'Please enter fees',
						digits : 'Digits only please'
					},
					time_interval : {
						required : 'Please enter fees',
						digits : 'Digits only please'
					}
				},
				
				 submitHandler: function(form) {
					checkChecklist();
					return false;
				 },
	
				// Do not change code below
				errorPlacement : function(error, element) {
					error.insertAfter(element.parent());
				}
			});
		/***********CHECKLIST Add  VALIDATION END *****/
		
		/************CHECK CHECKLIST*******************/
			function checkChecklist()
			{
				$('#btncheckadd').prop('disabled', true);
				$("#btncheckadd").removeClass('btn-primary');
				$("#btncheckadd").html('Submitting..');
				$("#btncheckadd").addClass('btn-success disabled');
				
				var data = $('#checklist-add-form').serialize();
				$.ajax({
						 type: "POST",
						 url: site_path + "admin/checklist/checklistadd_save", 
						 type : 'POST',
						 data : data,
						 async : false,
						 cache : false,
						 success: 
							  function(resp){ //alert(resp);
								d=JSON.parse(resp);//alert(d.error);
								//alert(d.file_name);
								if(d.XSTS!=1)//not valid
								{
										 $.SmartMessageBox({
											title : "Alert!",
											content : d.XMSG,
											buttons : '[Ok]'
										}, function(ButtonPressed) {
											if (ButtonPressed === "Ok") 
											{
												$("#btncheckadd").addClass('btn-primary');
												$("#btncheckadd").html('Add');
												$("#btncheckadd").removeClass('btn-success disabled');
												$('#btncheckadd').prop('disabled', false);
											}
											
										});
									
								}//if !=1
								else
								{ 
								window.location.reload();
									//window.location.href=site_path + "admin/Checklist";
								}//else
								
								
						  }
						});	
				return false;
			}//checklist add End
			
			//***********************CHECKLIST VALIDATION END***************************/
			
			/***********SERVICES VALIDATION *****/
		var $servicesaddForm = $("#services-add-form").validate({
				// Rules for form validation
				rules : {
					service_name : {
						required : true,
					//	alphanumeric: true
					accept: "[a-zA-Z0-9 ,-/]"
					}
				},
	
				// Messages for form validation
				messages : {
					
					service_name : {
						required : 'Please enter service name',
						accept : "Accecpt only -,/ special character"
					}
				},
				 submitHandler: function(form) {
					checkServices();
					return false;
				 },
	
				// Do not change code below
				errorPlacement : function(error, element) {
					error.insertAfter(element.parent());
				}
			});
			
		/************CHECK SERVICES*******************/
			function checkServices()
			{
				$('#btnserviceadd').prop('disabled', true);
				$("#btnserviceadd").removeClass('btn-primary');
				$("#btnserviceadd").html('Submitting..');
				$("#btnserviceadd").addClass('btn-success disabled');
				
				var data = $('#services-add-form').serialize();
				$.ajax({
						 type: "POST",
						 url: site_path + "admin/Checklist/servicesadd_save", 
						 type : 'POST',
						 data : data,
						 async : false,
						 cache : false,
						 success: 
							  function(resp){ //alert(resp);
								d=JSON.parse(resp);//alert(d.error);
								//alert(d.file_name);
								if(d.XSTS!=1)//not valid
								{
										 $.SmartMessageBox({
											title : "Alert!",
											content : d.XMSG,
											buttons : '[Ok]'
										}, function(ButtonPressed) {
											if (ButtonPressed === "Ok") 
											{
												$("#btnserviceadd").addClass('btn-primary');
												$("#btnserviceadd").html('Add');
												$("#btnserviceadd").removeClass('btn-success disabled');
												$('#btnserviceadd').prop('disabled', false);
											}
											
										});
									
								}//if !=1
								else
								{ 
									window.location.href=site_path + "admin/Checklist/services";
								}//else
								
								
						  }
						});	
				return false;
			}//checklist add End
			
			function AssignChecklist()
			{ 
				$('#btnsassignadd').prop('disabled', true);
				$("#btnsassignadd").removeClass('btn-primary');
				$("#btnsassignadd").html('Submitting..');
				$("#btnsassignadd").addClass('btn-success disabled');
				
				var data = $('#checklistassign-list-form').serialize();
				$.ajax({
						 type: "POST",
						 url: site_path + "admin/Checklist/servicesassignadd_save", 
						 type : 'POST',
						 data : data,
						 async : false,
						 cache : false,
						 success: 
							  function(resp){ //alert(resp);
								d=JSON.parse(resp);//alert(d.error);
								//alert(d.file_name);
								if(d.X_STS!=1)//not valid
								{
										 $.SmartMessageBox({
											title : "Alert!",
											content : d.X_MSG,
											buttons : '[Ok]'
										}, function(ButtonPressed) {
											if (ButtonPressed === "Ok") 
											{
												$("#btnsassignadd").addClass('btn-primary');
												$("#btnsassignadd").html('Assign');
												$("#btnsassignadd").removeClass('btn-success disabled');
												$('#btnsassignadd').prop('disabled', false);
											}
											
										});
									
								}//if !=1
								else
								{ 
									window.location.href=site_path + "admin/Checklist/services";
								}//else
								
								
						  }
						});	
				return false;
			}//checklist add End
		
		

/**********************INPUT NUMBER RESTRICTION ***********/
function isNumberKey(evt)
       { 
          var charCode = (evt.which) ? evt.which : evt.keyCode;
          if (charCode != 46 && charCode > 31 
            && (charCode < 48 || charCode > 57))
             return false;

          return true;
       }
	   /**********************INPUT NUMBER RESTRICTION ***********/
			
			/***********CHECK OLD PASSWORD & CHANEG PASS *********/
			function checkoldpass()
			{
				var data = $('#admin-changepass').serialize();
				$.ajax({
						 type: "POST",
						 url: site_path + "admin/home/checkpass", 
						 data: data,
						 cache:false,
						 success: 
							  function(resp){ //alert(resp);
								
								var d = JSON.parse(resp);
								
								if(d.STAT!=1)//not valid
								{
									
										 $.SmartMessageBox({
											title : "Alert!",
											content : d.MSG,
											buttons : '[Ok]'
										}, function(ButtonPressed) {
											if (ButtonPressed === "Ok") 
											{
												$("#btnchangepassadd").addClass('btn-primary');
												$("#btnchangepassadd").html('Change Password');
												$("#btnchangepassadd").removeClass('btn-success disabled');
												$('#btnchangepassadd').prop('disabled', false);
											}
											
										});
									
								}//if !=1
								else
								{ 
									$.SmartMessageBox({
											title : "Alert!",
											content : 'Password change successfully.',
											buttons : '[Ok]'
										}, function(ButtonPressed) {
											if (ButtonPressed === "Ok") 
											{
												window.location.href=site_path + "Login/logout";
											}
											
										});
									
								}//else
								
								
						  }
						});	
			}
			/****************CHECK OLD PASSWORD & CHANEG PASS END****************/
			
			/***************FILE UPLOAD ***********************/
			function fileUpload(folder_name,form_name,image_input)
			{
				var data = $('#'+form_name).serialize();
				data+='&folder='+folder_name+"&image_input="+image_input;
				$.ajax({
						 type: "POST",
						 url: site_path + "Commonfunctioncontroller/do_upload", 
						 data: data,
					     mimeType:"multipart/form-data",
						 cache:false,
						 success: 
							  function(resp){ //alert(resp);
								
								if(resp!=1)//not valid
								{
										 $.SmartMessageBox({
											title : "Alert!",
											content : resp,
											buttons : '[Ok]'
										}, function(ButtonPressed) {
											if (ButtonPressed === "Ok") 
											{
												//
											}
											
										});
									
								}//if !=1
								else
								{ 
									//window.location.href=site_path + "user/Reentry";
								}//else
								
								
						  }
						});	
			}
			/*****************FILE UPLOAD END ********************/
			
			function isAddress(evt)
			{
				 var charCode = (evt.which) ? evt.which : evt.keyCode;//alert(charCode);
				if((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123) || (charCode > 43 && charCode < 48) || (charCode > 47 && charCode < 58) || (charCode == 32 || charCode == 34 || charCode == 39))
				{return true;}
				else
				{return false;}
			}


//******************MODAL END ************************


$('#SubcatModal').on('show.bs.modal', function(e) {

        var $modal = $(this),
            data = e.relatedTarget.id;
			var passdata = data.split(',');
			$modal.find('#subcat_name').val(passdata[1]);
			$modal.find('#subcat_id').val(passdata[0]);
			$("#item_cat_id_update").val(passdata[2]).trigger('change');
});

 $('#update_subcat').on('submit', function(e){
        e.preventDefault();
        $.ajax({
            url: site_path + "admin/Item/itemsubcatedit_save", 
            type: 'POST', //or POST
            data: $('#update_subcat').serialize(),
            success: function(data){ //alert(data);
                d=JSON.parse(data);
								if(d.X_STS!=1)//not valid
								{
										 $.SmartMessageBox({
											title : "Alert!",
											content : d.X_MSG,
											buttons : '[Ok]'
										}, function(ButtonPressed) {
											if (ButtonPressed === "Ok") 
											{
												
											}
											
										});
									
								}//if !=1
								else
								{ 
								$('#SubcatModal').modal('hide');
								showMessages('success',d.X_MSG,'btncatadd','Add Category','admin/Item/subcategory');
								//window.location.href = site_path+"admin/Item/subcategory";
								}//else
            }
        });
    });
	/*****************************************************/
	$('#CatModal').on('show.bs.modal', function(e) {

        var $modal = $(this),
            data = e.relatedTarget.id;
			var passdata = data.split(',');
			$modal.find('#cat_name').val(passdata[1]);
			$modal.find('#cat_id').val(passdata[0]);
});

 $('#update_cat').on('submit', function(e){
        e.preventDefault();
        $.ajax({
            url: site_path + "admin/Item/itemcatedit_save", 
            type: 'POST', //or POST
            data: $('#update_cat').serialize(),
            success: function(data){ //alert(data);
                d=JSON.parse(data);
								if(d.X_STS!=1)//not valid
								{
										 $.SmartMessageBox({
											title : "Alert!",
											content : d.X_MSG,
											buttons : '[Ok]'
										}, function(ButtonPressed) {
											if (ButtonPressed === "Ok") 
											{
												
											}
											
										});
									
								}//if !=1
								else
								{ 
								$('#CatModal').modal('hide');
								showMessages('success',d.X_MSG,'btncatadd','Add Category','admin/Item/category');

								}//else
            }
        });
    });
	
	/*****************************************************/
	$('#RegionModal').on('show.bs.modal', function(e) {

        var $modal = $(this),
            data = e.relatedTarget.id;
			var passdata = data.split(',');
			$modal.find('#region_name_update').val(passdata[1]);
			$modal.find('#region_id').val(passdata[0]);
			$modal.find('#region_code_update').val(passdata[2]);
});

 $('#update_region').on('submit', function(e){
        e.preventDefault();
        $.ajax({
            url: site_path + "admin/Region/regionedit_save", 
            type: 'POST', //or POST
            data: $('#update_region').serialize(),
            success: function(data){ //alert(data);
                d=JSON.parse(data);
								if(d.X_STS!=1)//not valid
								{
										 $.SmartMessageBox({
											title : "Alert!",
											content : d.X_MSG,
											buttons : '[Ok]'
										}, function(ButtonPressed) {
											if (ButtonPressed === "Ok") 
											{
												
											}
											
										});
									
								}//if !=1
								else
								{ 
								$('#RegionModal').hide();
								window.location.href = site_path+"admin/Region";
								}//else
            }
        });
    });
	
	/*****************************************************/
	$('#ChecklistModal').on('show.bs.modal', function(e) {

        var $modal = $(this),
            data = e.relatedTarget.id;
			var passdata = data.split(',');
			$modal.find('#checklist_name_update').val(passdata[1]);
			$modal.find('#checklist_id').val(passdata[0]);
			$modal.find('#order_no_update').val(passdata[2]);
			$modal.find('#time_interval_update').val(passdata[3]);
			$modal.find('#fees_update').val(passdata[4]);
});

 $('#update_checklist').on('submit', function(e){
        e.preventDefault();
		
        $.ajax({
            url: site_path + "admin/checklist/checklistedit_save", 
            type: 'POST', //or POST
            data: $('#update_checklist').serialize(),
            success: function(data){ //alert(data);
                d=JSON.parse(data);
								if(d.X_STS!=1)//not valid
								{
										 $.SmartMessageBox({
											title : "Alert!",
											content : d.X_MSG,
											buttons : '[Ok]'
										}, function(ButtonPressed) {
											if (ButtonPressed === "Ok") 
											{
												
											}
											
										});
									
								}//if !=1
								else
								{ 
								$('#ChecklistModal').hide();
								window.location.reload();
								//window.location.href = site_path+"admin/Checklist/assignchecklist?id="+id;
								}//else
            }
        });
    });
	
	/*****************************************************/
	$('#ServiceModal').on('show.bs.modal', function(e) {

        var $modal = $(this),
            data = e.relatedTarget.id;
			var passdata = data.split(',');
			$modal.find('#service_name_update').val(passdata[1]);
			$modal.find('#order_no_update').val(passdata[2]);
			$modal.find('#service_id').val(passdata[0]);
			$modal.find('#dept_id_update').val(passdata[3]);
			
});

 $('#update_service').on('submit', function(e){
        e.preventDefault();
        $.ajax({
            url: site_path + "admin/Checklist/servicesedit_save", 
            type: 'POST', //or POST
            data: $('#update_service').serialize(),
            success: function(data){ //alert(data);
                d=JSON.parse(data);
								if(d.X_STS!=1)//not valid
								{
										 $.SmartMessageBox({
											title : "Alert!",
											content : d.X_MSG,
											buttons : '[Ok]'
										}, function(ButtonPressed) {
											if (ButtonPressed === "Ok") 
											{
												
											}
											
										});
									
								}//if !=1
								else
								{ 
								$('#ServiceModal').hide();
								window.location.href = site_path+"admin/Checklist/services";
								}//else
            }
        });
    });
	
	//*************GET Subcategory of item  ON CHANGE OF item category******************/
		function getItemSubcategory(item_cat_id,element)
		{ 
		//alert(dept_id);
			$.ajax({
			 type: "POST",
			 url: site_path + "commonfunctionController/getItemSubcategorys", 
			 data: {item_cat_id : item_cat_id},
			 dataType: "JSON",  
			 cache:false,
			 success: 
				  function(data){ //alert(data.length);

					$('#'+element).html('<option value="">Select Subcategory</option>');
					var toAppend = '';
				   $.each(data,function(i,o){
				   toAppend += '<option value="'+o.ID+'">'+o.NM+'</option>';
				  });
		
				 	$('#'+element).append(toAppend);

					
			  }
			});
		}
		
		function ExportToExcelDiv(div_id,filename)
		{
			
		  var file = new Blob([$('#'+div_id).html()], {type:"application/vnd.ms-excel"});
		
		var url = URL.createObjectURL(file);
		
		var a = $("<a />", {
		  href: url,
		  download: filename+".xls"
		})
		.appendTo("body")
		.get(0)
		.click();
		  e.preventDefault();

		}
		
		function printDiv(div_id) 
		{ 
			var html = '';
			$('link').each(function() { 
				if ($(this).attr('rel').indexOf('stylesheet') !=-1) 
				{  
				  html += '<link rel="stylesheet" href="'+$(this).attr("href")+'" />';
				}
			  });
		
		
		  var divToPrint=document.getElementById(div_id);
		  
		  html += divToPrint.innerHTML;
		
		  var newWin=window.open('','Print-Window');
		
		  newWin.document.open();
		
		  newWin.document.write('<html><body onload="window.print()">'+html+'</body></html>');
		
		  newWin.document.close();
		
		  setTimeout(function(){newWin.close();},10);
		
		}
		
		function showMessages(msgtype,msg,btnid,btntext,href)
		{
			$('#alert_message').html('');
			var cls='alert-success';
			if(msgtype == 'success')
			{			
			$('#alert_message').append('<div class="alert alert-success alert-block"><a class="close" data-dismiss="alert" href="#">×</a><h4 class="alert-heading">Success!</h4>'+msg+'</div>');
			cls = 'alert-success';
			}
			else if(msgtype == 'error')
			{			
			$('#alert_message').append('<div class="alert alert-danger alert-block"><a class="close" data-dismiss="alert" href="#">×</a><h4 class="alert-heading">Error!</h4>'+msg+'</div>');
			cls = 'alert-danger';
			}
			else if(msgtype == 'warning')
			{			
			$('#alert_message').append('<div class="alert alert-block alert-warning"><a class="close" data-dismiss="alert" href="#">×</a><h4 class="alert-heading">Warning!</h4>'+msg+'</div>');
			cls = 'alert-warning';
			}
			else if(msgtype == 'info')
			{			
			$('#alert_message').append('<div class="alert alert-info alert-block"><a class="close" data-dismiss="alert" href="#">×</a><h4 class="alert-heading">Info!</h4>'+msg+'</div>');
			cls = 'alert-info';
			}
									
			$("."+cls).fadeTo(2500, 2300).slideUp(2300, function(){
				$("."+cls).slideUp(2300);
				$("#"+btnid).addClass('btn-primary');
				$("#"+btnid).html(btntext);
				$("#"+btnid).removeClass('btn-success disabled');
				$('#'+btnid).prop('disabled', false);
				if(href!='')
				{	window.location.href=site_path + href;}
			});
		}
		
		
		function getItemsCategories(type,cnt)
		{
			 $('#cattype'+cnt).remove();
			 $('#itemsubcat'+cnt).remove();
			//alert(type_id+' '+element);
			if(type != 'SUBASS')
			{
			if(type == 'ASS')
			{
				
				var toAppend = '<section id="cattype'+cnt+'" class="col col-2"><label class="select"><select name="item[type_id'+cnt+']" id="type_id'+cnt+'" onChange=getAssembly(this.value,'+cnt+'); title="Select Type"><option value="" > &nbsp;Select Type</option><option value="ITEM">Item</option><option value="SUBASSEMBLY">Subassembly</option></select><i></i></label></section>';
		
				 	$('#typediv'+cnt).after(toAppend);
			}
			else
			{
			
			
			$.ajax({
			 type: "POST",
			 url: site_path + "commonfunctionController/getItemandAssemblyCategories", 
			 data: {type : type},
			 dataType: "JSON",  
			 cache:false,
			 success: 
				  function(data){ //alert(data.length);
				  
				  $('#cattype'+cnt).remove();$('#itemsubcat'+cnt).remove();
				  var text;
				  if(type == 'ITM'){text = 'Select Category'; method = "onChange=getItemSubcategoryByCategory(this.value,'"+cnt+"');";}
				  if(type == 'PRODUCT'){text = 'Select Product Type';method = "onChange=getProductcategory(this.value,'"+cnt+"'),getProducts(this.value,'"+cnt+"'),getProductBrand('"+cnt+"');";}
				

					var toAppend = '<section id="cattype'+cnt+'" class="col col-2"><label class="select"><select name="item[type_id'+cnt+']" id="type_id'+cnt+'" title="'+text+'" '+method+'><option value="" > &nbsp;'+text+'</option>';
				   $.each(data,function(i,o){
				   toAppend += '<option value="'+o.ID+'">'+o.CONM+'</option>';
				  });
				  
				  toAppend += '</select><i></i></label></section>';
		
				 	$('#typediv'+cnt).after(toAppend);

					
			  }
			});
			
			}//else
			}
		
		}
		
		//get assemblies according to item type and assembly type
		function getAssembly(asstype,cnt)
		{
			 
			//alert(type_id+' '+element);
			$.ajax({
			 type: "POST",
			 url: site_path + "commonfunctionController/getAssembly", 
			 data: {ass_type : asstype},
			 dataType: "JSON",  
			 cache:false,
			 success: 
				  function(data){ //alert(data.length);

					$('#comman_id'+cnt).html('<option value="">Select Assembly</option>');
					var toAppend = '';
				   $.each(data,function(i,o){
				   toAppend += '<option value="'+o.ID+'">'+o.CONM+'</option>';
				  });
		
				 	$('#comman_id'+cnt).append(toAppend);

					
			  }
			});
		
		}
		
		//get assemblies according to item type and assembly type
		function getItemSubcategoryByCategory(item_cat,cnt)
		{
			 $('#itemsubcat'+cnt).remove();
			//alert(type_id+' '+element);
			$.ajax({
			 type: "POST",
			 url: site_path + "commonfunctionController/getItemSubcategoryByCategory", 
			 data: {item_cat : item_cat},
			 dataType: "JSON",  
			 cache:false,
			 success: 
				  function(data){ //alert(data.length);


						var toAppend = '<section id="itemsubcat'+cnt+'" class="col col-2"><label class="select"><select name="item[isubcat_id'+cnt+']" id="isubcat_id'+cnt+'" onchange="getItems(\'type_id\',\'isubcat_id\','+cnt+')" title="Select item subcategory"><option value="" > &nbsp;Select Subcategory</option>';
				   $.each(data,function(i,o){
				   toAppend += '<option value="'+o.ID+'">'+o.CONM+'</option>';
				  });
				  
				  toAppend += '</select><i></i></label></section>';
				 			 
					$('#cattype'+cnt).after(toAppend);
					
			  }
			});
		
		}
		//--------------FOR PRODUCT S ---------------------------
		//get Product category  according to type 
		function getProductcategory(product_type,cnt)
		{
			 $('#itemsubcat'+cnt).remove();
			//alert(type_id+' '+element);
			$.ajax({
			 type: "POST",
			 url: site_path + "commonfunctionController/getProductCategoryByType", 
			 data: {product_type : product_type},
			 dataType: "JSON",  
			 cache:false,
			 success: 
				  function(data){ //alert(data.length);


						var toAppend = '<section id="itemsubcat'+cnt+'" class="col col-2"><label class="select"><select name="item[iprdcat_id'+cnt+']" id="iprdcat_id'+cnt+'" onchange="getProducts($(\'#type_id'+cnt+'\').val(),'+cnt+',\'iprdcat_id\',\'brand_id\')" title="Select category"><option value="" > &nbsp;Select Category</option>';
				   $.each(data,function(i,o){
				   toAppend += '<option value="'+o.ID+'">'+o.CONM+'</option>';
				  });
				  
				  toAppend += '</select><i></i></label></section>';
				 			 
					$('#cattype'+cnt).after(toAppend);
					
			  }
			});
		
		}
		
		function getProductBrand(cnt)
		{
			 $('#productbrand'+cnt).remove();
			//alert(type_id+' '+element);
			$.ajax({
			 type: "POST",
			 url: site_path + "commonfunctionController/getProductBrands", 
			 dataType: "JSON",  
			 cache:false,
			 success: 
				  function(data){ //alert(data.length);


						var toAppend = '<section id="productbrand'+cnt+'" class="col col-2"><label class="select"><select name="item[brand_id'+cnt+']" id="brand_id'+cnt+'" title="Select brand" onchange="getProducts($(\'#type_id'+cnt+'\').val(),\''+cnt+'\',\'iprdcat_id\',\'brand_id\')"><option value="" > &nbsp;Select brand</option>';
				   $.each(data,function(i,o){
				   toAppend += '<option value="'+o.ID+'">'+o.BNM+'</option>';
				  });
				  
				  toAppend += '</select><i></i></label></section>';
				 			 
					$('#itemsubcat'+cnt).after(toAppend);
					
			  }
			});
		
		
		}
		
		//get Product category  according to type 
		function getProductcategoryScrap(product_type)
		{
			 $('#itemsubcat').remove();
			//alert(type_id+' '+element);
			$.ajax({
			 type: "POST",
			 url: site_path + "commonfunctionController/getProductCategoryByType", 
			 data: {product_type : product_type},
			 dataType: "JSON",  
			 cache:false,
			 success: 
				  function(data){ //alert(data.length);


						var toAppend = '<section id="itemsubcat" class="col col-2"><label class="select"><select name="item[iprdcat_id]" id="iprdcat_id" onchange="getProductScrap($(\'#type_id\').val(),\'iprdcat_id\',\'brand_id\')" title="Select category"><option value="" > &nbsp;Select Category</option>';
				   $.each(data,function(i,o){
				   toAppend += '<option value="'+o.ID+'">'+o.CONM+'</option>';
				  });
				  
				  toAppend += '</select><i></i></label></section>';
				 			 
					$('#cattype').after(toAppend);
					
			  }
			});
		
		}
		
		function getProductBrandScrap()
		{
			 $('#productbrand').remove();
			//alert(type_id+' '+element);
			$.ajax({
			 type: "POST",
			 url: site_path + "commonfunctionController/getProductBrands", 
			 dataType: "JSON",  
			 cache:false,
			 success: 
				  function(data){ //alert(data.length);


						var toAppend = '<section id="productbrand" class="col col-2"><label class="select"><select name="item[brand_id]" id="brand_id" title="Select brand" onchange="getProductScrap($(\'#type_id\').val(),\'iprdcat_id\',\'brand_id\')"><option value="" > &nbsp;Select brand</option>';
				   $.each(data,function(i,o){
				   toAppend += '<option value="'+o.ID+'">'+o.BNM+'</option>';
				  });
				  
				  toAppend += '</select><i></i></label></section>';
				 			 
					$('#itemsubcat').after(toAppend);
					
			  }
			});
		
		
		}
		//--------------FOR PRODUCT END-------------
		function getItems(cat_ele,subcat_ele,cnt)
		{
			var catid = $('#'+cat_ele+''+cnt).val();
			var subcatid = $('#'+subcat_ele+''+cnt).val();
			
			$.ajax({
			 type: "POST",
			 url: site_path + "commonfunctionController/getItemsForSelect", 
			 data: {cat_id : catid,subcat_id : subcatid},
			 dataType: "JSON",  
			 cache:false,
			 success: 
				  function(data){ //alert(data.length);
			         $('#comman_id'+cnt).html('<option value="">Select Items</option>');
					var toAppend = '';
				   $.each(data,function(i,o){
				   toAppend += '<option value="'+o.ID+'">'+o.CONM+'</option>';
				  });
		
				 	$('#comman_id'+cnt).append(toAppend);
					
			  }
			});
		}
		//----------------------------------FOR SCRAP ENTRIES -----
		function getItemsCategoriesScrap(type)
		{
			 $('#cattype').remove();
			 $('#itemsubcat').remove();
			 $('#productbrand').remove();
			//alert(type_id+' '+element);
			if(type != 'SUBASS')
			{
			if(type == 'ASS')
			{
				
				var toAppend = '<section id="cattype" class="col col-2"><label class="select"><select name="item[type_id]" id="type_id" onChange=getAssemblyScrap(this.value); title="Select Type"><option value="" > &nbsp;Select Type</option><option value="ITEM">Item</option><option value="SUBASSEMBLY">Subassembly</option></select><i></i></label></section>';
		
				 	$('#typediv').after(toAppend);
			}
			else
			{
			
			
			$.ajax({
			 type: "POST",
			 url: site_path + "commonfunctionController/getItemandAssemblyCategories", 
			 data: {type : type},
			 dataType: "JSON",  
			 cache:false,
			 success: 
				  function(data){ //alert(data.length);
				  
				  $('#cattype').remove();$('#itemsubcat').remove();
				  var text;
				  if(type == 'ITM'){text = 'Select Category'; method = "onChange=getItemSubcategoryByCategoryScrap(this.value);";}
				  if(type == 'PRODUCT'){text = 'Select Product Type';method = "onChange=getProductcategoryScrap(this.value),getProductScrap(this.value),getProductBrandScrap();";}
				

					var toAppend = '<section id="cattype" class="col col-2"><label class="select"><select name="item[type_id]" id="type_id" title="'+text+'" '+method+'><option value="" > &nbsp;'+text+'</option>';
				   $.each(data,function(i,o){
				   toAppend += '<option value="'+o.ID+'">'+o.CONM+'</option>';
				  });
				  
				  toAppend += '</select><i></i></label></section>';
		
				 	$('#typediv').after(toAppend);

					
			  }
			});
			
			}//else
			}
		
		}
		
		//get assemblies according to item type and assembly type
		function getAssemblyScrap(asstype)
		{
			 
			//alert(type_id+' '+element);
			$.ajax({
			 type: "POST",
			 url: site_path + "commonfunctionController/getAssembly", 
			 data: {ass_type : asstype},
			 dataType: "JSON",  
			 cache:false,
			 success: 
				  function(data){ //alert(data.length);

					$('#item_id').html('<option value="">Select Assembly</option>');
					var toAppend = '';
				   $.each(data,function(i,o){
				   toAppend += '<option value="'+o.ID+'">'+o.CONM+'</option>';
				  });
		
				 	$('#item_id').append(toAppend);

					
			  }
			});
		
		}
		
		//get assemblies according to item type and assembly type
		function getItemSubcategoryByCategoryScrap(item_cat)
		{
			 $('#itemsubcat').remove();
			//alert(type_id+' '+element);
			$.ajax({
			 type: "POST",
			 url: site_path + "commonfunctionController/getItemSubcategoryByCategory", 
			 data: {item_cat : item_cat},
			 dataType: "JSON",  
			 cache:false,
			 success: 
				  function(data){ //alert(data.length);


						var toAppend = '<section id="itemsubcat" class="col col-2"><label class="select"><select name="item[isubcat_id]" id="isubcat_id" onchange="getItemsScrap(\'type_id\',\'isubcat_id\')" title="Select item subcategory"><option value="" > &nbsp;Select Subcategory</option>';
				   $.each(data,function(i,o){
				   toAppend += '<option value="'+o.ID+'">'+o.CONM+'</option>';
				  });
				  
				  toAppend += '</select><i></i></label></section>';
				 			 
					$('#cattype').after(toAppend);
					
			  }
			});
		
		}
		
		function getItemsScrap(cat_ele,subcat_ele)
		{
			var catid = $('#'+cat_ele).val();
			var subcatid = $('#'+subcat_ele).val();
			
			$.ajax({
			 type: "POST",
			 url: site_path + "commonfunctionController/getItemsForSelect", 
			 data: {cat_id : catid,subcat_id : subcatid},
			 dataType: "JSON",  
			 cache:false,
			 success: 
				  function(data){ //alert(data.length);
			         $('#item_id').html('<option value="">Select Items</option>');
					var toAppend = '';
				   $.each(data,function(i,o){
				   toAppend += '<option value="'+o.ID+'">'+o.CONM+'</option>';
				  });
		
				 	$('#item_id').append(toAppend);
					
			  }
			});
		}
		
		function getProducts(type,cnt,cat,brand)
		{
			var typeval = type;
			
			if(typeof(cat)==='undefined')
			{var catval = '';}
			else{var catval = $('#'+cat+''+cnt).val();}
			
			if(typeof(brand)==='undefined')
			{var brandval = '';}
			else{var brandval = $('#'+brand+''+cnt).val();}
			
			
			
			$.ajax({
			 type: "POST",
			 url: site_path + "commonfunctionController/getProductsByFilter", 
			 data: {type : typeval,cat : catval,brand : brandval},
			 dataType: "JSON",  
			 cache:false,
			 success: 
				  function(data){ //alert(data.length);
			         $('#comman_id'+cnt).html('<option value="">Select Products</option>');
					var toAppend = '';
				   $.each(data,function(i,o){
				   toAppend += '<option value="'+o.ID+'">'+o.CONM+'</option>';
				  });
		
				 	$('#comman_id'+cnt).append(toAppend);
					
			  }
			});
		}
		
		function getProductScrap(type,cat,brand)
		{
			var typeval = type;
			if(typeof(cat)==='undefined')
			{var catval = '';}
			else{var catval = $('#'+cat).val();}
			if(typeof(brand)==='undefined')
			{var brandval = '';}
			else{var brandval = $('#'+brand).val();}
			
			
			
			$.ajax({
			 type: "POST",
			 url: site_path + "commonfunctionController/getProductsByFilter", 
			 data: {type : typeval,cat : catval,brand : brandval},
			 dataType: "JSON",  
			 cache:false,
			 success: 
				  function(data){ //alert(data.length);
			         $('#item_id').html('<option value="">Select Products</option>');
					var toAppend = '';
				   $.each(data,function(i,o){
				   toAppend += '<option value="'+o.ID+'">'+o.CONM+'</option>';
				  });
		
				 	$('#item_id').append(toAppend);
					
			  }
			});
		}
		//BOM FUNCTIONS FOR FILTER
		function getProductCategoriesInBOM(type)
		{
			$.ajax({
			 type: "POST",
			 url: site_path + "commonfunctionController/getProductCategoryByType", 
			 data: {product_type : type},
			 dataType: "JSON",  
			 cache:false,
			 success: 
				  function(data){ //alert(data.length);
			         $('#product_cat_id').html('<option value="">Select Category</option>');
					var toAppend = '';
				   $.each(data,function(i,o){
				   toAppend += '<option value="'+o.ID+'">'+o.CONM+'</option>';
				  });
		
				 	$('#product_cat_id').append(toAppend);
					
			  }
			});
		}
		
		function getProductsInBOM(cat,type,brand)
		{
			$.ajax({
			 type: "POST",
			 url: site_path + "commonfunctionController/getProductsByFilter", 
			 data: {type : type,cat : cat,brand : brand},
			 dataType: "JSON",  
			 cache:false,
			 success: 
				  function(data){ //alert(data.length);
			         $('#product_id').html('<option value="">Select Products</option>');
					var toAppend = '';
				   $.each(data,function(i,o){
				   toAppend += '<option value="'+o.ID+'">'+o.CONM+'</option>';
				  });
		
				 	$('#product_id').append(toAppend);
					
			  }
			});
		}
		
		$('#doc_file').ajaxfileupload({
  action: site_path + "commonfunctionController/do_upload", 
 // valid_extensions : ['pdf','png','jpg','gif'],
  params: {
    folder_name: 'clientdoc',
	input_name: 'doc_file',
	allow_type: '*'//all exte
  },
  onComplete: function(response) { 
	 var data = JSON.parse(JSON.stringify(response));
	 if(!data.file_name)
	{
		 //alert(data.error);
		 $.SmartMessageBox({
					title : "Alert!",
					content : data.error,
					buttons : '[Ok]'
					}, function(ButtonPressed) {
					if (ButtonPressed === "Ok") 
					{
					$("#doc_file_name").val(''); 
					}
				});
	}
	else
	{	$('#doc_file_name').val(data.file_name);}
  },
  onStart: function() {
    //if(weWantedTo) return false; // cancels upload
  },
  onCancel: function() {
    alert('no file selected');
  }
});


/**********DELETE region******************/
			function region_delete(regid)
			{ 
			//alert(desigid);
				$.SmartMessageBox({
					title : "Alert!",
					content : 'Are you sure to delete this record?',
					buttons : '[Ok][Cancel]'
				}, function(ButtonPressed) {
					if (ButtonPressed === "Ok") 
					{
						$.ajax({
						 type: "POST",
						 url: site_path + "commonfunctionController/delete_region", 
						  data: {regid : regid},
						 cache:false,
						 success: 
							  function(data){ //alert(data);
								
								if(data==1)
								{	window.location.reload();
								}
								
						  }
						});	
					}
					else 
					{
						return false;
					}
					
		
				});
				return false;
			}
			/************DELETE region END *********/
			
			/**********DELETE region district******************/
			function delete_region_district(regid)
			{ 
			//alert(desigid);
				$.SmartMessageBox({
					title : "Alert!",
					content : 'Are you sure to delete this record?',
					buttons : '[Ok][Cancel]'
				}, function(ButtonPressed) {
					if (ButtonPressed === "Ok") 
					{
						$.ajax({
						 type: "POST",
						 url: site_path + "commonfunctionController/delete_region_district", 
						  data: {regid : regid},
						 cache:false,
						 success: 
							  function(data){ //alert(data);
								
								if(data==1)
								{	window.location.reload();
								}
								
						  }
						});	
					}
					else 
					{
						return false;
					}
					
		
				});
				return false;
			}
			/************DELETE region district END *********/
			
			
			/**********DELETE service type******************/
			function servicetype_delete(stid)
			{ 
			//alert(desigid);
				$.SmartMessageBox({
					title : "Alert!",
					content : 'Are you sure to delete this record?',
					buttons : '[Ok][Cancel]'
				}, function(ButtonPressed) {
					if (ButtonPressed === "Ok") 
					{
						$.ajax({
						 type: "POST",
						 url: site_path + "commonfunctionController/service_type_delete", 
						  data: {stid : stid},
						 cache:false,
						 success: 
							  function(data){ //alert(data);
								
								if(data==1)
								{	window.location.reload();
								}
								
						  }
						});	
					}
					else 
					{
						return false;
					}
					
		
				});
				return false;
			}
			/************DELETE service type END *********/
			
			
			
			/**********DELETE Enterprice******************/
			function enterprice_delete(cid)
			{ 
			//alert(desigid);
				$.SmartMessageBox({
					title : "Alert!",
					content : 'Are you sure to delete related enterprice with followup data?',
					buttons : '[Ok][Cancel]'
				}, function(ButtonPressed) {
					if (ButtonPressed === "Ok") 
					{
						$.ajax({
						 type: "POST",
						 url: site_path + "commonfunctionController/enterprice_delete", 
						  data: {cid : cid},
						 cache:false,
						 success: 
							  function(data){ //alert(data);
								
								if(data==1)
								{	window.location.reload();
								}
								
						  }
						});	
					}
					else 
					{
						return false;
					}
					
		
				});
				return false;
			}
			/************DELETE Enterprice END *********/
			
			
			/**********DELETE Client******************/
			function client_delete(cid)
			{ 
			//alert(desigid);
				$.SmartMessageBox({
					title : "Alert!",
					content : 'Are you sure to delete this Client?',
					buttons : '[Ok][Cancel]'
				}, function(ButtonPressed) {
					if (ButtonPressed === "Ok") 
					{
						$.ajax({
						 type: "POST",
						 url: site_path + "commonfunctionController/client_delete", 
						  data: {cid : cid},
						 cache:false,
						 success: 
							  function(data){ //alert(data);
								
								if(data==1)
								{	window.location.reload();
								}
								
						  }
						});	
					}
					else 
					{
						return false;
					}
					
		
				});
				return false;
			}
			/************DELETE Employee END *********/


		/**********DELETE service******************/
			function service_delete(sid)
			{ 
			//alert(desigid);
				$.SmartMessageBox({
					title : "Alert!",
					content : 'Are you sure to delete this Service?',
					buttons : '[Ok][Cancel]'
				}, function(ButtonPressed) {
					if (ButtonPressed === "Ok") 
					{
						$.ajax({
						 type: "POST",
						 url: site_path + "commonfunctionController/service_delete", 
						  data: {sid : sid},
						 cache:false,
						 success: 
							  function(data){ //alert(data);
								
								if(data==1)
								{	window.location.reload();
								}
								
						  }
						});	
					}
					else 
					{
						return false;
					}
					
		
				});
				return false;
			}
			
			/**********DELETE checklist******************/
			function checklist_delete(ckid)
			{ 
			//alert(desigid);
				$.SmartMessageBox({
					title : "Alert!",
					content : 'Are you sure to delete this checklist?',
					buttons : '[Ok][Cancel]'
				}, function(ButtonPressed) {
					if (ButtonPressed === "Ok") 
					{
						$.ajax({
						 type: "POST",
						 url: site_path + "commonfunctionController/checklist_delete", 
						  data: {ckid : ckid},
						 cache:false,
						 success: 
							  function(data){ //alert(data);
								
								if(data==1)
								{	window.location.reload();
								}
								
						  }
						});	
					}
					else 
					{
						return false;
					}
					
		
				});
				return false;
			}
			
			
			
			// Pai chart for dashboard 
			
			var chartDiv = $("#barChart");
				var myChart = new Chart(chartDiv, {
				type: 'pie',
				data: {
				labels: lable_pai,
				datasets: [
				{
				data: data_pai,
				backgroundColor: color_pai
				}]
				},
				options: {
				title: {
				display: false,
				text: 'Pie Chart'
				},
				responsive: true,
				maintainAspectRatio: false,
				}
				});
