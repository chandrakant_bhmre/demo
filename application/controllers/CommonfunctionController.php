<?php
/*
Created By : Chandrakant Bhamare
Purpose : Common function controller
creation Date : 2018-10-11
Modified By : Chandrakant
Modified Date : 19-02-14
Other Information : This controller is used to call helper function for ajax request 
*/
defined('BASEPATH') OR exit('No direct script access allowed');

class CommonfunctionController extends CI_Controller 
{
	function __construct()
	{
		parent::__construct();
		$this->load->model( 'utility_model');
		$this->load->model( 'StateModel');
		$this->load->model( 'CommonModel');
	}
		
	public function change_status_package()
	{
		
		$current_status=$this->input->post('stat');
		$sid=$this->input->post('sid');
		
				
		if($current_status=='y')
		{$change_status='n';}
		else
		{$change_status='y';}
		
		$status_result=$this->StateModel->changeStatus($change_status,$sid);
		
		echo $status_result;
		
		//echo 1;
		
	
	}
	
	
	public function change_status_comman()
	{
		
		$current_status=$this->input->post('stat');
		$rcid=$this->input->post('rcid');
		$section=$this->input->post('section');
				
		if($current_status=='y')
		{$change_status='n';}
		else
		{$change_status='y';}
		
		$status_result=$this->CommonModel->changeStatus($change_status,$rcid,$section);
		
		echo $status_result;
		
		//echo 1;
		
	
	}
	public function getdistrict()
	{
			$state_id=$this->input->post('state');
		$districts=$this->CommonModel->getDistricts($state_id);
		echo $districts;
	}
	
	public function getCompanyDetails()
	{
			$employee_id=$this->input->post('company');
		$companies=$this->CommonModel->getCompanyDetails($employee_id);
		echo $companies;
	}
	
	public function getTaluka()
	{
			$dist_id=$this->input->post('dist');
		$talukas=$this->CommonModel->getTaluka($dist_id);
		echo $talukas;
	}
	
	public function getArea()
	{
			$area_id=$this->input->post('area');
		$areas=$this->CommonModel->getArea($area_id);
		echo $areas;
	}
	
	public function getTalukaPincode()
	{
			$tl_id=$this->input->post('tal');
		$talukaspincode=$this->CommonModel->getTalukaPincode($tl_id);
		echo $talukaspincode;
	}
	
	public function getEmployee()
	{
		$dept_id=$this->input->post('dept');
		$employees=$this->CommonModel->getEmployees($dept_id);
		echo $employees;
	}
	
	public function getItemSubcategorys()
	{
		$item_cat_id=$this->input->post('item_cat_id');
		$cats=$this->CommonModel->getItemSubcategories($item_cat_id);
		echo $cats;
	}
	
	public function getUnits()
	{
		$units=$this->CommonModel->getUnitdropdown();
		echo $units;
	}
	public function getEmployeeMachinewise()
	{
		$dept_id=$this->input->post('dept');
		$employeemachines=$this->CommonModel->getEmployeeMachinewise($dept_id);
		echo $employeemachines;
	}
	
	public function getcategory()
	{
		$typeid=$this->input->post('typeid');
		$category=$this->CommonModel->getCategoryTypeDropdown($typeid);
		echo $category;
	}
	
	
	
	public function change_status_sponser()
	{
		$current_status=$this->input->post('stat');
		$uid=$this->input->post('uid');
		
				
		if($current_status=='y')
		{$change_status='n';}
		else
		{$change_status='y';}
		
			
		$status_result=$this->AdminprofileModel->changeStatus($change_status,$uid);
		
		echo $status_result;
		
		//echo 1;
		
	}
	
	public function delete_status()
	{
		$uid=$this->input->post('uid');
		$ad=$this->input->post('ad');
				
		if($ad=='y')
		{
			$delete_result=profileDelete($uid,true);
		}
		else
		{
			$delete_result=profileDelete($uid);
		}
				
		echo $delete_result;
	}
	
	public function delete_package()
	{
		$pid=base64_decode($this->input->post('pid'));
		$delete_result=$this->packageModel->packageDelete($pid);
				
		echo $delete_result;
	}
	
	
	
	
	//USER LIST CHANGE
	public function change_status_users()
	{
		
		$current_status=$this->input->post('stat');
		$uid=$this->input->post('uid');
		
				
		if($current_status=='y')
		{$change_status='n';}
		else
		{$change_status='y';}
		
			
		$status_result=$this->UserprofileModel->changeStatus($change_status,$uid);
		
		echo $status_result;
		
		//echo 1;
		
	
	}
	
	//SEND OTP To Loggin USer
	public function sendOTP()
	{
		//print_r($_POST);print_r($this->session->all_userdata());
		$otp=$this->input->post('EN');
		$type=$this->input->post('TYPE');
		
		if(isset($_POST['MOB']))
		{
			$mobile_no=$this->input->post('MOB');
		}
		else
		{
			$mobile_no=$this->session->userdata('cont');
		}
		
		if(isset($_POST['EMAIL']))
		{
			$email=$this->input->post('EMAIL');
		}
		else
		{
			$email=$this->session->userdata('email');
		}
		
		if(trim(strtoupper($type))=="RESEND")
		{
			$otp = $this->session->userdata('ENUMOP');
		}
		
		//echo $mobile_no;
		$smstext=$otp.' is your OTP for V-Business Zone online. Do not share it with anyone by any means.This is confidential and to be used by you only.\nTeam V-Business Zone.\nhttp://www.vonlineshopee.com';
		//echo $smstext;
		//$smsresp=$this->utility_model->sendSMS(trim($mobile_no),$smstext);
		//echo base64_encode($otp);
		
		$this->session->set_userdata('ENUMOP',base64_encode($otp));
		
		if(!empty($email))
		{
		//OTP EMAIL
			
			$otpto = $email;
			$otpsubject="V Shopee Registration OTP";
			$message=$otp." is your OTP for V-Business zone online. Do not share it with anyone by any means.This is confidential and to be used by you only.<br><br>Please keep this e-mail for your records and keep it confidential.<br><br>-------------------------------------------------------<br><strong>This is an automated message, please do not reply.</strong>";
		//$mresponse = $this->utility_model->sendMail($otpto,$otpsubject,$message);
			
		}	
		
		
	}
	
	//VALIDATE OTP 
	public function validateOTP()
	{
		/*$enteredOTP=$this->input->post('QUEN');
		$sentOTP=base64_decode($this->session->userdata('ENUMOP'));
		if($enteredOTP==$sentOTP)
		{
			$this->session->unset_userdata('ENUMOP');
			echo 1;
		}
		else
		{
			echo 0;
		}*/
		
		echo 1;
	}
	
	//SEND OTP  To Sponser
	public function sendOTPToSponser()
	{
		//OTP SMS
		$otp=$this->input->post('EN');
		$mobile_no=$this->input->post('mob');
		$email=$this->input->post('email');
		$type = $this->input->post('TYPE');
		
		if(trim(strtoupper($type))=="RESEND")
		{
			$otp = $this->session->userdata('ENUMOP');
		}
		
		$smstext=$otp.' is your OTP for V-Business Zone sponsership. Do not share it with anyone by any means.This is confidential and to be used by you only.\nTeam V-Business Zone.\nhttp://www.vonlineshopee.com';
		//echo $smstext;
		//$smsresp=$this->utility_model->sendSMS(trim($mobile_no),$smstext);
		//echo base64_encode($otp);
		
		$this->session->set_userdata('ENUMOP',base64_encode($otp));
		
		//OTP EMAIL
		if(!empty($email))
		{	
			$otpto = $email;
			$otpsubject="V Shopee Registration OTP";
			$message=$otp." is your OTP for V-Business Zone online. Do not share it with anyone by any means.This is confidential and to be used by you only.<br><br>Please keep this e-mail for your records and keep it confidential.<br><br>-------------------------------------------------------<br><strong>This is an automated message, please do not reply.</strong>";
			//$mresponse = $this->utility_model->sendMail($otpto,$otpsubject,$message);
		}
		
	}
	
	
	//*************FILE UPLOAD *************
	 public function do_upload()
        {
			
				$folder_name=$this->input->post('folder_name');
				$image_input=$this->input->post('input_name');
				$allowed_types = $this->input->post('allow_type');
				if(isset($_POST['file_name']))
				{
					$filename = $this->input->post('file_name');
				}
				
				//echo $image_input;
				
				//$user_image=$this->input->post($image_input);
				if(!empty($_FILES[$image_input]['name'])) //new image uploaded
				{
					//$config['upload_path']          = 'assets/global/img/upload/'.$folder_name;
					$config['upload_path']          = 'upload/'.$folder_name;
					//$config['upload_path']          = base_url().'upload/product';
					$config['allowed_types']        = $allowed_types;
					$config['max_size']             = 500;
					$config['max_width']            = 500;
					$config['max_height']           = 500;
					if(isset($_POST['file_name']))
					{
						$config['file_name'] = $filename;
					}
					
					
					//$this->upload->initialize($config);
	
					$this->load->library('upload', $config);
					
					
	
					if ( ! $this->upload->do_upload($image_input))
					{
							$error = array('error' => $this->upload->display_errors());
	
							//$this->load->view('upload_form', $error);
							//print_r($this->upload->display_errors());
							
							echo json_encode($error);
					}
					else
					{
							$upload_data = $this->upload->data();
							
							//echo $upload_data['full_path'];
						
							//$data = array('upload_data' => $this->upload->data());
	
							//$this->load->view('upload_success', $data);
							//print_r($data);
							//echo json_encode($data);
							
							echo '{"file_name":"'.$upload_data['file_name'].'","full_path":"'.$upload_data['full_path'].'"}';
					}
				}
				else
				{
					$existing_image=$this->input->post('prev_img');
					//echo $existing_image;
					$data = array('upload_data' => array('file_name'=>$existing_image));
					echo json_encode($data);
				}
        }
		
		//get item and assambly data using dropdown
		public function getItemandAssembly()
	{
			$type_id=$this->input->post('typeid');
			
		$ItemAss=$this->CommonModel->getItemandAssemblys($type_id);
		echo $ItemAss;
	}
		
	public function getTaxDetails()
	{
		$item_id = $this->input->post('item_id');
		$type = $this->input->post('type');
		$tax_result=$this->CommonModel->getTaxDetails($item_id,$type);
		echo $tax_result;
	}
	
	

	// get employee department wise and check is present in user master created by chandrakant
	public function getEmployeeDepartmentWise()
	{
		$dept_id=$this->input->post('dept');
		$employeeDepartment=$this->CommonModel->getEmployeeDepartmentWise($dept_id);
		echo $employeeDepartment;
	}
	

	public function getAvailableItemStock()
	{
		$item_id = $this->input->post('item_id');
		$item_type = $this->input->post('item_type');
		$batch_no =$this->input->post('batch_no');
		$godown_id = $this->input->post('godown');
		$result=$this->CommonModel->getAvailableItemStock($item_id,$item_type,$batch_no,$godown_id);
		echo $result;
	}

	// get user role department wise and check department present in role master created by chandrakant
	public function getUesrRole()
	{
		$dept_id=$this->input->post('dept');
		$roleanddept=$this->CommonModel->getUesrRole($dept_id);
		echo $roleanddept;
	}
	
	public function getCustomerOrDealer()
	{
		$type_id=$this->input->post('typeid');
		$data=$this->CommonModel->getCustomerOrDealer($type_id);
		echo $data;
	}
	

	//get item and assambly categories data using dropdown
	public function getItemandAssemblyCategories()
	{
		$type_id=$this->input->post('type');
			
		$ItemAss=$this->CommonModel->getItemandAssemblysCategories($type_id);
		echo $ItemAss;
	}
	
	public function getAssembly()
	{
		$ass_type=$this->input->post('ass_type');
			
		$Ass=$this->CommonModel->getAssemblys($ass_type);
		echo $Ass;
	}
	
	public function getItemSubcategoryByCategory()
	{
		$cat = $this->input->post('item_cat');
		$subcat=$this->CommonModel->getItemSubcategoryByCategory($cat);
		echo $subcat;
	}
	
	public function getItemsForSelect()
	{
		$cat_id = $this->input->post('cat_id');
		$subcat_id = $this->input->post('subcat_id');
		$item=$this->CommonModel->getItems($cat_id,$subcat_id);
		echo $item;
	}
	

	
	//Get module access page list for sorting user controller/ access function
	public function getpagesNames()
	{
			$page_module_id=$this->input->post('page_module');
		$pagenames=$this->CommonModel->getpagesNames($page_module_id);
		echo $pagenames;
	}
	
	public function getProductBrands()
	{
		$brands=$this->CommonModel->getProductBrandDropdown();
		echo $brands;
	}
	public function getProductCategoryByType()
	{
		$product_type=$this->input->post('product_type');
		$cats=$this->CommonModel->getProductCategoriesByType($product_type);
		echo $cats;
	}
	public function getProductsByFilter()
	{
		$type=$this->input->post('type');
		$cat=$this->input->post('cat');
		$brand=$this->input->post('brand');
		$products=$this->CommonModel->getProductsByFilter($type,$cat,$brand);
		echo $products;
	}
	
	// get employee department wise & role wise and check is present in user master created by chandrakant
	public function getEmployeeDepartmentRoleWise()
	{
		$dept_id=$this->input->post('dept');
		$employeeDepartment=$this->CommonModel->getEmployeeDepartmentRoleWise($dept_id);
		echo $employeeDepartment;
	}
	
	//get ASM OR ASO
	
	public function getUserTypeDepartmentRoleWise()
	{
		$dept_id=$this->input->post('dept');
		$employeeDepartment=$this->CommonModel->getUserTypeDepartmentRoleWise($dept_id);
		echo $employeeDepartment;
	}
	// current ASM/ASO
	public function getUserTypeDepartmentRoleWiseAsoASE()
	{
		$dept_id=$this->input->post('dept');
		$employeeDepartment=$this->CommonModel->getUserTypeDepartmentRoleWiseAsoASE($dept_id);
		echo $employeeDepartment;
	}
	
	
	public function getProjectClientDetails()
	{
		$project_id=$this->input->post('project');
		$districts=$this->CommonModel->getProjectClientDetails($project_id);
		echo $districts;
	}
	
	public function checkClientProject()
	{
		$client_id = $this->input->post('client_id');
		$result=$this->CommonModel->checkClientProjectDetails($client_id);
		echo $result;
	}
	
	
	// get User create employee wise created by chandrakant
	public function getUsersDepartmentWise()
	{
		$dept_id=$this->input->post('dept');
		$employeeDepartment=$this->CommonModel->getUsersDepartmentWise($dept_id);
		echo $employeeDepartment;
	}
	
	// User delete
	public function delete_user()
	{
		$user_id = $this->input->post('uid');
		$userdelete=$this->CommonModel->getUsersDelete($user_id);
		echo $userdelete;
	}
	
	// Delete department
	public function delete_designation()
	{
		$desigid_new = $this->input->post('desigid');
		$designdelete=$this->CommonModel->getDesignationDelete($desigid_new);
		echo $designdelete;
	}
	
	// Delete department
	public function delete_region()
	{
		$regid_new = $this->input->post('regid');
		$regiondelete=$this->CommonModel->getRegionDelete($regid_new);
		echo $regiondelete;
	}
	// delete region district
	public function delete_region_district()
	{
		$regid_new = $this->input->post('regid');
		$regiondistrdelete=$this->CommonModel->getRegionDistrictDelete($regid_new);
		echo $regiondistrdelete;
	}
	
	// delete Service type
	public function service_type_delete()
	{
		$stid_new = $this->input->post('stid');
		$stdelete=$this->CommonModel->service_type_delete($stid_new);
		echo $stdelete;
	}
	
	// delete Product Category
	public function product_category_delete()
	{
		$pcid_new = $this->input->post('pcid');
		$pcdelete=$this->CommonModel->product_category_delete($pcid_new);
		echo $pcdelete;
	}
	
	// delete Product type
	public function product_type_delete()
	{
		$pcid_new = $this->input->post('ptid');
		$ptdelete=$this->CommonModel->product_type_delete($pcid_new);
		echo $ptdelete;
	}
	
	// delete District
	public function district_delete()
	{
		$distid_new = $this->input->post('distid');
		$ptdelete=$this->CommonModel->district_delete($distid_new);
		echo $ptdelete;
	}
	
	// delete taluka
	public function taluka_delete()
	{
		$talid_new = $this->input->post('talid');
		$ptdelete=$this->CommonModel->taluka_delete($talid_new);
		echo $ptdelete;
	}
	
	// delete State
	public function state_delete()
	{
		$stateid_new = $this->input->post('stateid');
		$stdelete=$this->CommonModel->state_delete($stateid_new);
		echo $stdelete;
	}
	
	// delete Project
	public function project_delete()
	{
		$opid_new = $this->input->post('opid');
		$opdelete=$this->CommonModel->project_delete($opid_new);
		echo $opdelete;
	}
	
	// delete Product (Application) with tracking ids
	public function delete_product_with_tracking()
	{
		$appid_new = $this->input->post('appid');
		$appdelete=$this->CommonModel->delete_product_with_tracking($appid_new);
		echo $appdelete;
	}
	
	
	// delete Enterprice
	public function enterprice_delete()
	{
		$cid_new = $this->input->post('cid');
		$cdelete=$this->CommonModel->enterprice_delete($cid_new);
		echo $cdelete;
	}
	
	// delete Employee
	public function employee_delete()
	{
		$eid_new = $this->input->post('eid');
		$edelete=$this->CommonModel->employee_delete($eid_new);
		echo $edelete;
	}
	
	// delete Client
	public function client_delete()
	{
		$cid_new = $this->input->post('cid');
		$cdelete=$this->CommonModel->client_delete($cid_new);
		echo $cdelete;
	}
	
	
	// delete Client
	public function service_delete()
	{
		$sid_new = $this->input->post('sid');
		$sdelete=$this->CommonModel->service_delete($sid_new);
		echo $sdelete;
	}
	
	// delete checklist array
	public function checklist_delete()
	{
		$ckid_new = $this->input->post('ckid');
		$cdelete=$this->CommonModel->checklist_delete($ckid_new);
		echo $cdelete;
	}
	
	// delete Scheme array
	public function scheme_delete()
	{
		$sid_new = $this->input->post('sid');
		$sdelete=$this->CommonModel->scheme_delete($sid_new);
		echo $sdelete;
	}
	
	
	// Sanction claim delete
	public function delete_sanction_claim()
	{
		$appid_new = $this->input->post('appid');
		$appdelete=$this->CommonModel->delete_sanction_claim($appid_new);
		echo $appdelete;
	}
	
	// Sanction disbursement delete
	public function delete_disbursement_record()
	{
		$disbid_new = $this->input->post('disbid');
		
		$appdelete=$this->CommonModel->delete_disbursement_record($disbid_new);
		echo $appdelete;
	}
	
	
	// get User department wise
	public function getUserDepartmentWise()
	{
		$region_id=$this->input->post('region_id');
		$deptid=$this->input->post('deptid');
		$userDepartment=$this->CommonModel->getUserDepartmentWise($region_id,$deptid);
		echo $userDepartment;
	}
	
	// delete Service type
	public function costhead_delete()
	{
		$chid_new = $this->input->post('chid');
		$stdelete=$this->CommonModel->costhead_delete($chid_new);
		echo $stdelete;
	}
	
	public function getInvoicesDt()
	{
		$machines=$this->CommonModel->getCostHead();
		echo $machines;
	}
	public function getInvoicesTaxDt()
	{
		$machines=$this->CommonModel->getCostHeadTax($_POST['chid']);
		echo $machines;
	}
	
	// delete invoice by admin 
	
	public function delete_invoice_with_tracking()
	{
		$invid_new = $this->input->post('invid');
		$appdelete=$this->CommonModel->delete_invoice_with_tracking($invid_new);
		echo $appdelete;
	}
	
	// delete receipt by admin 
	public function delete_receipts_with_tracking()
	{
		$recid_new = $this->input->post('recid');
		$appdelete=$this->CommonModel->delete_receipts_with_tracking($recid_new);
		echo $appdelete;
	}
	
	// delete area
	public function area_delete()
	{
		$areaid_new = $this->input->post('areaid');
		$ptdelete=$this->CommonModel->area_delete($areaid_new);
		echo $ptdelete;
	}
	
	// Route assign to user user list function
	public function getUserRouteWise()
	{
		$usr_id=$this->input->post('usrid');
		$employeeDepartment=$this->CommonModel->getUserRouteWise($usr_id);
		echo $employeeDepartment;
	}
	
	
	// Get role wise user
	public function getRoleWiseUser()
	{
		$roleid=$this->input->post('roleid');
		$employeeDepartment=$this->CommonModel->getRoleWiseUser($roleid);
		echo $employeeDepartment;
	}
	
	// Get User wise assign route customer
	public function getUserClientList()
	{
		$userid=$this->input->post('userid');
		$employeeDepartment=$this->CommonModel->getUserClientList($userid);
		echo $employeeDepartment;
	}
	
	// Delete payment
	public function payment_delete()
	{
		$payid_new = $this->input->post('payid');
		$regiondelete=$this->CommonModel->payment_delete($payid_new);
		echo $regiondelete;
	}
	
	public function getTypeWiseUser()
	{
		$typeid=$this->input->post('typeid');
		$employeeDepartment=$this->CommonModel->getTypeWiseUser($typeid);
		echo $employeeDepartment;
	}
	
	
	// Find Pending Amount
	public function find_pending_amount()
	{
		$userid=$this->input->post('userid');
		$employeeDepartment=$this->CommonModel->find_pending_amount($userid);
		echo $employeeDepartment;
	}
	
	// Get Product List
	public function getUserProductList()
	{
		$roleid=$this->input->post('roleid');
		$employeeDepartment=$this->CommonModel->getUserProductList($roleid);
		echo $employeeDepartment;
	}
	
}
