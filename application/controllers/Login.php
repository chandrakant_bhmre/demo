<?php
//-----------------------------------------
// Create By 			: Chandrakant Bhamare
// Purpose   			: login and logout 
// Created Date 		: 01-03-2022,
// Modify By 			: 
// Modify Dates 		: 
// Other Information	: 
//-----------------------------------------
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller 
{
	function __construct()
	{
		parent::__construct();
		$this->load->model( 'CommonModel');
	}
	
	
	 public function index()
    { 
		if($this->session->userdata('usera_id'))
		{
			redirect(base_url('admin/home'));
		}
		else
		{
			$this->load->view('login/login');
		}
    }
	
	 public function userlogin()
    {
		// Login User name Password validation check 
		$this->form_validation->set_rules('u_name', 'Username', 'required');
		$this->form_validation->set_rules('u_password', 'Password', 'required');
		
		if ($this->form_validation->run()!= FALSE)
        {
			// check post or get variable data 
			$u_nnm=trim($this->input->post('u_name','',true));
			
			$u_password=trim($this->input->post('u_password','',true));
			
			// create session id and other dife parameters 
			$session_id            = $this->session->userdata('session_id');
			$status                = array();
			$msg                   = array();
			$response              = array();
			$error_message         = array();
			$redirection           = array();
			$data                  = json_decode(file_get_contents("php://input"));
			// Create web service path 
			$login_webservice_path = WEBSERVICE_PATH . "common/pro_user_login.php";
			
			// Create in parameters array
			$login_param_array     = array(
				'PUNM' => $u_nnm,
				'PCPW' => $u_password
			);
			// Call controler parameters to webserivces and return valid array 
			$login_web_response    = $this->utility_model->callback_webservice($login_webservice_path, $login_param_array);
			
			//echo '<pre>';print_r($login_web_response);echo '<pre>';exit;
			$user_details          = array();
			if ($login_web_response['http_code'] == 200) {
				//print_r($login_web_response);
				
				$json              = json_encode($login_web_response['response']);
				$log_web_res       = json_decode($login_web_response['response']);
				
				if ($log_web_res->XSTS == 1) 
				{
					
					$token_id = $login_web_response['token-id'];
				//echo $log_web_res->XUID."----"; exit;
					// token set in session id
					$this->utility_model->set_session_tokenid($token_id);
					// get value from json web service array
					/*$user_id      = $log_web_res->XUID;
					$first_name    = $log_web_res->XFNM;
					$last_name     = $log_web_res->XLNM;
					$email         = $log_web_res->XEMAIL;
					$mobile         = $log_web_res->XMOBILE;
					$age		   =  $log_web_res->XAGE;
					$utoken         = $log_web_res->XTOKEN;
					$city		   =  $log_web_res->XCITY;
					$gender		   =  $log_web_res->XGENDER;*/
					// create array to comman variables
					
					$user_details  = array(
						'user_id' => $log_web_res->XUID,
						'first_name' => $log_web_res->XFNM,
						'last_name' =>  $log_web_res->XLNM,
						'email' => $log_web_res->XEMAIL,
						'mobile' => $log_web_res->XMOBILE,
						'token_id' => $token_id,
						'age' => $log_web_res->XAGE,
						'city' => $log_web_res->XCITY,
						'gender' => $log_web_res->XGENDER,
						'login_flag' => '1'
					);
					//	print_r($user_details);exit;	
					// set get parameter to global session array only for controllers
						
						$this->session->set_userdata($user_details);
						
						$status      = "success";
						$msg         = "dashboard";
						//$redirection = "common/home";
						redirect(base_url('admin/home'));
						
				}else{
						$status      = "failed";
						$msg         = "login";
						$redirection = "";
					}
				}else{
				$status             = "error";
			}
		}
		
		
		
		redirect(base_url('login'));
    }
	
	
	 public function register()
    {
		
		
		
		$this->form_validation->set_rules('first_name', 'first_name', 'required');
		$this->form_validation->set_rules('last_name', 'last_name', 'required');
		$this->form_validation->set_rules('mobile_no', 'mobile_no', 'required');
		$this->form_validation->set_rules('email', 'email', 'required');
		$this->form_validation->set_rules('age', 'age', 'required');
		$this->form_validation->set_rules('city', 'city', 'required');
		$this->form_validation->set_rules('gender', 'gender', 'required');
		$this->form_validation->set_rules('u_password', 'Password', 'required');
		
		if ($this->form_validation->run()!= FALSE)
        {
			// check post or get variable data 
			$first_name=trim($this->input->post('first_name','',true));
			$last_name=trim($this->input->post('last_name','',true));
			$mobile_no=trim($this->input->post('mobile_no','',true));
			$email=trim($this->input->post('email','',true));
			$age=trim($this->input->post('age','',true));
			$city=trim($this->input->post('city','',true));
			$gender=trim($this->input->post('gender','',true));
			$u_password=trim($this->input->post('u_password','',true));
			
			
			// create session id and other dife parameters 
			$session_id            = $this->session->userdata('session_id');
			$status                = array();
			$msg                   = array();
			$response              = array();
			$error_message         = array();
			$redirection           = array();
			$data                  = json_decode(file_get_contents("php://input"));
			// Create web service path 
			$login_webservice_path = WEBSERVICE_PATH . "common/pro_user_create.php";
			
			// Create in parameters array
			$login_param_array     = array(
				'first_name' => $first_name,
				'last_name' => $last_name,
				'mobile_no' => $mobile_no,
				'email' => $email,
				'age' => $age,
				'city' => $city,
				'gender' => $gender,
				'u_password' => $u_password
			);
			
		//	print_r($login_param_array);exit;
			// Call controler parameters to webserivces and return valid array 
			$login_web_response    = $this->utility_model->callback_webservice($login_webservice_path, $login_param_array);
			
			//echo '<pre>';print_r($login_web_response);echo '<pre>';exit;
			$user_details          = array();
			if ($login_web_response['http_code'] == 200) {
				//print_r($login_web_response);
				
				$json              = json_encode($login_web_response['response']);
				$log_web_res       = json_decode($login_web_response['response']);
				
				if ($log_web_res->XSTS == 1) 
				{
					
					$token_id = $login_web_response['token-id'];
				//echo $log_web_res->XUID."----"; exit;
					// token set in session id
					$this->utility_model->set_session_tokenid($token_id);
					// get value from json web service array
					/*$user_id      = $log_web_res->XUID;
					$first_name    = $log_web_res->XFNM;
					$last_name     = $log_web_res->XLNM;
					$email         = $log_web_res->XEMAIL;
					$mobile         = $log_web_res->XMOBILE;
					$age		   =  $log_web_res->XAGE;
					$utoken         = $log_web_res->XTOKEN;
					$city		   =  $log_web_res->XCITY;
					$gender		   =  $log_web_res->XGENDER;*/
					// create array to comman variables
					
					$user_details  = array(
						'user_id' => $log_web_res->XUID,
						'first_name' => $log_web_res->XFNM,
						'last_name' =>  $log_web_res->XLNM,
						'email' => $log_web_res->XEMAIL,
						'mobile' => $log_web_res->XMOBILE,
						'token_id' => $token_id,
						'age' => $log_web_res->XAGE,
						'city' => $log_web_res->XCITY,
						'gender' => $log_web_res->XGENDER,
						'login_flag' => '1'
					);
					//	print_r($user_details);exit;	
					// set get parameter to global session array only for controllers
						
						$this->session->set_userdata($user_details);
						
						$status      = "success";
						$msg         = "dashboard";
						//$redirection = "common/home";
						redirect(base_url('admin/home'));
						
				}else{
						$status      = "failed";
						$msg         = "login";
						$redirection = "";
					}
				}else{
				$status             = "error";
			}
		}
		
		
		
		redirect(base_url('login'));
    }
	
	
	public function logout()
    {
		// get session id
		if ($this->session->userdata('user_id') != "") {
			// webservice path
			$logout_webservice_path = WEBSERVICE_PATH . "common/pro_user_logout.php";
			// webservice in parameters array 
            $logout_param_array     = array(
                'PUID' => $this->session->userdata('user_id'), // User id
                'PUTOKEN' => $this->session->userdata('token_id')// user token
			 );
		// Webservice calling function caal thro utility model
            $logout_web_response    = $this->utility_model->callback_webservice_token($logout_webservice_path, $logout_param_array);
			// return array response 
			
			if ($logout_web_response['http_code'] == 200) {
				$logout_web_res = json_decode($logout_web_response['response']);
				
				$status         = $logout_web_res->XSTS;
				//if status 1 i.e. logout successfull
                if ($status == 1) {
					// create session unset array 
                    $user_details = array(
                       'user_id' => '',
						'first_name' => '',
						'last_name' => '',
						'email' => '',
						'mobile' => '',
						'token_id' => '',
						'age' => '',
						'city' => '',
						'gender' => '',
						'login_flag' => '1'
					);
					// unset array and unset session array
                    $this->session->unset_userdata($user_details);
					// distroy all session 
                    @$this->session->sess_destroy();
					redirect(base_url(''));
                }
            } else {
				// Complite distory array
                 $status             = "error";
			     $this->session->unset_userdata('token_id');
			     $this->session->unset_userdata('user_id');
			     $this->session->unset_userdata('miss_token');
				@$this->session->sess_destroy();
				redirect(base_url(''));
            }
        }
		// clear all session variable and distroy session
		@$this->session->sess_destroy();
		redirect(base_url(''));
    }
		
}
