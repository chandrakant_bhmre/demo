<?php
/*
Created By : Chandrakant Bhamare
Purpose : 404 Error management
creation Date : 2018-08-08
Modified By : Chandrakant Bhamare
Modified Date : 2018-08-08
Other Information :
*/
defined('BASEPATH') OR exit('No direct script access allowed');

class Show404 extends CI_Controller 
{
	function __construct()
	{
		parent::__construct();
		 
	}
	
		
	public function index()
	{	
			$data['view_name'] = "error404";
			set_status_header(404); // set 404 header

		  if ($this->uri->segment(1) == 'user') 
		  {
			 
			 	$sa= $this->session->all_userdata();
				$data['sa']=$sa;
				$this->load->view('user/utemplate', $data);	
			 
			 return;
		  }
		
		  if ($this->uri->segment(1) == 'admin') 
		  {
			 
			 	$sa= $this->session->all_userdata();
				$data['sa']=$sa;
				$this->load->view('admin/atemplate', $data);	
			 
			 
			 return;
		  }
		
		  
  		// If there's no match to existing controller, default to generic 404 page view.
 		// $this->load->view('default404');

	}
	
	
}
