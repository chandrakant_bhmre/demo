<?php
//-----------------------------------------
// Create By 			: Chandrakant Bhamare
// Purpose   			: Home Functions
// Created Date 		: 01-03-2022,
// Modify By 			: 
// Modify Dates 		: 
// Other Information	: 
//-----------------------------------------
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller 
{
	function __construct()
	{
		parent::__construct();
		$this->load->model( 'utility_model');
		$this->load->model( 'CommonModel');
		$login_result=$this->utility_model->checkSession();
		
		if($login_result!=1)
		{
			redirect(site_url(),'login');
		}
		if(empty($this->session->userdata("user_id")))
        {
         	redirect(site_url(),'login');
        }
		$this->load->model('DashboardModel');
	}
	
	public function index()
	{
		if($this->session->userdata('user_id'))
		{
		// get book count
		$BOOK_DASH_DATA=$this->DashboardModel->getbook_dashboard();
		
		$data['book_dash']=$BOOK_DASH_DATA;
		
		$sa= $this->session->all_userdata();
		$data['sa']=$sa;
		$data['ACTFLAG']='Home';
		$data['view_name'] = "dashboard_body";
		$this->load->view('admin/atemplate', $data);
		}
		else
		{
			redirect(base_url('login'));
		}
	}
	
	//to show change pass 
	public function changepass()
	{
			$data['view_name'] = "changepass";
			$sa= $this->session->all_userdata();
			$data['sa']=$sa;
			$data['ACTFLAG']='CPASS';
			$this->load->view('admin/atemplate', $data);	
	}
	
	public function checkpass()
	{
		$old_password=$this->input->post('old_password');
		$new_password=$this->input->post('new_password');
		$confirm_password=$this->input->post('confirm_password');
		
		/*******WEBSERVICE CALL******************/
			$SESS_ARRAY = $this->session->all_userdata();
						
			$param_array     = array(
				'P_UID' => $SESS_ARRAY['user_id'],
				'P_TOKEN' => $SESS_ARRAY['token_id'],
				'P_OPASS' => trim($old_password),
				'P_NPASS' => trim($new_password)
				);
				
				
			
		$webservice_path = WEBSERVICE_PATH . "common/pro_change_password.php";
		$web_response = $this->utility_model->callback_webservice_token($webservice_path,$param_array);
		
		//echo '<pre>';print_r($web_response);echo '</pre>'; exit;
		
		$json='';
		
		if($web_response['http_code'] == 200)
			{
				//$json=$web_response['response'];
				$WEB_RESPONSE = json_decode($web_response['response']);
				
								
				$status=$WEB_RESPONSE->X_STS;
				$message=$WEB_RESPONSE->X_MSG;
					
							$json = $json . "{";
							$json = $json . "\"STAT\":\"".$status."\",";
							$json = $json . "\"MSG\":\"".$message."\"";
							$json = $json . "}";
				
			}
			else
			{
							$json = $json . "{";
							$json = $json . "\"STAT\":\"0\",";
							$json = $json . "\"MSG\":\"Unexpected error.\"";
							$json = $json . "}";
				
			}
			
		echo $json;
		
	
		/****************************************/
	}
	// Get  user in view
	public function getsingle_user($usrid)
	{
		$username=$this->CommonModel->get_single_user_list($usrid);
		return $username;
		exit;
		//echo $username;
	}
	
	
}
