<?php
/*Created By : Chandrakant Bhamare
Purpose :user list
creation Date : 2022-03-01
Modified By : 
Modified Date : 
Other Information :
*/
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller 
{
	function __construct()
	{
		parent::__construct();
		$this->load->model( 'utility_model');
		$login_result=$this->utility_model->checkSession();
		
		if($login_result!=1)
		{
			redirect(site_url(),'login');
		}
		if(empty($this->session->userdata("user_id")))
        {
         	redirect(site_url(),'login');
        }
		 $this->load->model( 'UserModel');
	}
	
		
	public function index()
	{	
			$data['view_name'] = "users";
			$sa= $this->session->all_userdata();
			$data['sa']=$sa;
			
			
			
			$data['ACTFLAG']='EMPLI';
			
			$this->load->view('admin/atemplate', $data);	
	}
	// get data for user
	public function getdata()
	{
		
		 $user=$this->UserModel->getUserList();	
		 echo $user;
	}
	
	public function userprofileedit()
	{
		$id= base64_decode($this->input->get('id'));
		$user_data=$this->UserModel->getUserDetails($id);
		
		//print_r($user_data);exit;
		$data['USER_ARRAY']=$user_data;
		
			$data['ACTFLAG']='PROFILE';
			$data['view_name'] = "userprofileedit";
			$sa= $this->session->all_userdata();
			$data['sa']=$sa;
			$this->load->view('admin/atemplate', $data);	
	}
	
	
	public function userprofileedit_save()
	{	
			$update_data=$this->input->post();
			//print_r($update_data);exit;
			$update_data=$this->UserModel->updateProfile($update_data);
			echo $update_data;	
	}
	
	
}
