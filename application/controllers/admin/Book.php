<?php
/*Created By : Chandrakant Bhamare
creation Date : 2022-03-01
Modified By : 
Modified Date : 
Other Information :
*/
defined('BASEPATH') OR exit('No direct script access allowed');

class Book extends CI_Controller 
{
	function __construct()
	{
		parent::__construct();
		$this->load->model( 'utility_model');
		
		$this->load->model( 'CommonModel');
		$login_result=$this->utility_model->checkSession();
		if($login_result!=1)
		{
			redirect(site_url(),'login');
		}
		if(empty($this->session->userdata("user_id")))
        {
         	redirect(site_url(),'login');
        }
		 $this->load->model('BookModel');
 
		 
	}
	

	public function index()
	{	
			$data['view_name'] = "book";
			// session array
			$sa= $this->session->all_userdata();
			$data['sa']=$sa;
			// get state list array
			$data['ACTFLAG']='BOOK';
			$this->load->view('admin/atemplate', $data);
	}
	
	public function getdata()
	{
		 $hq=$this->BookModel->getBookList();	
		 echo $hq;
	}
	
	public function bookadd()
	{
						
			$data['ACTFLAG']='BOOK';
			$data['view_name'] = "bookadd";
			$sa= $this->session->all_userdata();
			$data['sa']=$sa;
			$this->load->view('admin/atemplate', $data);	
	}
	
	public function bookadd_save()
	{
		$insert_data=$this->input->post();
		//print_r($insert_data);exit;
		$insert_res=$this->BookModel->createBook($insert_data);
		echo $insert_res;
	}
	
	public function bookedit()
	{
		$id= base64_decode($this->input->get('id'));
		$user_data=$this->BookModel->getBookDetails($id);
		$data['BOOK_ARRAY']=$user_data;
		
			$data['ACTFLAG']='BOOK';
			$data['view_name'] = "bookedit";
			$sa= $this->session->all_userdata();
			$data['sa']=$sa;
			$this->load->view('admin/atemplate', $data);	
	}
	
	
	public function bookedit_save()
	{	
			$update_data=$this->input->post();
			//print_r($update_data);exit;
			$update_data=$this->BookModel->updateBook($update_data);
			echo $update_data;	
	}
		
}
