<?php
/*Created By : Chandrakant Bhamare
creation Date : 2022-03-01
Modified By : 
Modified Date : 
Other Information :
*/
defined('BASEPATH') OR exit('No direct script access allowed');

class Rentbook extends CI_Controller 
{
	function __construct()
	{
		parent::__construct();
		$this->load->model( 'utility_model');
		
		$this->load->model( 'CommonModel');
		$login_result=$this->utility_model->checkSession();
		if($login_result!=1)
		{
			redirect(site_url(),'login');
		}
		if(empty($this->session->userdata("user_id")))
        {
         	redirect(site_url(),'login');
        }
		 $this->load->model('RentbookModel');
 
		 
	}
	

	public function index()
	{	
			$data['view_name'] = "rentbook";
			// session array
			$sa= $this->session->all_userdata();
			$data['sa']=$sa;
			// get state list array
			$data['ACTFLAG']='RENT';
			$this->load->view('admin/atemplate', $data);
	}
	
	public function getdata()
	{
		 $hq=$this->RentbookModel->getRentBookList();	
		 echo $hq;
	}
	
	public function bookrentadd()
	{
		
			$user=$this->CommonModel->getUserDropdown();	
			$user_array=json_decode($user,TRUE);
			//print_r($this->session->all_userdata());
			$data['users']=$user_array;
			
			// book dropdown
			$book=$this->CommonModel->getBookDropdown();	
			$book_array=json_decode($book,TRUE);
			$data['books']=$book_array;
						
			$data['ACTFLAG']='RENT';
			$data['view_name'] = "bookrentadd";
			$sa= $this->session->all_userdata();
			$data['sa']=$sa;
			$this->load->view('admin/atemplate', $data);	
	}
	
	public function rentbookadd_save()
	{
		$insert_data=$this->input->post();
		//print_r($insert_data);exit;
		$insert_res=$this->RentbookModel->createIssueBook($insert_data);
		echo $insert_res;
	}
	
	public function rentbookedit()
	{
		$id= base64_decode($this->input->get('id'));
		$user_data=$this->RentbookModel->getIssueBookDetails($id);
		$data['BOOK_ARRAY']=$user_data;
		
			$data['ACTFLAG']='RENT';
			$data['view_name'] = "rentbookedit";
			$sa= $this->session->all_userdata();
			$data['sa']=$sa;
			$this->load->view('admin/atemplate', $data);	
	}
	
	
	public function bookrentedit_save()
	{	
			$update_data=$this->input->post();
		//	print_r($update_data);exit;
			$update_data=$this->RentbookModel->updateBookIssue($update_data);
			echo $update_data;	
	}
	
	
	
	public function returnbook()
	{	
			$data['view_name'] = "returnbook";
			// session array
			$sa= $this->session->all_userdata();
			$data['sa']=$sa;
			// get state list array
			$data['ACTFLAG']='RETURN';
			$this->load->view('admin/atemplate', $data);
	}
	
	
	
	public function getdataReturn()
	{
		 $hq=$this->RentbookModel->getreturnBookList();	
		 echo $hq;
	}
	
	
	
		
}
