<!DOCTYPE html>
<html lang="en-us" id="extr-page">
	<head>
		<meta charset="utf-8">
		<title> Progemesis :: Login</title>
		<meta name="description" content="">
		<meta name="author" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
		
		<!-- #CSS Links -->
		<!-- Basic Styles -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/global/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/global/css/font-awesome.min.css">

		<!-- SmartAdmin Styles : Caution! DO NOT change the order -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/global/css/smartadmin-production-plugins.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/global/css/smartadmin-production.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/global/css/smartadmin-skins.min.css">

		<!-- SmartAdmin RTL Support -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/global/css/smartadmin-rtl.min.css"> 

		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/global/css/demo.min.css">

		<!-- #FAVICONS -->
		<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/global/img/favicon/favicon.ico" type="image/x-icon">
		<link rel="icon" href="<?php echo base_url(); ?>assets/global/img/favicon/favicon.ico" type="image/x-icon">

		<!-- #GOOGLE FONT -->
		<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,300,400,700">

		<link rel="apple-touch-icon" href="<?php echo base_url(); ?>assets/global/img/splash/sptouch-icon-iphone.png">
		<link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url(); ?>assets/global/img/splash/touch-icon-ipad.png">
		<link rel="apple-touch-icon" sizes="120x120" href="<?php echo base_url(); ?>assets/global/img/splash/touch-icon-iphone-retina.png">
		<link rel="apple-touch-icon" sizes="152x152" href="<?php echo base_url(); ?>assets/global/img/splash/touch-icon-ipad-retina.png">
		
		<!-- iOS web-app metas : hides Safari UI Components and Changes Status Bar Appearance -->
		<meta name="apple-mobile-web-app-capable" content="yes">
		<meta name="apple-mobile-web-app-status-bar-style" content="black">
		
		<!-- Startup image for web apps -->
		<link rel="apple-touch-startup-image" href="<?php echo base_url(); ?>assets/global/img/splash/ipad-landscape.png" media="screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:landscape)">
		<link rel="apple-touch-startup-image" href="<?php echo base_url(); ?>assets/global/img/splash/ipad-portrait.png" media="screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:portrait)">
		<link rel="apple-touch-startup-image" href="<?php echo base_url(); ?>assets/global/img/splash/iphone.png" media="screen and (max-device-width: 320px)">

	</head>
	
	<body class="animated fadeInDown">

		<header id="header">

			<div id="logo-group" style="width: 210px !important;">
				<span id="logo" > <img src="<?php echo base_url(); ?>assets/global/img/logo.png" alt="Progemesis"> </span>
			</div>

			<span id="extr-page-header-space"> <!--<span class="hidden-mobile hiddex-xs">Need an account?</span> <a href="javascript:void(0);" class="btn btn-danger">Create account</a>--> </span>

		</header>

		<div id="main" role="main">

			<!-- MAIN CONTENT -->
			<div id="content" class="container">
            
            <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-7 col-lg-8 hidden-xs hidden-sm">
            </div>
            <div class="col-xs-12 col-sm-12 col-md-5 col-lg-4">
            <?php 
	  if(isset($_SESSION['status']))
	  {
		  if($_SESSION['status']=='failed')
		  {
			  echo '<div id="alert_danger" class="alert alert-danger alert-dismissible">'.$_SESSION['msg'].'</div>';
		  }
		 /* else
		  {
			  echo '<div id="alert_success" class="alert alert-success alert-dismissible">Login Successfull.</div>';
		  }*/
	  }
	  ?>
      </div>
            </div>

				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
						<div class="well no-padding">
                        <form method="post" id="register-form" class="smart-form client-form" action="<?=base_url()?>login/register">
							<!--<form action="index.html" id="login-form" class="smart-form client-form">-->
								<header>
									Registration Form
								</header>

								<fieldset>
									
									<section>
										<label class="label">First Name<span style="color:red">*</span></label>
										<label class="input"> <i class="icon-append fa fa-user"></i>
											<input autofocus type="text" name="first_name" id="first_name" maxlength="50">
											<b class="tooltip tooltip-top-right"><i class="fa fa-user txt-color-teal"></i> Please enter first name</b></label>
									</section>

									<section>
										<label class="label">Last Name<span style="color:red">*</span></label>
										<label class="input"> <i class="icon-append fa fa-user"></i>
											<input type="text" name="last_name" id="last_name" maxlength="50">
											<b class="tooltip tooltip-top-right"><i class="fa fa-lock txt-color-teal"></i> Enter your last name</b> </label>
										
									</section>
                                    
                                    <section>
										<label class="label">Mobile No<span style="color:red">*</span></label>
										<label class="input"> 
											<input type="number" name="mobile_no" id="mobile_no" maxlength="10">
											<b class="tooltip tooltip-top-right"><i class="fa fa-lock txt-color-teal"></i> Enter your mobile no</b> </label>
										
									</section>
                                    
                                    <section>
										<label class="label">Email-Id<span style="color:red">*</span></label>
										<label class="input"> 
											<input type="email" name="email" id="email" maxlength="255">
											<b class="tooltip tooltip-top-right"><i class="fa fa-lock txt-color-teal"></i> Enter your email</b> </label>
										
									</section>
                                    
                                     <section>
										<label class="label">Age<span style="color:red">*</span></label>
										<label class="input"> 
											<input type="number" name="age" id="age" maxlength="3">
											<b class="tooltip tooltip-top-right"><i class="fa fa-lock txt-color-teal"></i> Enter your age</b> </label>
										
									</section>
                                    
                                     <section>
										<label class="label">City<span style="color:red">*</span></label>
										<label class="input">
											<input type="text" name="city" id="city" maxlength="50">
											<b class="tooltip tooltip-top-right"><i class="fa fa-lock txt-color-teal"></i> Enter your city</b> </label>
										
									</section>
                                    
                                    <section>
                                    <label class="label">Gender<span style="color:red">*</span></label>
										<label class="select">
											<select name="gender" id="gender" title="Select Gender">
                                            <option value="" >Select Gender </option>
												<option value="m">Male </option>
                                                <option value="f"> Female</option>
                                                <option value="o">Other</option>
												
											</select> <i></i> </label>
									</section>
                                    
                                    <section>
										<label class="label">Password<span style="color:red">*</span></label>
										<label class="input"> <i class="icon-append fa fa-lock"></i>
											<input type="password" name="u_password" id="u_password" maxlength="50">
											<b class="tooltip tooltip-top-right"><i class="fa fa-lock txt-color-teal"></i> Enter your password</b> </label>
										
									</section>
                                    
									</fieldset>
								<footer>
									<button type="submit" class="btn btn-primary" name="submit">
										Sign Up
									</button>
                                </footer>
							</form>

						</div>
						
					</div>
					<div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
						<div class="well no-padding">
                        <form method="post" id="login-form" class="smart-form client-form" action="<?=base_url()?>login/userlogin">
							<!--<form action="index.html" id="login-form" class="smart-form client-form">-->
								<header>
									Sign In
								</header>

								<fieldset>
									
									<section>
										<label class="label">MobileNo / User name<span style="color:red">*</span></label>
										<label class="input"> <i class="icon-append fa fa-user"></i>
											<input autofocus type="text" name="u_name" id="u_name" maxlength="10">
											<b class="tooltip tooltip-top-right"><i class="fa fa-user txt-color-teal"></i> Please enter username</b></label>
									</section>

									<section>
										<label class="label">Password<span style="color:red">*</span></label>
										<label class="input"> <i class="icon-append fa fa-lock"></i>
											<input type="password" name="u_password" id="u_password" maxlength="50">
											<b class="tooltip tooltip-top-right"><i class="fa fa-lock txt-color-teal"></i> Enter your password</b> </label>
										<!--<div class="note">
											<a href="<?=base_url()?>login/forgetpassword">Forgot password?</a>
										</div>-->
									</section>
									</fieldset>
								<footer>
									<button type="submit" class="btn btn-primary" name="submit">
										Sign in
									</button>
                                </footer>
							</form>

						</div>
						
					</div>
				</div>
			</div>

		</div>

		<!--================================================== -->	

		<!-- PACE LOADER - turn this on if you want ajax loading to show (caution: uses lots of memory on iDevices)-->
		<script src="<?php echo base_url(); ?>assets/global/js/plugin/pace/pace.min.js"></script>

	    <!-- Link to Google CDN's jQuery + jQueryUI; fall back to local -->
	    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
		<script> if (!window.jQuery) { document.write('<script src="<?php echo base_url(); ?>assets/global/js/libs/jquery-2.1.1.min.js"><\/script>');} </script>

	    <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
		<script> if (!window.jQuery.ui) { document.write('<script src="<?php echo base_url(); ?>assets/global/js/libs/jquery-ui-1.10.3.min.js"><\/script>');} </script>

		<!-- IMPORTANT: APP CONFIG -->
		<script src="<?php echo base_url(); ?>assets/global/js/app.config.js"></script>

		<!-- JS TOUCH : include this plugin for mobile drag / drop touch events 		
		<script src="js/plugin/jquery-touch/jquery.ui.touch-punch.min.js"></script> -->

		<!-- BOOTSTRAP JS -->		
		<script src="<?php echo base_url(); ?>assets/global/js/bootstrap/bootstrap.min.js"></script>

		<!-- JQUERY VALIDATE -->
		<script src="<?php echo base_url(); ?>assets/global/js/plugin/jquery-validate/jquery.validate.min.js"></script>
		
		<!-- JQUERY MASKED INPUT -->
		<script src="<?php echo base_url(); ?>assets/global/js/plugin/masked-input/jquery.maskedinput.min.js"></script>
		
		<!--[if IE 8]>
			
			<h1>Your browser is out of date, please update your browser by going to www.microsoft.com/download</h1>
			
		<![endif]-->

		<!-- MAIN APP JS FILE -->
		<script src="<?php echo base_url(); ?>assets/global/js/app.min.js"></script>

		<script type="text/javascript">
			runAllForms();

			$(function() {
				// Validation
				/*************LOGIN FORM VALIDATION ***************/
		pageSetUp();
			
			$.validator.addMethod("alphanumeric", function(value, element) { //alert(value);
    return this.optional(element) || /^[a-zA-Z0-9 ]+$/i.test(value); 
	/////^\w+$/		
}, "Special characters not allowed");
		var $loginForm = $("#login-form").validate({
	
				// Rules for form validation
				rules : {
					u_name : {
						required : true
					},
					u_password : {
						required : true,
						minlength : 5,
						maxlength : 10
					}
				},
	
				// Messages for form validation
				messages : {
					u_name : {
						required : 'Please enter your user name'
					},
					u_password : {
						required : 'Please enter your password'
					}
				},
	
				// Do not change code below
				errorPlacement : function(error, element) {
					error.insertAfter(element.parent());
				}
			});
			
			
			
			var $regisrtForm = $("#register-form").validate({
	
				// Rules for form validation
				rules : {
					first_name : {
						required : true,
						accept: "[a-zA-Z -/.]"
					},
					last_name : {
						required : true,
						accept: "[a-zA-Z -/.]"
					},
					mobile_no : {
						required : true,
						accept: "[0-9]"
					},
					email : {
						required : true,
						accept: "[a-zA-Z0-9 -/.]"
					},
					city : {
						required : true
					},
					age : {
						required : true,
						accept: "[0-9]"
					},
					gender : {
						required : true
					}
					
					},
		
				// Messages for form validation
				messages : {
					first_name :{
						required : 'Please enter first name.',
						accept:"Accecpt only a to z-/. special character."
					},
					last_name : {
						required : 'Please enter last name.',
						accept:"Accecpt only a to z-/. special character."
					},
					mobile_no : {
						required : 'Please enter mobile.',
						accept:"Accecpt only numbers and (.) special character."
					},
					email : {
						required : 'Please enter email.',
						accept:"Accecpt a-zA-Z0-9 -/."
					},
					city : {
						required : 'Please enter city.',
						accept:"Accecpt a-zA-Z -/."
					},
					age : {
						required : 'Please enter city.',
						accept:"Accecpt 0-9."
					},
					gender : {
						required : 'Please select gender.',
						accept:"Accecpt 0-9."
					}
				},
	
				// Do not change code below
				errorPlacement : function(error, element) {
					error.insertAfter(element.parent());
				}
			});
/*************LOGIN FORM VALIDATION ***************/
			});
		</script>
        
        <script>
$("#alert_danger").fadeTo(2000, 1800).slideUp(1800, function(){
    $("#alert_danger").slideUp(1800);
});

$("#alert_success").fadeTo(2000, 1800).slideUp(1800, function(){
    $("#alert_success").slideUp(1800);
});
</script>

	</body>
</html>