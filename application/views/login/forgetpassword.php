<!DOCTYPE html>
<html lang="en-us" id="extr-page">
	<head>
		<meta charset="utf-8">
		<title> Progemesis :: Book Demo - Forget Password</title>
		<meta name="description" content="">
		<meta name="author" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
		
		<!-- #CSS Links -->
		<!-- Basic Styles -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/global/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/global/css/font-awesome.min.css">

		<!-- SmartAdmin Styles : Caution! DO NOT change the order -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/global/css/smartadmin-production-plugins.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/global/css/smartadmin-production.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/global/css/smartadmin-skins.min.css">

		<!-- SmartAdmin RTL Support -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/global/css/smartadmin-rtl.min.css"> 

		<!-- We recommend you use "your_style.css" to override SmartAdmin
		     specific styles this will also ensure you retrain your customization with each SmartAdmin update.
		<link rel="stylesheet" type="text/css" media="screen" href="css/your_style.css"> -->

		<!-- Demo purpose only: goes with demo.js, you can delete this css when designing your own WebApp -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url(); ?>assets/global/css/demo.min.css">

		<!-- #FAVICONS -->
		<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/global/img/favicon/favicon.ico" type="image/x-icon">
		<link rel="icon" href="<?php echo base_url(); ?>assets/global/img/favicon/favicon.ico" type="image/x-icon">

		<!-- #GOOGLE FONT -->
		<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,300,400,700">

		<!-- #APP SCREEN / ICONS -->
		<!-- Specifying a Webpage Icon for Web Clip 
			 Ref: https://developer.apple.com/library/ios/documentation/AppleApplications/Reference/SafariWebContent/ConfiguringWebApplications/ConfiguringWebApplications.html -->
		<link rel="apple-touch-icon" href="<?php echo base_url(); ?>assets/global/img/splash/sptouch-icon-iphone.png">
		<link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url(); ?>assets/global/img/splash/touch-icon-ipad.png">
		<link rel="apple-touch-icon" sizes="120x120" href="<?php echo base_url(); ?>assets/global/img/splash/touch-icon-iphone-retina.png">
		<link rel="apple-touch-icon" sizes="152x152" href="<?php echo base_url(); ?>assets/global/img/splash/touch-icon-ipad-retina.png">
		
		<!-- iOS web-app metas : hides Safari UI Components and Changes Status Bar Appearance -->
		<meta name="apple-mobile-web-app-capable" content="yes">
		<meta name="apple-mobile-web-app-status-bar-style" content="black">
		
		<!-- Startup image for web apps -->
		<link rel="apple-touch-startup-image" href="<?php echo base_url(); ?>assets/global/img/splash/ipad-landscape.png" media="screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:landscape)">
		<link rel="apple-touch-startup-image" href="<?php echo base_url(); ?>assets/global/img/splash/ipad-portrait.png" media="screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:portrait)">
		<link rel="apple-touch-startup-image" href="<?php echo base_url(); ?>assets/global/img/splash/iphone.png" media="screen and (max-device-width: 320px)">

	</head>
	
	<body class="animated fadeInDown">

		<header id="header">

			<div id="logo-group">
				<span id="logo"> <img src="<?php echo base_url(); ?>assets/global/img/logo.png" alt="Progemesis"> </span>
			</div>

			<span id="extr-page-header-space"> <!--<span class="hidden-mobile hiddex-xs">Need an account?</span> <a href="javascript:void(0);" class="btn btn-danger">Create account</a>--> </span>

		</header>

		<div id="main" role="main">

			<!-- MAIN CONTENT -->
			<div id="content" class="container">

				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-7 col-lg-8 hidden-xs hidden-sm">
						<h1 class="txt-color-red login-header-big">Progemesis</h1>
						<div class="hero">

							<div class="pull-left login-desc-box-l">
								<h4 class="paragraph-header">It's Okay to be Smart. Experience the simplicity of Vijayeebhav, everywhere you go!</h4>
								<div class="login-app-icons">
									<!--<a href="javascript:void(0);" class="btn btn-danger btn-sm">Frontend Template</a>
									<a href="javascript:void(0);" class="btn btn-danger btn-sm">Find out more</a>-->
								</div>
							</div>
							
							<img src="<?php echo base_url(); ?>assets/global/img/demo/iphoneview.png" class="pull-right display-image" alt="" style="width:210px">

						</div>

						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
								<!--<h5 class="about-heading">About SmartAdmin - Are you up to date?</h5>
								<p>
									Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa.
								</p>-->
							</div>
							<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
								<!--<h5 class="about-heading">Not just your average template!</h5>
								<p>
									Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi voluptatem accusantium!
								</p>-->
							</div>
						</div>

					</div>
					<div class="col-xs-12 col-sm-12 col-md-5 col-lg-4">
						<div class="well no-padding">
                        <form action="<?php echo base_url();?>login/forgetpassword_save" id="forgetpass-form" class="smart-form client-form" method="post" onSubmit="return sendPassReset();">
								<header>
									Forgot Password
								</header>

								<fieldset>
									
									<section>
										<label class="label">Enter your email address</label>
										<label class="input"> <i class="icon-append fa fa-envelope"></i>
											<input type="email" name="email" id="email">
											<b class="tooltip tooltip-top-right"><i class="fa fa-envelope txt-color-teal"></i> Please enter email address for password reset</b></label>
									</section>
									<section>
										<span class="timeline-seperator text-center text-primary"> <span class="font-sm">OR</span> </span>
									</section>
									<section>
										<label class="label">Your Username</label>
										<label class="input"> <i class="icon-append fa fa-user"></i>
											<input type="text" name="username" id="username">
											<b class="tooltip tooltip-top-right"><i class="fa fa-user txt-color-teal"></i> Enter your username</b> </label>
										<div class="note">
											<a href="<?=base_url()?>login/userlogin">I remembered my password!</a>
										</div>
									</section>

								</fieldset>
								<footer>
									<button type="submit" class="btn btn-primary">
										<i class="fa fa-refresh"></i> Reset Password
									</button>
								</footer>
							</form>

							<!-- Modal -->
				<div class="modal fade" id="myModal" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false" aria-labelledby="myModalLabel" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
									&times;
								</button>
								<h4 class="modal-title" id="myModalLabel">OTP</h4>
							</div>
							<div class="modal-body">
				
								<div class="row">
									<div class="col-md-12">
										<div class="form-group">
											<input type="text" name="entered_otp" id="entered_otp" class="form-control" placeholder="Enter OTP" maxlength="6" required />
										</div>
									</div>
								</div>
								
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-default" data-dismiss="modal">
									Cancel
								</button>
                                <!--<button type="button" class="btn btn-primary" name="resend" id="resend" onclick="resendOTPSponser()">
									Resend
								</button>-->
								<button type="button" class="btn btn-primary" name="validate_otp" id="validate_otp" onclick="validateOTP();">
									Submit
								</button>
							</div>
						</div><!-- /.modal-content -->
					</div><!-- /.modal-dialog -->
				</div><!-- /.modal -->

						</div>
						
						<!--<h5 class="text-center"> - Or sign in using -</h5>
															
							<ul class="list-inline text-center">
								<li>
									<a href="javascript:void(0);" class="btn btn-primary btn-circle"><i class="fa fa-facebook"></i></a>
								</li>
								<li>
									<a href="javascript:void(0);" class="btn btn-info btn-circle"><i class="fa fa-twitter"></i></a>
								</li>
								<li>
									<a href="javascript:void(0);" class="btn btn-warning btn-circle"><i class="fa fa-linkedin"></i></a>
								</li>
							</ul>-->
						
					</div>
				</div>
			</div>

		</div>
        
        

		<!--================================================== -->	

		<!-- PACE LOADER - turn this on if you want ajax loading to show (caution: uses lots of memory on iDevices)-->
		<script src="<?php echo base_url(); ?>assets/global/js/plugin/pace/pace.min.js"></script>

	    <!-- Link to Google CDN's jQuery + jQueryUI; fall back to local -->
	    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
		<script> if (!window.jQuery) { document.write('<script src="<?php echo base_url(); ?>assets/global/js/libs/jquery-2.1.1.min.js"><\/script>');} </script>

	    <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
		<script> if (!window.jQuery.ui) { document.write('<script src="<?php echo base_url(); ?>assets/global/js/libs/jquery-ui-1.10.3.min.js"><\/script>');} </script>

		<!-- IMPORTANT: APP CONFIG -->
		<script src="<?php echo base_url(); ?>assets/global/js/app.config.js"></script>

		<!-- JS TOUCH : include this plugin for mobile drag / drop touch events 		
		<script src="js/plugin/jquery-touch/jquery.ui.touch-punch.min.js"></script> -->

		<!-- BOOTSTRAP JS -->		
		<script src="<?php echo base_url(); ?>assets/global/js/bootstrap/bootstrap.min.js"></script>
        
        <!-- CUSTOM NOTIFICATION -->
		<script src="<?php echo base_url(); ?>assets/global/js/notification/SmartNotification.min.js"></script>

		<!-- JQUERY VALIDATE -->
		<script src="<?php echo base_url(); ?>assets/global/js/plugin/jquery-validate/jquery.validate.min.js"></script>
		
		<!-- JQUERY MASKED INPUT -->
		<script src="<?php echo base_url(); ?>assets/global/js/plugin/masked-input/jquery.maskedinput.min.js"></script>
		
		<!--[if IE 8]>
			
			<h1>Your browser is out of date, please update your browser by going to www.microsoft.com/download</h1>
			
		<![endif]-->

		<!-- MAIN APP JS FILE -->
		<script src="<?php echo base_url(); ?>assets/global/js/app.min.js"></script>

		<script type="text/javascript">
			runAllForms();
			
			$('#myModal').on('shown.bs.modal', function () {
    $('#entered_otp').focus();
}) ;

			function sendPassReset()
			{
				if($('#email').val()=='' && $('#username').val()=='')
				{
					$.SmartMessageBox({
											title : "Alert!",
											content : 'Please enter either email or user name.',
											buttons : '[Ok]'
										}, function(ButtonPressed) {
											if (ButtonPressed === "Ok") 
											{
												//
											}
											
										});
										return false;
				}
				else
				{
				if($('#email').val()==''){var email='n';}else{var email=$('#email').val();}
				if($('#username').val()==''){var username='n';}else{var username=$('#username').val();}
				sendOTP(email,username);
				
				return false;
				}
				
				
			}//pass reset func end
			
			
			function sendOTP(email,username)
			{
				var data;
				var otp=Math.floor(100000 + Math.random() * 900000);
				if(!email && !username)
				{
					var data = $('#forgetpass-form').serialize();
					data+="&EN="+encodeURIComponent(otp)+"";
				}
				else
				{
					data="EN="+encodeURIComponent(otp)+"&UNM="+username+"&EMAIL="+email+"";
				}
				
				data+='&ROLE=AD';
				
				$.ajax({
						 type: "POST",
						 url: "<?php echo base_url(); ?>Website/sendOTP", 
						 data:data,
						 cache:false,
						 success: 
							  function(resp){ 
								var d=JSON.parse(resp);
								
								if(d.STAT!=1)//error
								{
									$.SmartMessageBox({
											title : "Alert!",
											content : d.MSG,
											buttons : '[Ok]'
										}, function(ButtonPressed) {
											if (ButtonPressed === "Ok") 
											{
												//
											}
											
										});
									
								}
								else
								{
									$("#myModal").modal();
									$("#resend").hide();
									//setTimeout(function() { $("#resend").show(); }, 20000);
								}
								
						  }
						});	
					

			}//SEND OTP END
			


			function reset_password()
			{
				site_path = '<?php echo base_url(); ?>' ;
				var data = $('#forgetpass-form').serialize();
				data+='&role=AD';
				$.ajax({
						 type: "POST",
						 url: site_path + "Website/forgetpassword_save", 
						 data: data,
						 cache:false,
						 success: 
							  function(resp){ //alert('reset'+resp);
								
								var d = JSON.parse(resp);
								if(d.STAT!=1)//not valid
								{
									$.SmartMessageBox({
											title : "Alert!",
											content : d.MSG,
											buttons : '[Ok]'
										}, function(ButtonPressed) {
											if (ButtonPressed === "Ok") 
											{
												//
											}
											
										});
								}//if !=1
								else
								{ 
								
								$.SmartMessageBox({
											title : "Alert!",
											content : 'Password sent to your email and SMS.',
											buttons : '[Ok]'
										}, function(ButtonPressed) {
											if (ButtonPressed === "Ok") 
											{
												window.location.href=site_path+"v-admin/login";
											}
											
										});
								
															
								}//else
								
								
						  }
						});
			}// RESET PASS END
			
			function validateOTP()
			{
				
	entered_val=$('#entered_otp').val();
	
	$.ajax({
			 type: "POST",
			 url: "<?php echo base_url(); ?>Website/validateOTP",
			 data: "QUEN="+encodeURIComponent(entered_val)+"",
			 cache:false,
			 success: 
				  function(resp)
				  {  
				  	 if(resp==1)
					  {
						  $('#myModal').modal('toggle');
						  reset_password();
					  }
					  else
					  {
						$.SmartMessageBox({
											title : "Alert!",
											content : 'You are not authorised for this entry! ',
											buttons : '[Ok]'
										}, function(ButtonPressed) {
											if (ButtonPressed === "Ok") 
											{
												//
											}
											
										});
						 
					  }
										
				  }
			});	

			}//VALIDATE OTP
			
		</script>

	</body>
</html>