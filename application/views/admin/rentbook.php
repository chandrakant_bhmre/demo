<!--
* Created By : Chandrakant Bhamare
* Created On : 2022-03-01
* Modified By : 
* Modified On : 
* Purpose : To Show All Rent Book
-->
		<!-- MAIN PANEL -->
		<div id="main" role="main">

			<!-- RIBBON -->
			<div id="ribbon">

				<span class="ribbon-button-alignment"> 
					<span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
						<i class="fa fa-refresh"></i>
					</span> 
				</span>

				<!-- breadcrumb -->
				<ol class="breadcrumb">
					<li><a href="<?php echo base_url(); ?>admin/home">Home</a></li><li>Issue Book List</li>
				</ol>
				<!-- end breadcrumb -->

			</div>
			<!-- END RIBBON -->

			<!-- MAIN CONTENT -->
			<div id="content">
				<!-- widget grid -->
				<section id="widget-grid" class="">
				
					<!-- row -->
					<div class="row">
				
						<!-- NEW WIDGET START -->
						<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				
							
							<!-- Widget ID (each widget will need unique ID)-->
							<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-1" data-widget-editbutton="false" data-widget-fullscreenbutton="false" data-widget-togglebutton="false" data-widget-deletebutton="false" data-widget-colorbutton="false">
								
								<header>
									<span class="widget-icon"> <i class="fa fa-product-hunt"></i> </span>
									<h2>Issue Book</h2><?php //if($this->session->userdata('urole')==1){ ?><span style="float:right; padding-right:5%"><a href="<?php echo base_url();?>admin/rentbook/bookrentadd" class="btn btn-sm btn-success "><i class="fa fa-plus"> </i> Issue Book</a></span><?php //}?>
				
								</header>
				
								<!-- widget div-->
								<div>
				
									<!-- widget edit box -->
									<div class="jarviswidget-editbox">
										<!-- This area used as dropdown edit box -->
				
									</div>
                                    
                                    
                                    
									<!-- end widget edit box -->
				
									<!-- widget content -->
									<div class="widget-body no-padding">
				
										<table id="dt_rent_book_list" class="table table-striped table-bordered" width="100%">										<thead>			                
												<tr>
												    <th data-hide="phone">#</th>
                                                   <th data-hide="phone">User Name</th>
                                                   <th data-hide="phone">Book Name</th>
                                                   <th data-hide="phone">Issue Date</th>
                                                   <th data-hide="phone">Rent</th>
                                                   <th>Actions</th>
												</tr>
											</thead>

				
									        <tbody>
									        </tbody>
									
										</table>
				
									</div>
									<!-- end widget content -->
				
								</div>
								<!-- end widget div -->
				
							</div>
							<!-- end widget -->
				
							
						</article>
						<!-- WIDGET END -->
				
					</div>
				
					<!-- end row -->
				
					<!-- end row -->
				
				</section>
				<!-- end widget grid -->
                
               
			</div>
			<!-- END MAIN CONTENT -->

		</div>
		<!-- END MAIN PANEL -->

		