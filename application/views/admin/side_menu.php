<aside id="left-panel">

			<!-- User info -->
			<div class="login-info">
				<span> <!-- User image size is adjusted inside CSS, it should stay as it --> 
					<!--<?php// //echo base_url() ?>admin/profile/profileview-->
                    <?php //echo base_url(); admin/profile?>
					<a id="show-shortcut1" data-action="toggleShortcut1" href="<?php echo base_url();?>admin/user/userprofileedit?id=<?php echo base64_encode($this->session->userdata('user_id'));?>">
						<img src="<?php echo base_url(); ?>assets/global/img/avatars/ch.png" alt="me"/> 
						<span>
							<?php echo $this->session->userdata('first_name');?>

						</span>
						
					</a> 
					
				</span>
			</div>
			<!-- end user info -->

			<!-- NAVIGATION : This navigation is also responsive-->
			<nav>
				<ul>
                  
                	<li class="<?php
					 $user_role=$this->session->userdata('urole');
					 $deptid=$this->session->userdata('deptid');
					
					 if($ACTFLAG=='Home'){?> active<?php }?>">
						<a href="<?php echo base_url(); ?>admin/home/" title="Dashboard"><i class="fa fa-lg fa-fw fa-home"></i> <span class="menu-item-parent">Dashboard</span></a>
					</li>
                    
                 	<li class="top-menu-invisible1 <?php if(($ACTFLAG=='EMPADD') || ($ACTFLAG=='EMPLI')){?>active<?php }?>">
						<a href="javascript:void(0);"><i class="fa fa-lg fa-fw fa-list txt-color-blue"></i> <span class="menu-item-parent">Masters</span></a>
						<ul>
                       
                        	<!-- Employee Master-->
                           
                                 
                            <li class="<?php if(($ACTFLAG=='EMPADD') || ($ACTFLAG=='EMPLI')){?>active<?php }?>">
                            <a href="<?php echo base_url(); ?>admin/user"><i class="fa fa-lg fa-fw fa-user"></i> User List</a>
                            </li>
                             <!-- closed Employee Master-->
                            
                             
                            <!-- Client-->
                            <!--Product Item -->
                            <li class="<?php if($ACTFLAG=='BOOK'){?>active<?php }?>">
								<a href="<?php echo base_url();?>admin/book"><i class="fa fa-lg fa-fw fa-product-hunt"></i> Book List</a>
                            </li>
                            <!--Closed Product Item -->
                           
                           
                        </ul>
					</li>
                    
                    
                    
                    <li class="top-menu-invisible1 <?php if(($ACTFLAG=='RENT') || ($ACTFLAG=='RETURN')){?>active<?php }?>">
						<a href="javascript:void(0);"><i class="fa fa-lg fa-fw fa-list txt-color-blue"></i> <span class="menu-item-parent">Transaction</span></a>
						<ul>
                       
                        	<!-- Employee Master-->
                           
                                 
                            <li class="<?php if(($ACTFLAG=='RENT') || ($ACTFLAG=='RENT')){?>active<?php }?>">
                            <a href="<?php echo base_url(); ?>admin/rentbook"><i class="fa fa-lg fa-fw fa-user"></i> Issue Book  List</a>
                            </li>
                             <!-- closed Employee Master-->
                            
                             
                            <!-- Client-->
                            <!--Product Item -->
                            <li class="<?php if($ACTFLAG=='RETURN'){?>active<?php }?>">
								<a href="<?php echo base_url();?>admin/rentbook/returnbook"><i class="fa fa-lg fa-fw fa-product-hunt"></i> Return List</a>
                            </li>
                            <!--Closed Product Item -->
                           
                           
                        </ul>
					</li>
                    
                   
                    
                                     
                                        
                                       
                  <li class="<?php if($ACTFLAG=='CPASS'){?>active<?php }?>">
						<a href="<?php echo base_url(); ?>admin/home/changepass" title="change password"><i class="fa fa-lg fa-fw fa-key"></i> <span class="menu-item-parent">Change Password</span></a>
					</li>
                    
                    <li class="">
						<a href="<?php echo base_url(); ?>logout" title="Logout"><i class="fa fa-power-off fa-lg"></i> <span class="menu-item-parent">Logout</span></a>
					</li>
                    
               </ul>
			</nav>
			

			<span class="minifyme" data-action="minifyMenu"> 
				<i class="fa fa-arrow-circle-left hit"></i> 
			</span>

		</aside>