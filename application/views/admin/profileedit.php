<!--
Created By : Chandrakant Bhamare
Creted On : 2018-11-03
Modified By : 
Modified On : 
Purpose : Edit Company Profile
Other information : 
-->

		<!-- MAIN PANEL -->
		<div id="main" role="main">

			<!-- RIBBON -->
			<div id="ribbon">

				<span class="ribbon-button-alignment"> 
					<span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
						<i class="fa fa-refresh"></i>
					</span> 
				</span>

				<!-- breadcrumb -->
				<ol class="breadcrumb">
					<li><a href="<?php echo base_url(); ?>admin/home">Home</a></li><li><a href="<?php echo base_url(); ?>admin/profile">Company Profile</a></li><li>Update Company Profile</li>
				</ol>
				<!-- end breadcrumb -->

				<!-- You can also add more buttons to the
				ribbon for further usability

				Example below:

				<span class="ribbon-button-alignment pull-right">
				<span id="search" class="btn btn-ribbon hidden-xs" data-title="search"><i class="fa-grid"></i> Change Grid</span>
				<span id="add" class="btn btn-ribbon hidden-xs" data-title="add"><i class="fa-plus"></i> Add</span>
				<span id="search" class="btn btn-ribbon" data-title="search"><i class="fa-search"></i> <span class="hidden-mobile">Search</span></span>
				</span> -->

			</div>
			<!-- END RIBBON -->

<!-- MAIN CONTENT -->
			<div id="content">

<!--<div class="alert alert-block alert-success">
	<a class="close" data-dismiss="alert" href="#">×</a>
	<h4 class="alert-heading"><i class="fa fa-check-square-o"></i> Check validation!</h4>
	<p>
		You may also check the form validation by clicking on the form action button. Please try and see the results below!
	</p>
</div>-->

<!-- widget grid -->
<section id="widget-grid" class="">


	<!-- START ROW -->

	<div class="row">

		<!-- NEW COL START -->
		<article class="col-sm-12 col-md-12 col-lg-12">
			
			<!-- Widget ID (each widget will need unique ID)-->
			<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-1" data-widget-editbutton="false" data-widget-fullscreenbutton="false" data-widget-togglebutton="false" data-widget-deletebutton="false" data-widget-colorbutton="false">
				<!-- widget options:
					usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">
					
					data-widget-colorbutton="false"	
					data-widget-editbutton="false"
					data-widget-togglebutton="false"
					data-widget-deletebutton="false"
					data-widget-fullscreenbutton="false"
					data-widget-custombutton="false"
					data-widget-collapsed="true" 
					data-widget-sortable="false"
					
				-->
				<header>
					<span class="widget-icon"> <i class="fa fa-user"></i> </span>
					<h2>Update Company Profile </h2>				
					
				</header>

				<!-- widget div-->
				<div>
					
					<!-- widget edit box -->
					<div class="jarviswidget-editbox">
						<!-- This area used as dropdown edit box -->
						
					</div>
					<!-- end widget edit box -->
					
					<!-- widget content -->
					<div class="widget-body no-padding">
                    
                                        
						<form id="company-update-form" action="<?php echo base_url();?>admin/profile/companyedit_save" class="smart-form" method="post" novalidate>
                        
                        
                        
                        <header>Company Details</header>
							<fieldset>
								<div class="row">
									
									<section class="col col-6">
										<label class="input"> <i class="icon-prepend fa fa-user"></i>
											<input type="text" name="company_name" placeholder="Company name" title="Company name" value="<?php echo $company_array['X_CNM']; ?>">
                                            <b class="tooltip tooltip-bottom-right">Enter company name</b> 
										</label>
									</section>
                                    
                                    <section class="col col-3">
										<label class="input"> <i class="icon-prepend fa fa-user"></i>
											<input type="text" name="company_code" placeholder="Company code" title="Company code" value="<?php echo $company_array['X_CCODE']; ?>">
                                            <b class="tooltip tooltip-bottom-right">Enter company code</b> 
										</label>
									</section>
                                    
                                    <section class="col col-3">
										<label class="input"> <i class="icon-prepend fa fa-user"></i>
											<input type="text" name="company_type" placeholder="Company type" title="Company type" value="<?php echo $company_array['X_CTYPE']; ?>">
                                            <b class="tooltip tooltip-bottom-right">Enter company type</b> 
										</label>
									</section>
                                    
								</div>
                                
                                <section>
									<label for="company_address" class="input">
										<input type="text"  onkeypress="return isAddress(event)" name="company_address" id="company_address" placeholder="Address" value="<?php echo $company_array['X_CDDR']; ?>" title="Enter address">
                                        <b class="tooltip tooltip-bottom-right">Enter company address</b> 
									</label>
								</section>
                                
                                <div class="row">
									<section class="col col-3">
										<label class="select">
											<select name="state_id" id="state_id" onchange="getdistrict(this.value,'dist_id')" title="Select State" ><!--- class="required"-->
												<option value="">State</option>
												<?php
													foreach($states as $state)
													{
														$select='';
														if($company_array['X_STATEID']==$state['X_SID'])
														{$select='selected="selected"';}
														
														echo '<option value="'.$state['X_SID'].'" '.$select.'>'.$state['X_SNAME'].'</option>';
													}
												?>
											</select> <i></i> 
                                            <b class="tooltip tooltip-bottom-right">Needed to select state</b> 
                                            </label>
									</section>

									<section class="col col-3">
										<label class="select">
											<select name="dist_id" id="dist_id" title="Select district"><!-- class="required"-->
												<option value="">District</option>
                                                <?php
													foreach($districts as $district)
													{
														$select='';
														if($company_array['X_DISTID']==$district['ID'])
														{$select='selected="selected"';}
														
														echo '<option value="'.$district['ID'].'" '.$select.'>'.$district['DNM'].'</option>';
													}
												?>
											</select> <i></i>
                                            <b class="tooltip tooltip-bottom-right">Needed to select district</b> 
                                         </label>
									</section>
									
                                    <section class="col col-3">
										<label class="input">
											<input type="text" name="city_name" placeholder="City name" value="<?php echo $company_array['X_CITYNM']; ?>" title="Enter city name">
                                            <b class="tooltip tooltip-bottom-right">Needed to enter city Name</b> 
										</label>
									</section>

									<section class="col col-3">
										<label class="input">
											<input type="text" name="pin_code" id="pin_code" maxlength="6" onkeypress="return isNumberKey(event)" placeholder="Pin code" value="<?php echo $company_array['X_PINCODE']; ?>" title="Enter pincode">
                                            <b class="tooltip tooltip-bottom-right">Enter pincode</b> 
										</label>
									</section>
								</div>
                                
                                <div class="row">
                                	<section class="col col-3">
										<label class="input"> <i class="icon-prepend fa fa-mobile"></i>
											<input type="tel" name="mobile_no" id="mobile_no" maxlength="10" placeholder="mobile_no" value="<?php echo $company_array['X_MOBNO']; ?>" title="Emter mobile no">
                                            <b class="tooltip tooltip-bottom-right">Enter Mobile number</b> 
										</label>
									</section>
                                    <section class="col col-3">
										<label class="input"> <i class="icon-prepend fa fa-phone"></i>
											<input type="tel" name="phone_no" id="phone_no" maxlength="12" placeholder="Phone" value="<?php echo $company_array['X_PHNO']; ?>" title="Enter phone no">
                                            <b class="tooltip tooltip-bottom-right">Enter Phone number</b> 
										</label>
									</section>
                                    
                                    <section class="col col-6">
										<label class="input"> <i class="icon-prepend fa fa-envelope-o"></i>
											<input type="email" name="email" id="email" placeholder="E-mail" value="<?php echo $company_array['X_EMAIL']; ?>" required="required" title="Enter email address">
                                            <b class="tooltip tooltip-bottom-right">Enter email address</b> 
										</label>
									</section>
                                    
                                </div>
                                <div class="row">
									<section class="col col-3">
										<label class="input">
											<input type="text" name="faxno" id="faxno" placeholder="Fax Number" value="<?php echo $company_array['X_FXNO']; ?>" title="Fax no">
                                            <b class="tooltip tooltip-bottom-right">Needed to enter fax number</b> 
										</label>
									</section>
                                    <section class="col col-3">
										<label class="input">
											<input type="text" name="pan_no" id="pan_no" maxlength="10" placeholder="PAN Number" value="<?php echo $company_array['X_PANNO']; ?>" title="Enter PAN no">
                                            <b class="tooltip tooltip-bottom-right">Enter PAN number</b> 
										</label>
									</section>
									<section class="col col-3">
										<label class="input"> 
										<input type="text" name="gstin" id="gstin" placeholder="GSTN" value="<?php echo $company_array['X_GSTNNO']; ?>" title="Enter GSTN">
                                        <b class="tooltip tooltip-bottom-right">Enter GSTN number</b> 
                                        </label>
                                        
									</section>
                                    
                                    <section class="col col-3">
										<label class="input"> 
										<input type="text" name="website" id="website" placeholder="Web site" value="<?php echo $company_array['X_WEBSITE']; ?>" title="Enter web address">
                                        <b class="tooltip tooltip-bottom-right">Enter web address</b> 
                                        </label>
                                        
									</section>
                                </div>
                                </fieldset>
                               
                            
                                 <header>Contact Person Information</header>
                                <fieldset>

								<div class="row">
									<section class="col col-6">
										<label class="input">
											<input type="text" name="contact_person_name" placeholder="Contact person name" value="<?php echo $company_array['X_FRCONPERNM']; ?>" title="Enter contact person name">
                                            <b class="tooltip tooltip-bottom-right">Enter contact person name</b> 
										</label>
									</section>
                                    
                                    <section class="col col-3">
										<label class="input"><i class="icon-prepend fa fa-mobile"></i>
											<input type="text" name="contact_person_mobile_no" maxlength="10" placeholder="Mobile no" value="<?php echo $company_array['X_FRCONPERMOB']; ?>" title="Enter contact person mobile">
                                            <b class="tooltip tooltip-bottom-right">Enter mobile number</b> 
										</label>
									</section>
                                    
                                    
                                    <section class="col col-3">
										<label class="input"><i class="icon-prepend fa fa-phone"></i>
											<input type="text" name="contact_person_phone_no" maxlength="12" placeholder="Phone no" value="<?php echo$company_array['X_FRCONPERPH']; ?>" title="Enter phone no">
                                            <b class="tooltip tooltip-bottom-right">Enter phone number</b> 
										</label>
									</section>
                                    
								</div>
                                
                                 <div class="row">
                                    <section  class="col col-6">
                                        <label for="firm_address" class="input">
                                            <input type="text"  onkeypress="return isAddress(event)" name="contact_person_address" id="contact_person_address" placeholder="Address" value="<?php echo $company_array['X_FRCONPERADDR']; ?>" title="Enter contact person address">
                                            <b class="tooltip tooltip-bottom-right">Enter address of contact person</b> 
                                        </label>
                                    </section>
                                    
                                    <section class="col col-3">
										<label class="input">
											<input type="text" name="contact_person_city" placeholder="City name" value="<?php echo $company_array['X_FRCONPERCITY']; ?>" title="Enter contact person city">
                                            <b class="tooltip tooltip-bottom-right">Enter to contact person city</b> 
										</label>
									</section>
                                    
                                    <section class="col col-3">
									<label class="input">
									<input type="text" id="contact_person_pincode" name="contact_person_pincode" placeholder="Pin code" value="<?php echo $company_array['X_FRCONPERPIN']; ?>" title="Enter pin code">
                                    <b class="tooltip tooltip-bottom-right">Enter pin code</b> 
                                     </label>
									</section>	
                                
                                	
                                    
								</div>
                                
							</fieldset>
                            
                            
							<footer><?php $cid=base64_decode($_GET['id']);?>
								<input type="hidden" name="cid" value="<?php echo $cid; ?>"/>
                                <button type="submit" class="btn btn-primary" id="btncomapnyupdate">
									Update 
								</button>
                                <button type="reset" class="btn btn-primary">
									Reset
								</button>
							</footer>
						</form>

					</div>
					<!-- end widget content -->
					
				</div>
				<!-- end widget div -->
				
			</div>
			<!-- end widget -->
			
			
			

		</article>
		<!-- END COL -->

		

	</div>

	<!-- END ROW -->

</section>
<!-- end widget grid -->




			</div>
			<!-- END MAIN CONTENT -->

		</div>
		<!-- END MAIN PANEL -->

