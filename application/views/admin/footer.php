<!-- PAGE FOOTER -->
		<div class="page-footer">
			<div class="row">
				<div class="col-xs-12 col-sm-6">
					<span class="txt-color-white">Progemesis <span class="hidden-xs"> - Develop by Chandrakant Bhamare</span> @ <?php echo date("Y");?></span>
				</div>

				
			</div>
		</div>
		<!-- END PAGE FOOTER -->

		
		
		<!--================================================== -->
        <!-- Bar chart JS-->
		        <!-- bar chart js close -->
		 <script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
		<script data-pace-options='{ "restartOnRequestAfter": true }' src="<?php echo base_url(); ?>assets/global/js/plugin/pace/pace.min.js"></script>

	
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
		<script>
			if (!window.jQuery) {
				document.write('<script src="<?php echo base_url(); ?>assets/global/js/libs/jquery-2.1.1.min.js"><\/script>');
			}
		</script>

		<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
		<script>
			if (!window.jQuery.ui) {
				document.write('<script src="<?php echo base_url(); ?>assets/global/js/libs/jquery-ui-1.10.3.min.js"><\/script>');
			}
		</script>

		<!-- IMPORTANT: APP CONFIG -->
		<script src="<?php echo base_url(); ?>assets/global/js/app.config.js"></script>

		<!-- JS TOUCH : include this plugin for mobile drag / drop touch events-->
		<script src="<?php echo base_url(); ?>assets/global/js/plugin/jquery-touch/jquery.ui.touch-punch.min.js"></script> 

		<!-- BOOTSTRAP JS -->
		<script src="<?php echo base_url(); ?>assets/global/js/bootstrap/bootstrap.min.js"></script>

				<!-- EASY PIE CHARTS -->
		<script src="<?php echo base_url(); ?>assets/global/js/plugin/easy-pie-chart/jquery.easy-pie-chart.min.js"></script>

		<!-- SPARKLINES -->
		<script src="<?php echo base_url(); ?>assets/global/js/plugin/sparkline/jquery.sparkline.min.js"></script>

		<!-- JQUERY VALIDATE -->
		<script src="<?php echo base_url(); ?>assets/global/js/plugin/jquery-validate/jquery.validate.min.js"></script>

		<!-- JQUERY MASKED INPUT -->
		<script src="<?php echo base_url(); ?>assets/global/js/plugin/masked-input/jquery.maskedinput.min.js"></script>

		<!-- JQUERY SELECT2 INPUT -->
		<script src="<?php echo base_url(); ?>assets/global/js/plugin/select2/select2.min.js"></script>

		<!-- JQUERY UI + Bootstrap Slider -->
		<script src="<?php echo base_url(); ?>assets/global/js/plugin/bootstrap-slider/bootstrap-slider.min.js"></script>

		<!-- browser msie issue fix -->
		<script src="<?php echo base_url(); ?>assets/global/js/plugin/msie-fix/jquery.mb.browser.min.js"></script>

		<!-- FastClick: For mobile devices -->
		<script src="<?php echo base_url(); ?>assets/global/js/plugin/fastclick/fastclick.min.js"></script>

		
		<!-- Demo purpose only -->
		<script src="<?php echo base_url(); ?>assets/global/js/demo.min.js"></script>

		<!-- MAIN APP JS FILE -->
      	<script src="<?php echo base_url(); ?>assets/global/js/app.min.js"></script>

		
		   
		
		<script src="<?php echo base_url(); ?>assets/global/js/plugin/flot/jquery.flot.cust.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/global/js/plugin/flot/jquery.flot.fillbetween.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/global/js/plugin/flot/jquery.flot.pie.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/global/js/plugin/flot/jquery.flot.orderBar.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/global/js/plugin/flot/jquery.flot.resize.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/global/js/plugin/flot/jquery.flot.time.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/global/js/plugin/flot/jquery.flot.tooltip.min.js"></script>
        
        <!-- Flot Chart Plugin: Flot Engine, Flot Resizer, Flot Tooltip -->
		
		        
		
		<!-- Vector Maps Plugin: Vectormap engine, Vectormap language -->
		<script src="<?php echo base_url(); ?>assets/global/js/plugin/vectormap/jquery-jvectormap-1.2.2.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/global/js/plugin/vectormap/jquery-jvectormap-world-mill-en.js"></script>
		
		<!-- Full Calendar -->
		<script src="<?php echo base_url(); ?>assets/global/js/plugin/moment/moment.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/global/js/plugin/fullcalendar/jquery.fullcalendar.min.js"></script>
        
        <!-- PAGE RELATED PLUGIN(S) -->
		<script src="<?php echo base_url(); ?>assets/global/js/plugin/datatables/jquery.dataTables.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/global/js/plugin/datatables/dataTables.colVis.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/global/js/plugin/datatables/dataTables.tableTools.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/global/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/global/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>
        
        <script src="<?php echo base_url(); ?>assets/global/js/plugin/bootstrapvalidator/bootstrapValidator.min.js"></script>
        <!-- tree view js start -->
        
        <script src="<?php echo base_url(); ?>assets/global/js/hummingbird-treeview.js"></script>
         <!-- tree view js close -->
		<script src="<?php echo base_url(); ?>assets/global/js/jquery.ajaxfileupload.js"></script>
        
		<script src="<?php echo base_url(); ?>assets/global/js/custom_script_admin.js"></script>
      <?php
	   if(strtoupper($this->uri->segment(2)) == strtoupper('book'))
	   {
		   ?>
           <script src="<?php echo base_url(); ?>assets/global/js/custom_script_book.js"></script>
           <?php
	   }
	   
	   if(strtoupper($this->uri->segment(2)) == strtoupper('user'))
	   {
		   ?>
           <script src="<?php echo base_url(); ?>assets/global/js/custom_script_users.js"></script>
           <?php
	   }

	   
	  
	   if(strtoupper($this->uri->segment(2)) == strtoupper('rentbook'))
	   {
		   ?>
           <script src="<?php echo base_url(); ?>assets/global/js/custom_script_rentbook.js"></script>
           <?php
	   }
	   	    ?>

        
        <script src="<?php echo base_url(); ?>assets/global/js/jquery-1.11.1.min.js"></script>
        <script type="text/javascript">
		var jQuery_1_11_1_min = $.noConflict(true);
		</script> 
        
        
<!-- bootsrap select -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.7.5/js/bootstrap-select.min.js"></script>

	</body>

</html>