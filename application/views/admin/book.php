<!--
* Created By : Chandrakant Bhamare
* Created On : 2022-03-01
* Modified By : 
* Modified On : 
* Purpose : To Show All Project
-->
		<!-- MAIN PANEL -->
		<div id="main" role="main">

			<!-- RIBBON -->
			<div id="ribbon">

				<span class="ribbon-button-alignment"> 
					<span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
						<i class="fa fa-refresh"></i>
					</span> 
				</span>

				<!-- breadcrumb -->
				<ol class="breadcrumb">
					<li><a href="<?php echo base_url(); ?>admin/home">Home</a></li><li>Book List</li>
				</ol>
				<!-- end breadcrumb -->

			</div>
			<!-- END RIBBON -->

			<!-- MAIN CONTENT -->
			<div id="content">
				<!-- widget grid -->
				<section id="widget-grid" class="">
				
					<!-- row -->
					<div class="row">
				
						<!-- NEW WIDGET START -->
						<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				
							
							<!-- Widget ID (each widget will need unique ID)-->
							<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-1" data-widget-editbutton="false" data-widget-fullscreenbutton="false" data-widget-togglebutton="false" data-widget-deletebutton="false" data-widget-colorbutton="false">
								
								<header>
									<span class="widget-icon"> <i class="fa fa-product-hunt"></i> </span>
									<h2>Book</h2><?php //if($this->session->userdata('urole')==1){ ?><span style="float:right; padding-right:5%"><a href="<?php echo base_url();?>admin/book/bookadd" class="btn btn-sm btn-success "><i class="fa fa-plus"> </i> New Book</a></span><?php //}?>
				
								</header>
				
								<!-- widget div-->
								<div>
				
									<!-- widget edit box -->
									<div class="jarviswidget-editbox">
										<!-- This area used as dropdown edit box -->
				
									</div>
                                    
                                    
                                    
									<!-- end widget edit box -->
				
									<!-- widget content -->
									<div class="widget-body no-padding">
				
										<table id="dt_book_list" class="table table-striped table-bordered" width="100%">										<thead>			                
												<tr>
												    <th data-hide="phone">#</th>
                                                   <th data-hide="phone">Image</th>
                                                   <th data-hide="phone">Book Name</th>
                                                   <th data-hide="phone">Author</th>
                                                   <th data-hide="phone">Price</th>
                                                   <th>Actions</th>
												</tr>
											</thead>

				
									        <tbody>
									        </tbody>
									
										</table>
				
									</div>
									<!-- end widget content -->
				
								</div>
								<!-- end widget div -->
				
							</div>
							<!-- end widget -->
				
							
						</article>
						<!-- WIDGET END -->
				
					</div>
				
					<!-- end row -->
				
					<!-- end row -->
				
				</section>
				<!-- end widget grid -->
                
                <!-- Modal -->
				<div class="modal fade" id="DESModel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
                         <form role="form" id="update_project">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
									&times;
								</button>
								<h4 class="modal-title" id="myModalLabel">Update Project</h4>
							</div>
							<div class="modal-body">
				
								<div class="row">
                                	<div class="col-md-4">
                                   <div class="form-group">
                                   	<span style="vertical-align:central !important">Project Name<n style="color:red !important;">*</n></span>
                                            </div>
                                      </div>
									<div class="col-md-8">
                                   
                                  		<div class="form-group">
											<input type="text" name="project_name_update" id="project_name_update" class="form-control" placeholder="Project Name" style="text-transform:uppercase;" maxlength="100" required />
                                            <input type="hidden" name="opro_id_update" id="opro_id_update"  />
										</div>
                                     </div>
                                  </div>
                                  
                                  <div class="row">
                                	<div class="col-md-4">
                                   <div class="form-group">
                                   	<span style="vertical-align:central !important">Project Code<n style="color:red !important;">*</n></span>
                                            </div>
                                      </div>
									<div class="col-md-8">
                                        
                                        <div class="form-group">
											<input type="text" name="project_code_update" id="project_code_update" class="form-control" placeholder="Project Code" style="text-transform:uppercase;" maxlength="20" required readonly="readonly" />
                                        </div>
                                     </div>
                                  </div>
                                  <div class="row">
                                	<div class="col-md-4">
                                   <div class="form-group">
                                   	<span style="vertical-align:central !important">Client<n style="color:red !important;">*</n></span>
                                            </div>
                                      </div>
									<div class="col-md-8">
                                        
                                        <div class="form-group">
											
                                              <select class="form-control select2 required" id="client_id_update" name = "client_id_update">
                                                <option value="" >Select client*</option>
                                                <?php
                                                                        foreach($client_li as $client1)
                                                                        {
                                                                            
                                                                            echo '<option value="'.$client1['X_CID'].'">'.$client1['X_CNM'].'</option>';
                                                                        }
                                                                        ?>
                                              </select>
										</div>
									
									</div>
								</div>
				
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-default" data-dismiss="modal">
									Cancel
								</button>
								<button type="submit" class="btn btn-primary" >
									Update
								</button>
							</div>
                            
                            	</form>
                            
						</div><!-- /.modal-content -->
					</div><!-- /.modal-dialog -->
				</div><!-- /.modal -->

			</div>
			<!-- END MAIN CONTENT -->

		</div>
		<!-- END MAIN PANEL -->

		