<div id="main" role="main">

			<!-- RIBBON -->
			<div id="ribbon">

				<span class="ribbon-button-alignment"> 
					<span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
						<i class="fa fa-refresh"></i>
					</span> 
				</span>

				<!-- breadcrumb -->
				<ol class="breadcrumb">
					<li>Dashboard</li>
				</ol>
				
			</div>
			<!-- END RIBBON -->
			
			

			<!-- MAIN CONTENT -->
			<div id="content">
				             
				<div class="row">

	<div class="col-sm-6 col-md-12 col-lg-12">

		

		<div class="well well-sm well-light">
        <div class="row">
								
						        
						        		       
						        <div class="col-xs-12 col-sm-3 col-md-3">
						            <div class="panel panel-greenLight">
						                <div class="panel-heading">
						                    <h3 class="panel-title">NO OF BOOK</h3>
						                </div>
						                <div class="panel-body no-padding text-align-center">
						                    <div class="the-price">
						                        <h1><?php echo  $book_dash[0]->X_BOOKCNT;?></h1>
						                    </div>
						                </div>
						                <div class="panel-footer no-padding">
						                    <a href="<?php echo base_url(); ?>admin/book" class="btn bg-color-greenLight txt-color-white btn-block" role="button">Book List</a>
						                </div>
						            </div>
						        </div>		        
						                
						   </div>
                            
                            
			

			
		</div>
	</div>

	

</div>
			
			

			</div>
			<!-- END MAIN CONTENT -->

		</div>