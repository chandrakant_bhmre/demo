<!--
Created By : Chandrakant Bhamare
Creted On : 2022-03-02
Modified By : 
Modified On : 
Purpose : 
Other information : Update User Profile
-->

		<!-- MAIN PANEL -->
		<div id="main" role="main">

			<!-- RIBBON -->
			<div id="ribbon">

				<span class="ribbon-button-alignment"> 
					<span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
						<i class="fa fa-refresh"></i>
					</span> 
				</span>

				<!-- breadcrumb -->
				<ol class="breadcrumb">
					<li><a href="<?php echo base_url(); ?>admin/home">Home</a></li><li><a href="<?php echo base_url(); ?>admin/rentbook">Book List</a></li><li>Book Update</li>
				</ol>
				<!-- end breadcrumb -->

				<!-- You can also add more buttons to the
				ribbon for further usability

				Example below:

				<span class="ribbon-button-alignment pull-right">
				<span id="search" class="btn btn-ribbon hidden-xs" data-title="search"><i class="fa-grid"></i> Change Grid</span>
				<span id="add" class="btn btn-ribbon hidden-xs" data-title="add"><i class="fa-plus"></i> Add</span>
				<span id="search" class="btn btn-ribbon" data-title="search"><i class="fa-search"></i> <span class="hidden-mobile">Search</span></span>
				</span> -->

			</div>
			<!-- END RIBBON -->

<!-- MAIN CONTENT -->
			<div id="content">

<!--<div class="alert alert-block alert-success">
	<a class="close" data-dismiss="alert" href="#">×</a>
	<h4 class="alert-heading"><i class="fa fa-check-square-o"></i> Check validation!</h4>
	<p>
		You may also check the form validation by clicking on the form action button. Please try and see the results below!
	</p>
</div>-->

<!-- widget grid -->
<section id="widget-grid" class="">

<!-- ROW FOR MESSAGES -->
					<div class="row">
						<!-- NEW WIDGET START -->
						<article class="col-sm-12" id="alert_message">
						</article>
						<!-- WIDGET END -->
					</div>
<!-- ROW FOR MESSAGES END -->


	<!-- START ROW -->

	<div class="row">

		<!-- NEW COL START -->
		<article class="col-sm-12 col-md-12 col-lg-12">
			
			<!-- Widget ID (each widget will need unique ID)-->
			<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-1" data-widget-editbutton="false" data-widget-fullscreenbutton="false" data-widget-togglebutton="false" data-widget-deletebutton="false" data-widget-colorbutton="false">
				<!-- widget options:
					usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">
					
					data-widget-colorbutton="false"	
					data-widget-editbutton="false"
					data-widget-togglebutton="false"
					data-widget-deletebutton="false"
					data-widget-fullscreenbutton="false"
					data-widget-custombutton="false"
					data-widget-collapsed="true" 
					data-widget-sortable="false"
					
				-->
				<header>
					<span class="widget-icon"> <i class="fa fa-user"></i> </span>
					<h2>Book Update </h2>				
					
				</header>

				<!-- widget div-->
				<div>
					
					<!-- widget edit box -->
					<div class="jarviswidget-editbox">
						<!-- This area used as dropdown edit box -->
						
					</div>
					<!-- end widget edit box -->
					
					<!-- widget content -->
					<div class="widget-body no-padding">
                    
                                        
						<form id="book-edit-form" action="<?php echo base_url();?>admin/rentbook/bookedit_save" class="smart-form" method="post" novalidate>
                        
                        
                        
                        <header>Book Details</header>
							<br/>
                               
                                 <fieldset> 
                                <div class="row" >
                                    <section class="col col-2">
										<label class="input"> Book Name<span style="color:red">*</span>
										</label>
									</section>
                                    
                                    <section class="col col-4">
										<label class="input"> 
											<input type="text" name="book_name" id="book_name" placeholder="Book Name" value="<?php echo $BOOK_ARRAY->X_BNAME;?>" title="Book Name" maxlength="100">
                                            <b class="tooltip tooltip-bottom-right">Please enter book name</b> 
										</label>
									</section>
                                    
                                    <section class="col col-2">
										<label class="input">Author<span style="color:red">*</span>
										</label>
									</section>
									
                                    <section class="col col-4">
										<label class="input">
											<input type="text" name="author_name" id="author_name" placeholder="Author Code" value="<?php echo $BOOK_ARRAY->X_BAUTHOR; ?>" title="Author name" maxlength="50">
                                            <b class="tooltip tooltip-bottom-right">Needed to author name</b> 
										</label>
									</section>
                                    
                                                                                                  
                                
                                </div>
                              
                              
                                <div class="row">
                                	<section class="col col-2">
										<label class="input"> Book Price <span style="color:red">*</span>
										</label>
									</section>
									
                                    <section class="col col-4">
										<label class="input">
											<input type="number" id="book_amount" name="book_amount" placeholder="Amount in Rs" value="<?php echo $BOOK_ARRAY->X_BPRICE; ?>" title="Amount in Rs">
                                            <b class="tooltip tooltip-bottom-right">Needed to enter book amount</b> 
										</label>
									</section>
                                 </div>
                                
                           </fieldset>
                            
                           
                            <footer>
                              <input type="hidden" name="bookid" id="bookid" value="<?php echo $_GET['id'];?>" />
                                <button type="submit" class="btn btn-primary" id="btnbookedit">
									Update 
								</button>
                                <button type="reset" class="btn btn-primary">
									Reset
								</button>
							</footer>
						</form>

					</div>
					<!-- end widget content -->
					
				</div>
				<!-- end widget div -->
				
			</div>
			<!-- end widget -->
			
			
			

		</article>
		<!-- END COL -->

		

	</div>

	<!-- END ROW -->

</section>
<!-- end widget grid -->




			</div>
			<!-- END MAIN CONTENT -->

		</div>
		<!-- END MAIN PANEL -->

