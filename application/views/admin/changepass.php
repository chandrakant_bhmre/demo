<!--
Created By : Chandrakant
Creted On : 2022-03-01
Modified By : 
Modified On : 
Purpose : Change Password
Other information : 
-->

		<!-- MAIN PANEL -->
		<div id="main" role="main">

			<!-- RIBBON -->
			<div id="ribbon">

				<span class="ribbon-button-alignment"> 
					<span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
						<i class="fa fa-refresh"></i>
					</span> 
				</span>

				<!-- breadcrumb -->
				<ol class="breadcrumb">
					<li><a href="<?php echo base_url(); ?>admin/home">Home</a></li><li>Change Password</li>
				</ol>
				<!-- end breadcrumb -->

				<!-- You can also add more buttons to the
				ribbon for further usability

				Example below:

				<span class="ribbon-button-alignment pull-right">
				<span id="search" class="btn btn-ribbon hidden-xs" data-title="search"><i class="fa-grid"></i> Change Grid</span>
				<span id="add" class="btn btn-ribbon hidden-xs" data-title="add"><i class="fa-plus"></i> Add</span>
				<span id="search" class="btn btn-ribbon" data-title="search"><i class="fa-search"></i> <span class="hidden-mobile">Search</span></span>
				</span> -->

			</div>
			<!-- END RIBBON -->

<!-- MAIN CONTENT -->
			<div id="content">

<!--<div class="alert alert-block alert-success">
	<a class="close" data-dismiss="alert" href="#">×</a>
	<h4 class="alert-heading"><i class="fa fa-check-square-o"></i> Check validation!</h4>
	<p>
		You may also check the form validation by clicking on the form action button. Please try and see the results below!
	</p>
</div>-->

<!-- widget grid -->
<section id="widget-grid" class="">


	<!-- START ROW -->

	<div class="row">

		<!-- NEW COL START -->
		<article class="col-sm-12 col-md-12 col-lg-12">
			
			<!-- Widget ID (each widget will need unique ID)-->
			<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-1" data-widget-editbutton="false" data-widget-fullscreenbutton="false" data-widget-togglebutton="false" data-widget-deletebutton="false" data-widget-colorbutton="false">
				<!-- widget options:
					usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">
					
					data-widget-colorbutton="false"	
					data-widget-editbutton="false"
					data-widget-togglebutton="false"
					data-widget-deletebutton="false"
					data-widget-fullscreenbutton="false"
					data-widget-custombutton="false"
					data-widget-collapsed="true" 
					data-widget-sortable="false"
					
				-->
				<header>
					<span class="widget-icon"> <i class="fa fa-key"></i> </span>
					<h2>Change Password</h2>				
					
				</header>

				<!-- widget div-->
				<div>
					
					<!-- widget edit box -->
					<div class="jarviswidget-editbox">
						<!-- This area used as dropdown edit box -->
						
					</div>
					<!-- end widget edit box -->
					
					<!-- widget content -->
					<div class="widget-body no-padding">
						
						<form id="admin-changepass" action="<?php echo base_url();?>admin/home/changepass_save" class="smart-form" method="post" novalidate ><br/><br/>
							<fieldset>
								<div class="row">
                                	<section class="col col-2">
	                                <label class="label">Old Password</label>
    	                            </section>
									<section class="col col-3">
										<label class="input"><i class="icon-prepend fa fa-lock"></i>
											<input type="password" name="old_password" id="old_password"/>
                                            </label>
									</section>
                                    </div>
                                
                                
                                    <div class="row">
                                    <section class="col col-2">
	                                <label class="label">New Password</label>
    	                            </section>
                                    <section class="col col-3">
										<label class="input"><i class="icon-prepend fa fa-lock"></i>
											<input type="password" name="new_password" id="new_password" min="5" maxlength="20"/>
                                            </label>
									</section>
                                    </div>
                                    
                                   
                                    
                                    <div class="row">
                                    <section class="col col-2">
	                                <label class="label">Confirm Password</label>
    	                            </section>
									<section class="col col-3">
										<label class="input"><i class="icon-prepend fa fa-lock"></i>
											<input type="password" name="confirm_password" id="confirm_password" min="5" maxlength="20">
										</label>
									</section>
                                    
                                    <section class="col col-3">
                                    
                                    </section>
                                    
								</div>
                                
                               </fieldset>
                                
                              
							<footer>
								<button type="submit" class="btn btn-primary" id="btnpassadd">
									Change Password 
								</button>
                                <button type="reset" class="btn btn-primary">
									Reset
								</button>
							</footer>
                           
						</form>

					</div>
					<!-- end widget content -->
					
				</div>
				<!-- end widget div -->
				
			</div>
			<!-- end widget -->
			
			
			

		</article>
		<!-- END COL -->

		

	</div>

	<!-- END ROW -->

</section>
<!-- end widget grid -->


<!-- Modal -->
				<!--<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static" data-keyboard="false" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
									&times;
								</button>
								<h4 class="modal-title" id="myModalLabel">OTP</h4>
							</div>
							<div class="modal-body">
				
								<div class="row">
									<div class="col-md-12">
										<div class="form-group">
											<input type="text" name="enter_otp" id="enter_otp" class="form-control" placeholder="Enter OTP" maxlength="6" required />
										</div>
									</div>
								</div>
								
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-default" data-dismiss="modal">
									Cancel
								</button>
                                <button type="button" class="btn btn-primary" name="resend" id="resend" onclick="sendOTP()">
									Resend
								</button>
								<button type="button" class="btn btn-primary" name="validate_otp" id="validate_otp" onclick="validateOTP('btnepincreateadd','Create',function () { epinCreationAfterValidate() });">
									Validate
								</button>
							</div>
						</div><!-- /.modal-content --
					</div><!-- /.modal-dialog --
				</div><!-- /.modal -->
				



			</div>
			<!-- END MAIN CONTENT -->

		</div>
		<!-- END MAIN PANEL -->

