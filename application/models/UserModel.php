<?php
/*Created By : Chandrakant Bhamare
Purpose : User  
creation Date : 2022-03-01
Modified By : 
Modified Date : 
Other Information :
*/
defined('BASEPATH') OR exit('No direct script access allowed');

class UserModel extends CI_Model 
{
	/* get user list */
	function getUserList()
	{
		$SESS_ARRAY= $this->session->all_userdata();
		
		// Get Parameters
		$param_array     = array(
				'P_UID' => $SESS_ARRAY['user_id'],
				'P_TOKEN' => $SESS_ARRAY['token_id']
			);
			
		$webservice_path = WEBSERVICE_PATH . "common/pro_get_user_list.php";
		$web_response = $this->utility_model->callback_webservice_token($webservice_path,$param_array);
		//print_r($web_response); exit;
		if($web_response['http_code'] == 200)
			{

				$WEB_RESPONSE = json_decode($web_response['response']);

				$status=$WEB_RESPONSE->X_STS;
			if($status == 1)
			{
			// Final aray
			$FINAL_ARRAY= $WEB_RESPONSE->X_USER_LIST;
			$json='';
			$json = $json . "{";
			$json = $json . "\"data\":";
			$json = $json . "[";
			
			for($j=0;$j<count($FINAL_ARRAY);$j++)
				{
				
										
					$AUTOID=$j+1;
					
					$full_name=$FINAL_ARRAY[$j]->X_FNAME." ".$FINAL_ARRAY[$j]->X_LNAME;
					$json = $json . "[";
					$json = $json . "\"".$AUTOID."\",";
					$json = $json . "\"".$full_name."\",";
					$json = $json . "\"".$FINAL_ARRAY[$j]->X_EMAIL."\",";
					$json = $json . "\"".$FINAL_ARRAY[$j]->X_MNO."\",";
					$json = $json . "\"".$FINAL_ARRAY[$j]->X_AGE."\",";
					$json = $json . "\"".$FINAL_ARRAY[$j]->X_GENDER."\",";
					$json = $json . "\"".$FINAL_ARRAY[$j]->X_CITY."\"";
					$json = $json . "]";
				if(count($FINAL_ARRAY)==($j+1))
					{
						$json = $json ."";
					}
					else
					{
						$json = $json .",";
					}
				}
				$json = $json . "]";
				$json = $json . "}";
			}
			
			echo $json; // data list return response
			}
	}
	
	/***************User LIST END ******************/
	
	
	function getUserDetails($uid)
	{
		$SESS_ARRAY= $this->session->all_userdata();
		// Get Parameters
			$param_array     = array(
				'P_UID' => $SESS_ARRAY['user_id'],
				'P_TOKEN' => $SESS_ARRAY['token_id'],
				'P_MID' => trim($uid)
			);
		// Web service path
		$webservice_path = WEBSERVICE_PATH . "common/pro_get_user_profile.php";
		// get web service calling function and return complite array
		$web_response = $this->utility_model->callback_webservice_token($webservice_path,$param_array);
		
		//print_r($web_response);exit;
		
		// web service return status
		if($web_response['http_code'] == 200)
			{
				// web service response 
				return $WEB_RESPONSE = json_decode($web_response['response']);
			}
	}
	
	
	/**********UPDATE Profile *****************************/
	function updateProfile($update_data)
	{
			$SESS_ARRAY= $this->session->all_userdata();
			
			// Get Parameters
				$param_array     = array(
				'P_UID' => $SESS_ARRAY['user_id'],
				'P_TOKEN' => $SESS_ARRAY['token_id'],
				'P_FNM' => trim($update_data['first_name']),
				'P_LNM' => trim($update_data['last_name']),
				'P_EMIAL' => trim($update_data['email']),
				'P_CITY' => trim($update_data['city']),
				'P_AGE' => trim($update_data['age']),
				'P_GENDER' => trim($update_data['gender']),
				'P_MID' =>trim(base64_decode($update_data['mid']))
				);
		//print_r($param_array);exit;
		$webservice_path = WEBSERVICE_PATH . "common/pro_update_user_profile.php";
		$web_response = $this->utility_model->callback_webservice_token($webservice_path,$param_array);
		
		//echo '<pre>';print_r($web_response);echo '</pre>';		exit;					
		if($web_response['http_code'] == 200)
			{
				$resp = $web_response['response'];
				return $resp;
			}
			else
			{
				$resp = $web_response['response'];
				return $resp;
			}
	}//function end
	/************update  Profile END *********************/

	
	
}
?>
