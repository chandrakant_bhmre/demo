<?php
/*Created By : Chandrakant Bhamare
Purpose : All Section common functions
creation Date : 2022-03-02
Modified By : 
Modified Date : 
Other Information :*/
defined('BASEPATH') OR exit('No direct script access allowed');

class CommonModel extends CI_Model 
{
	// Get user dropdown
	function getUserDropdown()
		{
			$SESS_ARRAY= $this->session->all_userdata();
		
		$param_array     = array(
				'P_UID' => $SESS_ARRAY['user_id'],
				'P_TOKEN' => $SESS_ARRAY['token_id']
			);
		
		$webservice_path = WEBSERVICE_PATH . "common/pro_get_user_dropdown.php";
		$web_response = $this->utility_model->callback_webservice_token($webservice_path,$param_array);	
		
		// web service return status
		if($web_response['http_code'] == 200)
			{
				// web service response 
				$WEB_RESPONSE = json_decode($web_response['response']);
				// Web service return ststus response
				$status=$WEB_RESPONSE->X_STS;
			if($status == 1)
			{
			$FINAL_ARRAY= $WEB_RESPONSE->X_USER_LIST;
			
			$USERARR=array();
			
			//To show only active citis
			$cntr=0;
			for($cnt=0;$cnt<count($FINAL_ARRAY);$cnt++)
			{
				if($FINAL_ARRAY[$cnt]->X_PSTAT=='y')
				{
					$cntr++;
					array_push($USERARR,$FINAL_ARRAY[$cnt]);
				}
			}//for
			
			$json='';
			$json = $json . "[";
			
				for($j=0;$j<count($USERARR);$j++)
				{
					$json = $json . "{";
					$json = $json . "\"X_ID\" : \"".$USERARR[$j]->X_UID."\",";
					$json = $json . "\"X_NAME\" : \"".$USERARR[$j]->X_UNAME."\"";
					$json = $json . "}";
					if(count($USERARR)==($j+1))
					{
						$json = $json ."";
					}
					else
					{
						$json = $json .",";
					}
				}
				$json = $json . "]";
			}
			
			return  $json; // data list return response
			}
		}
	// Get book dropdown	
	function getBookDropdown()
		{
			$SESS_ARRAY= $this->session->all_userdata();
		
		$param_array     = array(
				'P_UID' => $SESS_ARRAY['user_id'],
				'P_TOKEN' => $SESS_ARRAY['token_id']
			);
		
		$webservice_path = WEBSERVICE_PATH . "common/pro_get_book_dropdown.php";
		$web_response = $this->utility_model->callback_webservice_token($webservice_path,$param_array);	
		
		// web service return status
		if($web_response['http_code'] == 200)
			{
				// web service response 
				$WEB_RESPONSE = json_decode($web_response['response']);
				// Web service return ststus response
				$status=$WEB_RESPONSE->X_STS;
			if($status == 1)
			{
			$FINAL_ARRAY= $WEB_RESPONSE->X_BOOK_LIST;
			
			$USERARR=array();
			
			//To show only active citis
			$cntr=0;
			for($cnt=0;$cnt<count($FINAL_ARRAY);$cnt++)
			{
				if($FINAL_ARRAY[$cnt]->X_PSTAT=='y')
				{
					$cntr++;
					array_push($USERARR,$FINAL_ARRAY[$cnt]);
				}
			}//for
			
			$json='';
			$json = $json . "[";
			
				for($j=0;$j<count($USERARR);$j++)
				{
					$json = $json . "{";
					$json = $json . "\"X_ID\" : \"".$USERARR[$j]->X_BID."\",";
					$json = $json . "\"X_NAME\" : \"".$USERARR[$j]->X_BNAME."\"";
					$json = $json . "}";
					if(count($USERARR)==($j+1))
					{
						$json = $json ."";
					}
					else
					{
						$json = $json .",";
					}
				}
				$json = $json . "]";
			}
			
			return  $json; // data list return response
			}
		}
	

}
?>
