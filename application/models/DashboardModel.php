<?php
/*Created By : CHandrakant Bhamare
Purpose : Dashboard Model
creation Date : 2019-03-02
Modified By : 
Modified Date : 
Other Information :
*/
defined('BASEPATH') OR exit('No direct script access allowed');

class DashboardModel extends CI_Model 
{
	// get book count
	function getbook_dashboard()
	{
		$SESS_ARRAY= $this->session->all_userdata();
		
		// Get Parameters
		$param_array     = array(
				'P_UID' => $SESS_ARRAY['user_id'],
				'P_TOKEN' => $SESS_ARRAY['token_id']
			);
			
		$webservice_path = WEBSERVICE_PATH . "common/pro_get_book_count.php";
		$web_response = $this->utility_model->callback_webservice_token($webservice_path,$param_array);
		
		if($web_response['http_code'] == 200)
			{

				$WEB_RESPONSE = json_decode($web_response['response']);

				$status=$WEB_RESPONSE->X_STS;
			if($status == 1)
			{
				return $WEB_RESPONSE->X_BOOK_LIST;
				}
			
			
			}
	}
	
	
	// admin dashboard function
	function getmarketing()
	{
		$SESS_ARRAY= $this->session->all_userdata();
		
		// Get Parameters
		$param_array     = array(
				'P_UID' => $SESS_ARRAY['user_id'],
				'P_TOKEN' => $SESS_ARRAY['token_id']
			);
			
		$webservice_path = WEBSERVICE_PATH . "common/skd_get_admin_dashboard_marketing.php";
		$web_response = $this->utility_model->callback_webservice_token($webservice_path,$param_array);
		
		if($web_response['http_code'] == 200)
			{

				$WEB_RESPONSE = json_decode($web_response['response']);

				$status=$WEB_RESPONSE->X_STS;
			if($status == 1)
			{
				return $WEB_RESPONSE->X_REGION_LIST;
				}
			
			
			}
	}
	
	// marketing dashboard function
	
	
	function getApplcation()
	{
		$SESS_ARRAY= $this->session->all_userdata();
		
		// Get Parameters
		$param_array     = array(
				'P_UID' => $SESS_ARRAY['user_id'],
				'P_TOKEN' => $SESS_ARRAY['token_id']
			);
			
		$webservice_path = WEBSERVICE_PATH . "common/skd_get_admin_dashboard_application.php";
		$web_response = $this->utility_model->callback_webservice_token($webservice_path,$param_array);
		
		if($web_response['http_code'] == 200)
			{

				$WEB_RESPONSE = json_decode($web_response['response']);

				$status=$WEB_RESPONSE->X_STS;
			if($status == 1)
			{
				return $WEB_RESPONSE->X_USER_LIST;
				}
			
			
			}
	}
	
	
	// Applcation dashboard function
	function getapplication_dashboard($DEPT_ID)
	{
		$SESS_ARRAY= $this->session->all_userdata();
		
		// Get Parameters
		$param_array     = array(
				'P_UID' => $SESS_ARRAY['user_id'],
				'P_TOKEN' => $SESS_ARRAY['token_id'],
				'P_DEPTID' => $DEPT_ID
			);
			
		$webservice_path = WEBSERVICE_PATH . "common/skd_get_applcation_dashboard.php";
		$web_response = $this->utility_model->callback_webservice_token($webservice_path,$param_array);
		//print_r($web_response);exit;
		if($web_response['http_code'] == 200)
			{

				$WEB_RESPONSE = json_decode($web_response['response']);

				$status=$WEB_RESPONSE->X_STS;
			if($status == 1)
			{
				return $WEB_RESPONSE->X_APPLICATION_LIST;
				}
			
			
			}
	}
	
	function getProccessing()
	{
		$SESS_ARRAY= $this->session->all_userdata();
		
		// Get Parameters
		$param_array     = array(
				'P_UID' => $SESS_ARRAY['user_id'],
				'P_TOKEN' => $SESS_ARRAY['token_id']
			);
			
		$webservice_path = WEBSERVICE_PATH . "common/skd_get_admin_dashboard_processing.php";
		$web_response = $this->utility_model->callback_webservice_token($webservice_path,$param_array);
		//print_r($web_response);exit;
		if($web_response['http_code'] == 200)
			{

				$WEB_RESPONSE = json_decode($web_response['response']);

				$status=$WEB_RESPONSE->X_STS;
			if($status == 1)
			{
				return $WEB_RESPONSE->X_USER_LIST;
				}
			
			
			}
	}
	// report 
	
	// userwise marketing Bar chart report
	function getmarketing_barchart()
	{
		$SESS_ARRAY= $this->session->all_userdata();
		
		// Get Parameters
		$param_array     = array(
				'P_UID' => $SESS_ARRAY['user_id'],
				'P_TOKEN' => $SESS_ARRAY['token_id']
			);
			
		$webservice_path = WEBSERVICE_PATH . "common/skd_get_admin_barchart_marketing_region.php";
		$web_response = $this->utility_model->callback_webservice_token($webservice_path,$param_array);
		//print_r($web_response);exit;
		if($web_response['http_code'] == 200)
			{
				$WEB_RESPONSE = json_decode($web_response['response']);
				$status=$WEB_RESPONSE->X_STS;
				if($status == 1)
				{
				return $WEB_RESPONSE->X_USER_LIST;
				}
			}
	}
	
	function getmarketing_barchartNew()
	{
		$SESS_ARRAY= $this->session->all_userdata();
		
		// Get Parameters
		$param_array     = array(
				'P_UID' => $SESS_ARRAY['user_id'],
				'P_TOKEN' => $SESS_ARRAY['token_id']
			);
			
		$webservice_path = WEBSERVICE_PATH . "common/skd_get_admin_barchart_marketing_region_new.php";
		$web_response = $this->utility_model->callback_webservice_token($webservice_path,$param_array);
		//print_r($web_response);exit;
		if($web_response['http_code'] == 200)
			{
				$WEB_RESPONSE = json_decode($web_response['response']);
				$status=$WEB_RESPONSE->X_STS;
				if($status == 1)
				{
				return $WEB_RESPONSE->X_USER_LIST;
				}
			}
	}
	
	function getmarketing_paichart()
	{
		$SESS_ARRAY= $this->session->all_userdata();
		
		// Get Parameters
		$param_array     = array(
				'P_UID' => $SESS_ARRAY['user_id'],
				'P_TOKEN' => $SESS_ARRAY['token_id']
			);
			
		$webservice_path = WEBSERVICE_PATH . "common/skd_get_admin_paichart.php";
		$web_response = $this->utility_model->callback_webservice_token($webservice_path,$param_array);
		//print_r($web_response);	exit;
		if($web_response['http_code'] == 200)
			{
				$WEB_RESPONSE = json_decode($web_response['response']);
				$status=$WEB_RESPONSE->X_STS;
				if($status == 1)
				{
				return $WEB_RESPONSE;
				}
			}
	}
	
	
	function getprocessing_barchart()
	{
		$SESS_ARRAY= $this->session->all_userdata();
		
		// Get Parameters
		$param_array     = array(
				'P_UID' => $SESS_ARRAY['user_id'],
				'P_TOKEN' => $SESS_ARRAY['token_id']
			);
			
		$webservice_path = WEBSERVICE_PATH . "common/skd_get_admin_barchart.php";
		$web_response = $this->utility_model->callback_webservice_token($webservice_path,$param_array);
		//print_r($web_response);exit;
		if($web_response['http_code'] == 200)
			{
				$WEB_RESPONSE = json_decode($web_response['response']);
				$status=$WEB_RESPONSE->X_STS;
				if($status == 1)
				{
				return $WEB_RESPONSE;
				}
			}
	}
	
	
	function getpaichart_processing()
	{
		$SESS_ARRAY= $this->session->all_userdata();
		
		// Get Parameters
		$param_array     = array(
				'P_UID' => $SESS_ARRAY['user_id'],
				'P_TOKEN' => $SESS_ARRAY['token_id']
			);
			
		$webservice_path = WEBSERVICE_PATH . "common/skd_get_admin_paichart_processing.php";
		$web_response = $this->utility_model->callback_webservice_token($webservice_path,$param_array);
		//print_r($web_response);exit;
		if($web_response['http_code'] == 200)
			{
				$WEB_RESPONSE = json_decode($web_response['response']);
				$status=$WEB_RESPONSE->X_STS;
				if($status == 1)
				{
				return $WEB_RESPONSE;
				}
			}
	}
	
	
	// Claim Report 
	
	function getUesrReportClaimData()
	{
		
		$SESS_ARRAY= $this->session->all_userdata();
		
		
		
				$param_array     = array(
				'P_UID' => $SESS_ARRAY['user_id'],
				'P_TOKEN' => $SESS_ARRAY['token_id'],
				'P_RTYPE' => "SU",
				'P_MUID'=> "ALL"
			);
		
		
		 $webservice_path = WEBSERVICE_PATH . "claim/skd_get_claim_user_summary.php";
		
		$web_response = $this->utility_model->callback_webservice_token($webservice_path,$param_array);
		/*echo '<pre>';print_r($web_response);echo '</pre>';exit;
		if($web_response['http_code'] == 200)
		{
			return $web_response['response'];
		}
		else
		{
			return $web_response['response'];
		}*/
		
		if($web_response['http_code'] == 200)
			{
				$WEB_RESPONSE = json_decode($web_response['response']);
				$status=$WEB_RESPONSE->X_STS;
				if($status == 1)
				{
				return $WEB_RESPONSE;
				}
			}
	
	}
	
	
	function getpaichart_claiming()
	{
		$SESS_ARRAY= $this->session->all_userdata();
		
		// Get Parameters
		$param_array     = array(
				'P_UID' => $SESS_ARRAY['user_id'],
				'P_TOKEN' => $SESS_ARRAY['token_id']
			);
			
		$webservice_path = WEBSERVICE_PATH . "common/skd_get_admin_paichart_claiming.php";
		$web_response = $this->utility_model->callback_webservice_token($webservice_path,$param_array);
		//print_r($web_response);exit;
		if($web_response['http_code'] == 200)
			{
				$WEB_RESPONSE = json_decode($web_response['response']);
				$status=$WEB_RESPONSE->X_STS;
				if($status == 1)
				{
				return $WEB_RESPONSE;
				}
			}
	}
	
	
	// Processing bar chart function
	function getprocessingstatus_barchart()
	{
		$SESS_ARRAY= $this->session->all_userdata();
		
		// Get Parameters
		$param_array     = array(
				'P_UID' => $SESS_ARRAY['user_id'],
				'P_TOKEN' => $SESS_ARRAY['token_id']
			);
			
		$webservice_path = WEBSERVICE_PATH . "common/skd_get_admin_processing_barchart.php";
		$web_response = $this->utility_model->callback_webservice_token($webservice_path,$param_array);
		//print_r($web_response);exit;
		if($web_response['http_code'] == 200)
			{
				$WEB_RESPONSE = json_decode($web_response['response']);
				$status=$WEB_RESPONSE->X_STS;
				if($status == 1)
				{
				return $WEB_RESPONSE;
				}
			}
	}
		
		
	
}
?>
