<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class DB_operationmodel extends CI_Model 
{
		/*
		* Fetching data from table
		*/
        function get_data($tablename,$where='',$order='',$limit='',$start='')
		{
			$sql = $this->db->query('SELECT * FROM '.$tablename.' '.$where.' '.$order.' '.$limit.' '.$start);
			return $sql->result();
		}
		/*
		*Get total row count
		*/
		function getRows($params = array())
		{
		if(array_key_exists('select_fields',$params))
		{
			$this->db->select($params['select_fields']);
		}	
		else
		{
			$this->db->select('*');
		}	
			
        
        $this->db->from($params['tablename']);
        $this->db->order_by($params['order_by'],$params['order']);
        
        if(array_key_exists("start",$params) && array_key_exists("limit",$params))
		{
            $this->db->limit($params['limit'],$params['start']);
        }
		else if(!array_key_exists("start",$params) && array_key_exists("limit",$params))
		{
            $this->db->limit($params['limit']);
        }
        
        $query = $this->db->get();
		
		       
        return ($query->num_rows() > 0)?$query->result():FALSE;
		}
		
		/*
		* Insert data into table
		*/
		function insert_data($tablename,$fld=array(),$values=array())
		{
			$fields=implode(',',$fld);
			$insert_val=implode(',',$values);
						
			$insert_result=$this->db->query("call MLM_insert('".$tablename."','".$fields."',\"(".$insert_val.")\",@result)");
			
			$get_insert_result = "SELECT @result as result;";
			$outValue = $this->db->query($get_insert_result);
			
			return $outValue->row()->result;
			
		}
		
		
		/*
		* Update data in table
		*/
		
		function update_data($tablename,$set_condition=array(),$where='')
		{
			$update_data='';
			foreach($set_condition as $key=>$value)
			{
				if(trim($update_data)=='')
				{
					$update_data=$key."='".$value."'";
				}
				else
				{
					$update_data.=",".$key."='".$value."'";
				}
			}
			
			//echo $update_data;
			
			$procedure_sql='call MLM_update(?,?,?,@result)';
			
			$update_result=$this->db->query($procedure_sql,array($tablename,$update_data,$where));
			
			$get_update_result = "SELECT @result as result;";
			$outValue = $this->db->query($get_update_result);
			
			return $outValue->row()->result;
		}
		
		/*
		* Delete data in table
		*/
		
		function delete_data($tablename,$where_condition='')
		{
			$procedure_sql='call MLM_delete(?,?,@result)';
			
			$delete_result=$this->db->query($procedure_sql,array($tablename,$where_condition));
			
			$get_delete_result = "SELECT @result as result;";
			$outValue = $this->db->query($get_delete_result);
			
			return $outValue->row()->result;
		}
		
		
}
?>
