<?php
/*Created By : Chandrakant Bhamare
creation Date : 2022-03-01
Modified By : 
Modified Date : 
Other Information : Bo0ok Rent List
*/
defined('BASEPATH') OR exit('No direct script access allowed');

class RentbookModel extends CI_Model 
{
	/******************GET LIST OF ALL issue books***/
	function getRentBookList()
	{
		
		$SESS_ARRAY= $this->session->all_userdata();
		
		$param_array     = array(
				'P_UID' => $SESS_ARRAY['user_id'],
				'P_TOKEN' => $SESS_ARRAY['token_id']
			);
		
		$webservice_path = WEBSERVICE_PATH . "common/pro_get_rent_book_list.php";
		
		$web_response = $this->utility_model->callback_webservice_token($webservice_path,$param_array);
		
//echo '<pre>';print_r($web_response);echo '</pre>'; exit;
		
		if($web_response['http_code'] == 200)
			{
				
				$WEB_RESPONSE = json_decode($web_response['response']);
				$status=$WEB_RESPONSE->XSTS;
			if($status == 1)
			{
			// Final aray
			$FINAL_ARRAY= $WEB_RESPONSE->X_BOOK_LIST;
			$json='';
			$json = $json . "{";
			$json = $json . "\"data\":";
			$json = $json . "[";
			
			for($j=0;$j<count($FINAL_ARRAY);$j++)
				{
					if($FINAL_ARRAY[$j]->X_ACT=="y"){$status="eye";$status_class='btn-success';$editClass='';$statustitle='Active';}
					else if($FINAL_ARRAY[$j]->X_ACT=="n"){$status="eye-slash";$status_class='btn-warning';$editClass='disabled';$statustitle='Inactive';}
					
										
					$INC=$j+1;
					$json = $json . "[";
					$json = $json . "\"".$INC."\",";
					$json = $json . "\"".$FINAL_ARRAY[$j]->X_USRNM."\",";
					$json = $json . "\"".$FINAL_ARRAY[$j]->X_BNM."\",";
					$json = $json . "\"".$FINAL_ARRAY[$j]->X_RETURNDT."\",";
					$json = $json . "\"".$FINAL_ARRAY[$j]->X_RENT."\",";
					
					
					
					
				/*	if($this->session->userdata('urole')==1){
						
						$json = $json .",";}
						else
						{
							$json = $json .",";
						}*/
					
				
					$json = $json . "\"<a href='".base_url()."admin/rentbook/rentbookedit?id=".base64_encode($FINAL_ARRAY[$j]->X_BID)."' class='btn btn-sm btn-primary ".$editClass."' title='Return'><i class='fa fa-link'> Return</i></a>\"";
						
						/*$json = $json. "&nbsp;<a id='user_stat_".$FINAL_ARRAY[$j]->X_ACT."' class='btn ".$status_class."' onclick=change_all_status('".$FINAL_ARRAY[$j]->X_ACT."','user_stat','".$FINAL_ARRAY[$j]->X_BID."','ORG_PROJECT') href='javascript:void(0);' title='".$statustitle."'><i class='fa fa-".$status."'></i></a>";
						$json = $json. "&nbsp;<button type='button' title='Delete' class='btn btn-danger'  onclick=project_delete('".$FINAL_ARRAY[$j]->X_BID."') ".$ACCSS_HIDE."><i class='fa fa-trash'></button></button>\"";*/
					
					$json = $json . "]";
					if(count($FINAL_ARRAY)==($j+1))
					{
						$json = $json ."";
					}
					else
					{
						$json = $json .",";
					}
				}
				$json = $json . "]";
				$json = $json . "}";
			}
			
			echo $json; // data list return response
			}
	}
	
	
	/**********CREATE issue Book*****************************/
	function createIssueBook($insert_data)
	{
			$SESS_ARRAY= $this->session->all_userdata();
			
			$param_array     = array(
				'P_UID' => $SESS_ARRAY['user_id'],
				'P_TOKEN' => $SESS_ARRAY['token_id'],
				'P_MID' => trim($insert_data['u_id']),
				'P_BID' => trim($insert_data['book_id']),
				'P_BRENT' => trim($insert_data['book_rent']),
				'P_IDATE' => trim($insert_data['issue_date'])
				
			);
		$webservice_path = WEBSERVICE_PATH . "common/pro_issue_book.php";
		$web_response = $this->utility_model->callback_webservice_token($webservice_path,$param_array);
				
	//echo '<pre>';print_r($web_response);echo '</pre>'; exit;							
		if($web_response['http_code'] == 200)
			{
				$resp = $web_response['response'];
				return $resp;
			}
			else
			{
				$resp = $web_response['response'];
				return $resp;
			}
	}//function end
	/************CREATE book END *********************/
	
	/* get Book Detaikls */
	function getIssueBookDetails($bookid)
	{
		$SESS_ARRAY= $this->session->all_userdata();
		// Get Parameters
			$param_array     = array(
				'P_UID' => $SESS_ARRAY['user_id'],
				'P_TOKEN' => $SESS_ARRAY['token_id'],
				'P_ISSUEID' => trim($bookid)
			);
		// Web service path
		$webservice_path = WEBSERVICE_PATH . "common/pro_get_issue_details.php";
		// get web service calling function and return complite array
		$web_response = $this->utility_model->callback_webservice_token($webservice_path,$param_array);
		
		//print_r($web_response);exit;
		
		// web service return status	
		if($web_response['http_code'] == 200)
			{
				// web service response 
				return $WEB_RESPONSE = json_decode($web_response['response']);
			}
	}
	
	
	
	/**********UPDATE Book *****************************/
	function updateBookIssue($update_data)
	{
			$SESS_ARRAY= $this->session->all_userdata();
			
			// Get Parameters
				$param_array     = array(
				'P_UID' => $SESS_ARRAY['user_id'],
				'P_TOKEN' => $SESS_ARRAY['token_id'],
				'P_RETURNDT' => trim($update_data['return_date']),
				'P_RENTID' =>trim(base64_decode($update_data['rentid']))
				);
		//print_r($param_array);exit;
		$webservice_path = WEBSERVICE_PATH . "common/pro_update_return_book.php";
		$web_response = $this->utility_model->callback_webservice_token($webservice_path,$param_array);
		
		//echo '<pre>';print_r($web_response);echo '</pre>';		exit;					
		if($web_response['http_code'] == 200)
			{
				$resp = $web_response['response'];
				return $resp;
			}
			else
			{
				$resp = $web_response['response'];
				return $resp;
			}
	}//function end
	/************update  book END *********************/

// return boook list
function getreturnBookList()
	{
		
		$SESS_ARRAY= $this->session->all_userdata();
		
		$param_array     = array(
				'P_UID' => $SESS_ARRAY['user_id'],
				'P_TOKEN' => $SESS_ARRAY['token_id']
			);
		
		$webservice_path = WEBSERVICE_PATH . "common/pro_get_rent_book_return_list.php";
		
		$web_response = $this->utility_model->callback_webservice_token($webservice_path,$param_array);
		
//echo '<pre>';print_r($web_response);echo '</pre>'; exit;
		
		if($web_response['http_code'] == 200)
			{
				
				$WEB_RESPONSE = json_decode($web_response['response']);
				$status=$WEB_RESPONSE->XSTS;
			if($status == 1)
			{
			// Final aray
			$FINAL_ARRAY= $WEB_RESPONSE->X_BOOK_LIST;
			$json='';
			$json = $json . "{";
			$json = $json . "\"data\":";
			$json = $json . "[";
			
			for($j=0;$j<count($FINAL_ARRAY);$j++)
				{
					if($FINAL_ARRAY[$j]->X_ACT=="y"){$status="eye";$status_class='btn-success';$editClass='';$statustitle='Active';}
					else if($FINAL_ARRAY[$j]->X_ACT=="n"){$status="eye-slash";$status_class='btn-warning';$editClass='disabled';$statustitle='Inactive';}
					
										
					$INC=$j+1;
					$json = $json . "[";
					$json = $json . "\"".$INC."\",";
					$json = $json . "\"".$FINAL_ARRAY[$j]->X_USRNM."\",";
					$json = $json . "\"".$FINAL_ARRAY[$j]->X_BNM."\",";
					$json = $json . "\"".$FINAL_ARRAY[$j]->X_RETURNDT."\",";
					$json = $json . "\"".$FINAL_ARRAY[$j]->X_RENT."\"";
					
					
					
					$json = $json . "]";
					if(count($FINAL_ARRAY)==($j+1))
					{
						$json = $json ."";
					}
					else
					{
						$json = $json .",";
					}
				}
				$json = $json . "]";
				$json = $json . "}";
			}
			
			echo $json; // data list return response
			}
	}
	
	
	
	
}
?>
