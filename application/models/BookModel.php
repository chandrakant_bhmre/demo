<?php
/*Created By : Chandrakant Bhamare
creation Date : 2022-03-01
Modified By : 
Modified Date : 
Other Information : Bo0ok List
*/
defined('BASEPATH') OR exit('No direct script access allowed');

class BookModel extends CI_Model 
{
	/******************GET LIST OF ALL books***/
	function getBookList()
	{
		
		$SESS_ARRAY= $this->session->all_userdata();
		
		$param_array     = array(
				'P_UID' => $SESS_ARRAY['user_id'],
				'P_TOKEN' => $SESS_ARRAY['token_id']
			);
		
		$webservice_path = WEBSERVICE_PATH . "common/pro_get_book_list.php";
		
		$web_response = $this->utility_model->callback_webservice_token($webservice_path,$param_array);
		
//echo '<pre>';print_r($web_response);echo '</pre>'; exit;
		
		if($web_response['http_code'] == 200)
			{
				
				$WEB_RESPONSE = json_decode($web_response['response']);
				$status=$WEB_RESPONSE->XSTS;
			if($status == 1)
			{
			// Final aray
			$FINAL_ARRAY= $WEB_RESPONSE->X_BOOK_LIST;
			$json='';
			$json = $json . "{";
			$json = $json . "\"data\":";
			$json = $json . "[";
			
			for($j=0;$j<count($FINAL_ARRAY);$j++)
				{
					if($FINAL_ARRAY[$j]->X_ACT=="y"){$status="eye";$status_class='btn-success';$editClass='';$statustitle='Active';}
					else if($FINAL_ARRAY[$j]->X_ACT=="n"){$status="eye-slash";$status_class='btn-warning';$editClass='disabled';$statustitle='Inactive';}
					
										
					$INC=$j+1;
					$json = $json . "[";
					$json = $json . "\"".$INC."\",";
					$json = $json . "\"".$FINAL_ARRAY[$j]->X_BNM."\",";
					$json = $json . "\"".$FINAL_ARRAY[$j]->X_BNM."\",";
					$json = $json . "\"".$FINAL_ARRAY[$j]->X_BAUTHOR."\",";
					$json = $json . "\"".$FINAL_ARRAY[$j]->X_PRICE."\",";
					
					
					
					
				/*	if($this->session->userdata('urole')==1){
						
						$json = $json .",";}
						else
						{
							$json = $json .",";
						}*/
					
				
					$json = $json . "\"<a href='".base_url()."admin/book/bookedit?id=".base64_encode($FINAL_ARRAY[$j]->X_BID)."' class='btn btn-sm btn-primary ".$editClass."' title='Edit'><i class='fa fa-pencil'></i></a>\"";
						
						/*$json = $json. "&nbsp;<a id='user_stat_".$FINAL_ARRAY[$j]->X_ACT."' class='btn ".$status_class."' onclick=change_all_status('".$FINAL_ARRAY[$j]->X_ACT."','user_stat','".$FINAL_ARRAY[$j]->X_BID."','ORG_PROJECT') href='javascript:void(0);' title='".$statustitle."'><i class='fa fa-".$status."'></i></a>";
						$json = $json. "&nbsp;<button type='button' title='Delete' class='btn btn-danger'  onclick=project_delete('".$FINAL_ARRAY[$j]->X_BID."') ".$ACCSS_HIDE."><i class='fa fa-trash'></button></button>\"";*/
					
					$json = $json . "]";
					if(count($FINAL_ARRAY)==($j+1))
					{
						$json = $json ."";
					}
					else
					{
						$json = $json .",";
					}
				}
				$json = $json . "]";
				$json = $json . "}";
			}
			
			echo $json; // data list return response
			}
	}
	
	
	/**********CREATE Book*****************************/
	function createBook($insert_data)
	{
			$SESS_ARRAY= $this->session->all_userdata();
			
			$param_array     = array(
				'P_UID' => $SESS_ARRAY['user_id'],
				'P_TOKEN' => $SESS_ARRAY['token_id'],
				'P_BNM' => trim($insert_data['book_name']),
				'P_ANM' => trim($insert_data['author_name']),
				'P_BPRICE' => trim($insert_data['book_amount'])
				
			);
		$webservice_path = WEBSERVICE_PATH . "common/pro_create_book.php";
		$web_response = $this->utility_model->callback_webservice_token($webservice_path,$param_array);
				
	//echo '<pre>';print_r($web_response);echo '</pre>'; exit;							
		if($web_response['http_code'] == 200)
			{
				$resp = $web_response['response'];
				return $resp;
			}
			else
			{
				$resp = $web_response['response'];
				return $resp;
			}
	}//function end
	/************CREATE book END *********************/
	
	/* get Book Detaikls */
	function getBookDetails($bookid)
	{
		$SESS_ARRAY= $this->session->all_userdata();
		// Get Parameters
			$param_array     = array(
				'P_UID' => $SESS_ARRAY['user_id'],
				'P_TOKEN' => $SESS_ARRAY['token_id'],
				'P_BOOKID' => trim($bookid)
			);
		// Web service path
		$webservice_path = WEBSERVICE_PATH . "common/pro_get_book_details.php";
		// get web service calling function and return complite array
		$web_response = $this->utility_model->callback_webservice_token($webservice_path,$param_array);
		
		//print_r($web_response);exit;
		
		// web service return status
		if($web_response['http_code'] == 200)
			{
				// web service response 
				return $WEB_RESPONSE = json_decode($web_response['response']);
			}
	}
	
	
	
	/**********UPDATE Book *****************************/
	function updateBook($update_data)
	{
			$SESS_ARRAY= $this->session->all_userdata();
			
			// Get Parameters
				$param_array     = array(
				'P_UID' => $SESS_ARRAY['user_id'],
				'P_TOKEN' => $SESS_ARRAY['token_id'],
				'P_BOOKNM' => trim($update_data['book_name']),
				'P_ANAME' => trim($update_data['author_name']),
				'P_BAMT' => trim($update_data['book_amount']),
				'P_BOOKID' =>trim(base64_decode($update_data['bookid']))
				);
		//print_r($param_array);exit;
		$webservice_path = WEBSERVICE_PATH . "common/pro_update_book.php";
		$web_response = $this->utility_model->callback_webservice_token($webservice_path,$param_array);
		
		//echo '<pre>';print_r($web_response);echo '</pre>';		exit;					
		if($web_response['http_code'] == 200)
			{
				$resp = $web_response['response'];
				return $resp;
			}
			else
			{
				$resp = $web_response['response'];
				return $resp;
			}
	}//function end
	/************update  book END *********************/

	
	
	
	
}
?>
