<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Utility_model extends CI_Model 
{
	var $miss_token;
	
		public function __construct()
		{
			parent::__construct();
			$this->load->library('email');
		}
		//common webservice function
		/*
		$webservice_path = path of webservice url
		$param_array = webserivce parameter array
		*/
		//for token_id and session_id web service calling(before login and login)
		public function callback_webservice($webservice_path=false,$param_array=false)
		{
			// check if the cookie token Id is null, if it is then flush all cokkies, session variables and route user to the Login Page
			$curl = curl_init();
			curl_setopt_array($curl, array(
			CURLOPT_URL 			=> $webservice_path,
			CURLOPT_RETURNTRANSFER 	=> true,
			CURLOPT_ENCODING 		=> "",
			CURLOPT_MAXREDIRS 		=> 10,
			CURLOPT_TIMEOUT 		=> 30,
			CURLOPT_HEADER 			=> 1,
			CURLOPT_HTTP_VERSION 	=> CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST 	=> "POST",
			CURLOPT_POSTFIELDS 		=> json_encode($param_array),
			CURLOPT_HTTPHEADER		=> array(
										"authorization: Basic ".base64_encode('ultrashop:ULTshop@V'),
										"cache-control: no-cache",
										"content-type: application/json"
										),
			));
			//webservice response
			$response = curl_exec($curl);
			/*print_r($response);
			exit;*/
			
			//curl header response
			$http_response = curl_getinfo($curl);
			/*var_dump($http_response);*/
			/*var_dump($_SESSION);*/
		
			//get customise curl header response
			$headers = $this->get_headers_from_curl_response($response);
			
			$curl_response = array_merge($http_response, $headers);
			
			
			// check if the cookie token Id is null, if it is then flush all cokkies, session variables and route user to the Login Page
			$err = curl_error($curl);
			
			curl_close($curl);
			if ($err) {
			// debug
			} 
			else {
			return $curl_response;
			}
		}
		
		
		
		// use this function after login web services 
		public function callback_webservice_token($webservice_path=false,$param_array=false)
		{
			
			if(isset($param_array['P_ROLE'])=='U'){
			$session_token = $this->session->userdata('token_id');
			}else if(isset($param_array['P_ROLE'])=='AD')
			{
				$session_token = $this->session->userdata('token_id');
			}
			//echo $session_token;
//			exit;

		
			// check if the cookie token Id is null, if it is then flush all cokkies, session variables and route user to the Login Page
			$curl = curl_init();
			curl_setopt_array($curl, array(
			CURLOPT_URL 			=> $webservice_path,
			CURLOPT_RETURNTRANSFER 	=> true,
			CURLOPT_ENCODING 		=> "",
			CURLOPT_MAXREDIRS 		=> 10,
			CURLOPT_TIMEOUT 		=> 30,
			CURLOPT_HEADER 			=> 1,
			CURLOPT_HTTP_VERSION 	=> CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST 	=> "POST",
			CURLOPT_POSTFIELDS 		=> json_encode($param_array),
			CURLOPT_HTTPHEADER		=> array(
										"authorization: Basic ".base64_encode('ultrashop:ULTshop@V'),
										"cache-control: no-cache",
										"content-type: application/json"
										),
			));
			//webservice response
			$response = curl_exec($curl);
					
			
			//curl header response
			$http_response = curl_getinfo($curl);
			/*var_dump($http_response);*/
			/*var_dump($_SESSION);*/
			
			//get customise curl header response
			$headers = $this->get_headers_from_curl_response($response);
			
			
			$curl_response = array_merge($http_response, $headers);
			// check if the cookie token Id is null, if it is then flush all cokkies, session variables and route user to the Login Page
			/*var_dump($headers);exit;*/
			if($curl_response['http_code']=="406")
			{
			    $this->session->set_userdata('miss_token','session_expired');	
				
			}
			/*$responses=json_decode($response);*/
			$err = curl_error($curl);
			
			curl_close($curl);
			/*$array_rsp=array();
			$array_rsp['header']=$headers;
			$array_rsp['status']=$http_response['http_code'];*/
			if ($err) {
			// debug
			} 
			else {
			return $curl_response;
			}
		}
		
		public function callback_webservice_token_account($webservice_path=false,$param_array=false)
		{
			$session_token = $this->session->userdata('token_id');
			//print_r($param_array);
			$session_token;
		 
			// check if the cookie token Id is null, if it is then flush all cokkies, session variables and route user to the Login Page
			$curl = curl_init();
			curl_setopt_array($curl, array(
			CURLOPT_URL 			=> $webservice_path,
			CURLOPT_RETURNTRANSFER 	=> true,
			CURLOPT_ENCODING 		=> "",
			CURLOPT_MAXREDIRS 		=> 10,
			CURLOPT_TIMEOUT 		=> 30,
			CURLOPT_HEADER 			=> 1,
			CURLOPT_HTTP_VERSION 	=> CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST 	=> "POST",
			CURLOPT_POSTFIELDS 		=> $param_array,
			CURLOPT_HTTPHEADER		=> 
										array(
										"authorization: Basic ".base64_encode('ultrashop:ULTshop@V'),
										"token-session: ".$session_token,
										"cache-control: no-cache",
										"content-type: application/json"
										),
			));
			//webservice response
			$response = curl_exec($curl);
			//var_dump($response);
			//exit;
			//curl header response
			$http_response = curl_getinfo($curl);
			
			/*var_dump($_SESSION);*/
			
			//get customise curl header response
			$headers = $this->get_headers_from_curl_response($response);
			
			$curl_response = array_merge($http_response, $headers);
			// check if the cookie token Id is null, if it is then flush all cokkies, session variables and route user to the Login Page
			/*var_dump($headers);exit;*/
			if($curl_response['http_code']=="406")
			{
			    $this->session->set_userdata('miss_token','session_expired');	
				
			}
			/*$responses=json_decode($response);*/
			$err = curl_error($curl);
			
			curl_close($curl);
			/*$array_rsp=array();
			$array_rsp['header']=$headers;
			$array_rsp['status']=$http_response['http_code'];*/
			if ($err) {
			// debug
			} 
			else {
			return $curl_response;
			}
		}
		
		//to get header response
		function get_headers_from_curl_response($headerContent)
		{
			
			
			$headers = array();
			
			// Split the string on every "double" new line.
			$arrRequests = explode("\r\n\r\n", $headerContent);
			
			// Loop of response headers. The "count() -1" is to 
			//avoid an empty row for the extra line break before the body of the response.
			$requestCount = (count($arrRequests)-1);
			
			for ($index = 0; $index < $requestCount; $index++) {
				
				foreach (explode("\r\n", $arrRequests[$index]) as $i => $line)
				{
					if ($i === 0)
						$headers['http_response_code'] = $line;
					else
					{
						list ($key, $value) = explode(': ', $line);
						$headers[$key] = $value;
					}
				}
			}
			
			//$headers['response'] =  (isset($arrRequests[1]) ? $arrRequests[1] : "");
			$headers['response'] = $arrRequests[$requestCount];
			//var_dump($headers['response']);		
			return $headers;
		}
		
		//error handling function 
				
		//set token id in session after web service calling
		public function set_session_tokenid($token_id=false)
		{
			$this->session->set_userdata('token_id',$token_id);
		}
		
		// SMTP email setting here
		public function setting_smtp()
		{
			$permission=TRUE;
			if($permission==TRUE)
			{
				$config['protocol']    	= 'smtp';
				$config['smtp_host']    = 'ssl://smtp.gmail.com';
				$config['smtp_port']    = '465';
				$config['smtp_timeout'] = '7';
				$config['smtp_user']    = '';
				$config['smtp_pass']    = '';
				$config['charset']    	= 'utf-8';
				$config['newline']    	= "\r\n";
				$config['mailtype'] 	= 'html'; // or html
				$config['validation'] 	= TRUE; // bool whether to validate email or not  
				$this->email->initialize($config);	
			}
		}
		
		function space_injection($string){
		$string = preg_replace('!\s+!', ' ', $string);
		return $string;
	}
	
	
	//To check session of user 
	public function checkSession()
	{
		
		$SESS_ARRAY= $this->session->all_userdata();
		
		if(isset($SESS_ARRAY['urole']))
		{	$role=$SESS_ARRAY['urole'];$userid=$SESS_ARRAY['user_id'];	}
		else{$role=$SESS_ARRAY['urolea'];$userid=$SESS_ARRAY['usera_id'];}
			
			// Get Parameters
				$param_array     = array(
				'P_ROLE' => $role,
				'P_TOKEN' => $SESS_ARRAY['token_id'],
				'P_UID' => $userid
			);
			
				$webservice_path = WEBSERVICE_PATH . "common/mlm_check_session.php";
				$web_response = $this->callback_webservice_token($webservice_path,$param_array);
				//echo '<pre>';print_r($web_response);echo '</pre>';
				if($web_response['http_code'] == 200)
				{
					$resp = json_decode($web_response['response']);
					
					$status=$resp->X_STS;
										
					return $status;
				}
				else
				{
					$resp = json_decode($web_response['response']);
					
					$status=$resp->X_MSG;
					//logout
					if(isset($SESS_ARRAY['urole']))
					{
						$user_details = array(
                        'user_id' => '',
                        'emp_id' => '',
                        'user_name' => '',
                        'first_name' => '',
                        'last_name' => '',
                        'email' => '',
						'app_id' => '',
						'cont' => '',
						'pos' => '',
                        'urole' => '',
                        'token_id' => '',
                        'login_flag' => ''
						);
						// unset array and unset session array
						$this->session->unset_userdata($user_details);
						// distroy all session 
						@$this->session->sess_destroy();
					}//user logout
					else if(isset($SESS_ARRAY['urolea']))
					{
								$user_details = array(
								
								'usera_id' => '',
								'usera_fname' => '',
								'emaila' => '',
								'urolea' => '',
								'utokena' => '',
								'urolea' => '',
								'tokena_id' => '',
								'uphonea' => '',
								'logina_flag' => ''
							);
							// unset array and unset session array
							$this->session->unset_userdata($user_details);
							// distroy all session 
							@$this->session->sess_destroy();
					}//admin logout
										
					return $status;
				}
	
	}//checksession END */
	
	public function sendSMS($mobile_no,$msg)
	{
		$ch = curl_init("http://mysmsshop.in/V2/http-api.php?apikey=SpwpWZK4Jw0qgKDL&senderid=VSHOPE&number=".$mobile_no."&message=".urlencode($msg)."");
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch,CURLOPT_POST,1);
		curl_setopt($ch,CURLOPT_POSTFIELDS,"");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,TRUE);
		$data = curl_exec($ch);
		$json = json_decode($data,TRUE);
		return $json['status'];
	}
	
	public function sendMail($to,$subject,$message)
	{
		$headers  = 'From: no-reply@vonlineshopee.com' . "\r\n" .
											'Reply-To: support@ultraliant.com' . "\r\n" .
											'MIME-Version: 1.0' . "\r\n" .
											'Content-type: text/html; charset=iso-8859-1' . "\r\n" .
											'X-Mailer: PHP/' . phpversion();
		$mresponse = mail($to,$subject,$message,$headers);
		return $mresponse;
	}
	
	//GET SUFFIX FOR NUMBERS
	public function ordinal_suffix($num)
	{
    $num = $num % 100; // protect against large numbers
    if($num < 11 || $num > 13){
         switch($num % 10){
            case 1: return 'st';
            case 2: return 'nd';
            case 3: return 'rd';
        }
    }
    return 'th';
	}
	
	//FIND KEY VALUE PAIR IN ASSOCIATIVE ARRAY
	function is_in_array($array, $key, $key_value){
      $within_array = 'no';
      foreach( $array as $k=>$v ){
        if( is_array($v) ){
            $within_array = is_in_array($v, $key, $key_value);
            if( $within_array == 'yes' ){
                break;
            }
        } else {
                if( $v == $key_value && $k == $key ){
                        $within_array = 'yes';
                        break;
                }
        }
      }
      return $within_array;
}
	
}
